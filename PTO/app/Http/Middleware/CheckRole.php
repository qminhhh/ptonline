<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('homepage');
        }
        $permission = $this->getRequiredRoleForRoute($request->route());
        $role = Role::find(Auth::user()->idRole);
        $permissions=[];
        foreach ($role->RolePermissions as $r){
            $permissions[]=$r->Permission->slug;

        }
//      dd($permissions,$permission);
        if (in_array($permission,$permissions)){
            return $next($request);
        }
//        return response([
//            'error' => [
//                'code' => 'INSUFFICIENT_ROLE',
//                'description' => 'You are not authorized to access this resource.'
//            ]
//        ], 401);
//        return view('master_layout.page.error');
        return redirect()->route('pto.error');
    }
    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['permission']) ? $actions['permission'] : null;
    }
}
