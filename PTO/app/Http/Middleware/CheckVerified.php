<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->verified!=1){
            Auth::logout();
            return redirect()->route('login')->with('thongbao','Vui lòng kiểm tra email để kích hoạt tài khoản');
        }
        return $next($request);
    }
}
