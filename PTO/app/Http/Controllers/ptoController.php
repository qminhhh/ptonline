<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use App\User;
use App\Rating;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ptoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->status == 1) {
            return view('pages.wattingconfirm');
        } else {
            return view('pto.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $request->validate([
//            'introduce' => 'required',
//            'certificate' => 'required',
//            'reason' => 'required',
//        ]);
//        $user = new User;
//        $user->introduce = $request->introduce;
//        $user->certificate = $request->certificate;
//        $user->reason = $request->reason;
//        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('pto.show', ['user' => $user]);
    }

    public function ptoshow()
    {
        // dd(Auth::id());
        $id = \Auth::id();
        $user = User::find($id);
        return view('pto.show', ['user' => $user]);

    }

    public function ptoList()
    {
        return view('pto.pto_list');
    }

    public function dataPT()
    {
        return DataTables::of(User::where('status', '=', 2)->where('idRole', '=', 2)->get())
            ->addColumn('numCourse', function ($data) {
                return $data->courses->count();
            })
            ->addColumn('actions', function ($data) {
                return view('partials/actions', ['route' => 'pto', 'id' => $data->id, 'type' => 'only-show']);
            })->rawColumns(['actions'])->make(true);
    }

    public function ptoDetail($id)
    {
        $user = User::find($id);
        return view('pto.pto_detail', compact('user'));
    }

    public function ptoshowdetail($slug)
    {
        $user = User::where('slug', $slug)->first();
        return view('master_layout.page.pto_detail', ['user' => $user]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    user đăng ký form được edit thành PTO
    public function edit($id)
    {
        if (Auth::id() == $id) {
            $user = User::find($id);
            return view('pto.create', ['user' => $user]);
        } else {
            return redirect()->route('pto.error');
        }
    }

    public function edited($id)
    {
        $user = User::find($id);
        return view('pto.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'introduce' => 'required|min:30',
                'phone' => 'required|min:10|max:11',
                'DOB' => 'required|date',
                'certificate' => 'required',
                'reason' => 'required|min:30',
                'height' => 'required|integer|min:0',
                'weight' => 'required|integer|min:0',
                'bust' => 'required|integer|min:0',
                'wais' => 'required|integer|min:0',
                'hips' => 'required|integer|min:0',
                'rule' => 'required'

            ],
            [
                'introduce.required' => 'Chưa nhập giới thiệu bản thân',
                'introduce.min' => 'Giới thiệu bản thân ít nhất 30 kí tự',
                'phone.required' => 'Chưa nhập số điện thoại',
                'phone.min' => 'Chưa nhập đúng định dạng số điện thoại',
                'phone.max' => 'Chưa nhập đúng định dạng số điện thoại',
                'DOB.required' => 'Chưa nhập sinh nhật',
                'certificate.required' => 'Chưa nhập bằng cấp, chứng chỉ',
                'reason.required' => 'Chưa nhập lý do',
                'reason.min' => 'Lý do muốn trở thành PTO ít nhất 30 kí tự',
                'height.required' => 'Chưa nhập chiều cao',
                'height.integer' => 'Chiều cao là số',
                'height.min' => 'Chiều cao phải > 0',
                'weight.required' => 'Chưa nhập cân nặng',
                'weight.integer' => 'Cân nặng là số',
                'weight.min' => 'Cân nặng phải > 0',
                'bust.required' => 'Chưa nhập vòng 1',
                'bust.integer' => 'Vòng 1 là số',
                'bust.min' => 'Vòng 1 phải > 0',
                'wais.required' => 'Chưa nhập vòng 2',
                'wais.integer' => 'Vòng 2 là số',
                'wais.min' => 'Vòng 2 phải > 0',
                'hips.required' => 'Chưa nhập vòng 3',
                'hips.integer' => 'Vòng 3 là số',
                'hips.min' => 'Vòng 3 phải > 0',
                'rule.required' => 'Chưa đồng ý điều khoản dịch vụ'

            ]);
        $user = User::find($id);
        $user->introduce = $request->introduce;
        $user->phone = $request->phone;
        $user->DOB = $request->DOB;
        $user->height = $request->height;
        $user->weight = $request->weight;
        $user->bust = $request->bust;
        $user->wais = $request->wais;
        $user->hips = $request->hips;
        $user->certificate = $request->certificate;
        $user->reason = $request->reason;
        $user->status = 1;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->userName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $user->image = "/upload/users/" . $name;
        };
        $user->save();
        return view('pages.wattingconfirm');
    }

    public function update1($id)
    {
        $user = User::find($id);
        $user->status = 2;
        $user->idRole = 2;
        $user->save();
        return redirect()->route('pto.list')->with('thongbao', 'Phê duyệt thành công');
    }

    public function updated(Request $request, $id)
    {
        $request->validate([
            'fullName' => 'required|string|max:255',
            'phone' => 'required|min:10|max:11',
            'DOB' => 'required|date',
            'introduce' => 'required',
            'certificate' => 'required',
            'reason' => 'required',
            'height' => 'required|integer|min:0',
            'weight' => 'required|integer|min:0',
            'bust' => 'required|integer|min:0',
            'wais' => 'required|integer|min:0',
            'hips' => 'required|integer|min:0',

        ]);
        $user = User::find($id);
        $user->status = 2;
        $user->idRole = 2;
        $user->fullName = $request->fullName;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->userName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $user->image = "/upload/users/" . $name;
        };
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->DOB = $request->DOB;
        $user->introduce = $request->introduce;
        $user->height = $request->height;
        $user->weight = $request->weight;
        $user->bust = $request->bust;
        $user->wais = $request->wais;
        $user->hips = $request->hips;
        $user->certificate = $request->certificate;
        $user->reason = $request->reason;
        $user->save();
        return redirect()->route('pto.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->status = 0;
        $user->save();
        return redirect()->route('pto.list')->with('thongbao', 'Đã xóa thành công');
    }

    public function data()
    {
        return DataTables::of(User::where('status', '=', 1)->get())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'pto', 'id' => $data->id, 'type' => '']);
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('pto.wattinglist');
    }

    public function accept($id)
    {
        $user = User::find($id);

//        return view('master_layout.page.pto_detail', ['user' => $user]);
        return view('pto.showforadmin', ['user' => $user]);
    }

    public function data1()
    {
        return DataTables::of(User::where('status', '=', 1)->get())->addColumn('actions', function ($data) {
            return view('partials/act', ['route' => 'pto', 'id' => $data->id, 'type' => '']);
        })->rawColumns(['actions'])->make(true);
    }

    public function error()
    {
        $id = \Auth::id();
        $user = User::find($id);
        return view('master_layout.page.error', ['user' => $user]);
    }

    public function rule()
    {
        return view('pages.rule');
    }
}
