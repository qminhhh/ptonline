<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_Process;
use App\Mail\OrderPaymentDetail;
use App\Notify;
use App\Order;
use App\OrderDetail;
use App\Province;
use App\User;
use App\Course_Manage;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $order = Order::all();
        return view('master_layout.orders.order_list', compact('order'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('orders.order_detail', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add_cart(Request $request)
    {

        $id = $request->id;
        $courseName = $request->courseName;
        $price = $request->price;
        $image = $request->image;
        $namept = $request->namept;
        $slug = $request->slug;

        $courseMn = Course_Manage::where('user_id', Auth::id())->where('course_id', $id)->first();
        $orderDts = OrderDetail::where('course_id', $id)->get();


        if ($courseMn != null) {
            return response()
                ->json([
                    'status' => 2,
                    'message' => 'Bạn đã mua khóa học này',
                ]);
        }
        foreach (Cart::content() as $row) {
            if ($id == $row->id) {
                return response()
                    ->json(
                        [
                            'status' => 0, 'message' => 'Khóa học đã có trong giỏ hàng',
                            'data' => Cart::content()
                        ]);
            }


        }

        foreach ($orderDts as $orderDt) {
            if ($orderDt->order->user->id == Auth::id()) {
                return response()
                    ->json([
                        'status' => 3,
                        'message' => 'Khóa học đang chờ kích hoạt',
                    ]);
            }
        }
        Cart::add($id, $courseName, 1, $price, ['image' => $image, 'namept' => $namept,'slug'=>$slug]);
        return response()
            ->json(['status' => 1, 'message' => 'Thêm khóa học thành công']);

    }

    public function cartList()
    {
        return response()
            ->json(
                [
                    'status' => 1,
                    'message' => 'Get list carts successfully',
                    'data' => Cart::content()
                ]);
    }

    public function showCart()
    {
        $province = Province::all();
        return view('master_layout.page.cart', ['province' => $province]);
    }

    public function deleteCart(Request $request)
    {
        $id = $request->id;
        Cart::remove($id);
        //return view('master_layout.page.cart');
    }

    public function showPayment()
    {
        return view('cart.payment');
    }

    public function saveCOD(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required|min:3',
                'note' => 'required|min:10',
                'district' => 'required|min:5',
                'address' => 'required|min:10',
                'phone' => 'required',
                'email' => 'required',
            ],
            [
                'name.required' => 'Chưa nhập họ tên',
                'name.min' => 'Họ tên ít nhất 3 kí tự',
                'note.required' => 'Chưa nhập ghi chú',
                'note.min' => 'Ghi chú ít nhất 10 kí tự',
                'district.required' => 'Chưa nhập Quận/Huyện',
                'district.min' => 'Quận/Huyện ít nhất 5 kí tự',
                'address.required' => 'Chưa nhập địa chỉ',
                'address.min' => 'Địa chỉ ít nhất 10 kí tự',
                'phone.required' => 'Chưa nhập số điện thoại',
                'email.required' => 'Chưa nhập email',
            ]);

        $order = new Order();
        $user = User::find(Auth::id());
        $order->name = $request->name;
        $user->name = $request->name;
        $order->province = $request->province;
        $user->province = $request->province;
        $order->district = $request->district;
        $user->district = $request->district;
        $order->address = $request->address;
        $user->address = $request->address;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $user->phoneOrder = $request->phone;
        $order->note = $request->note;
        $order->user_id = Auth::id();
        $order->total = str_replace(',', '', Cart::subtotal());
        $order->status = 0;
        $order->tus = 0;
        //temporary
        $order->active_code = time() . rand(100, 999);
        $params = $request->all();
        $captchaParams = [
            'secret' => config('app.google_secret'),
            'response' => $params['g-recaptcha-response']
        ];
        //dd($captchaParams);
        $verifyCaptcha = json_decode($this->verifyCaptcha($captchaParams));
        if ($verifyCaptcha->success) {
            $order->save();
        }
        $user->save();

        foreach (Cart::content() as $row) {
            $order_detail = new OrderDetail();
            $order_detail->course_id = $row->id;
            $order_detail->order_id = $order->id;
            $order_detail->price = $row->price;
            $order_detail->save();
        }


        Mail::to($request->email)->send(new OrderPaymentDetail($order));
//        Nexmo::message()->send([
//            'to'   => $request->phone,
//            'from' => '841684603855',
//            'text' => 'Ban da mua khoa hoc thanh cong tren trang PTO.com . '
//        ]);
        return redirect()->route('courses.myCourse');

    }

    public function verifyCaptcha($params)
    {
        $verifyUrl = env('GOOGLE_CAPTCHA_VERIFY_LINK', 'https://www.google.com/recaptcha/api/siteverify');
        $ch = curl_init($verifyUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function list()
    {
        return view('orders.order_list');
    }

    public function data()
    {
        return DataTables::of(Order::all())
            ->addColumn('name', function ($data) {
                if ($data->name != null) {
                    return $data->name;
                } else {
                    return $data->user->fullName;
                }
            })
            ->addColumn('total_nice', function ($data) {
                return number_format($data->total) . " ₫";
            })
            ->addColumn('actions', function ($data) {
                return view('partials/actions', ['route' => 'order', 'id' => $data->id, 'type' => 'only-show']);
            })->rawColumns(['actions'])->make(true);
    }

    public function orderSave(Request $request, $id)
    {
        $order = Order::find($id);
        if ($request->tus == 1) {
            $order->tus = 1;
            if ($order->name != null) {
                $notify = new Notify();
                $notify->user_id = $order->user->id;
                $notify->seen = 0;
                $notify->noti = 'Mã kích hoạt đơn hàng #' . $order->id . ' của bạn đã được gửi đi. Thời gian vận chuyển có thể lên tới 3 ngày và muộn nhất là 1
                tuần !';
                $notify->route = route('order.detailOrder', $order->id);
                $notify->save();
            }
        } else {
            $order->tus = 0;
        }
        $order->save();

        return redirect()->route('order.list')->with('thongbao', 'Đã xuất mã kích hoạt thành công');

    }

    public function detailOrder($id)
    {
        $order = Order::find($id);
        if (Auth::id() == $order->user_id) {
            return view('user_manage.course.activeCourse', ['order' => $order]);
        } else {
            return redirect()->route('pto.error');
        }
    }

    public function activeOrder(Request $request, $id)
    {
        $order = Order::find($id);
        if (Auth::id() == $order->user_id) {
            if ($request->active_code == $order->active_code) {
                $order->status_active = 1;
                $order->status = 1;
                $order->active_code = 0;
                $order->save();
                foreach ($order->orderdetails as $od) {
                    $course_manage = New Course_Manage();
                    $course_manage->course_id = $od->course_id;
                    $course_manage->user_id = $order->user_id;
                    $course_manage->order_id = $order->id;
                    $course_manage->status = 0;
                    $course_manage->save();
                    foreach ($od->course->lessons as $l) {
                        $course_process = New Course_Process();
                        $course_process->lesson_id = $l->id;
                        $course_process->user_id = Auth::id();
                        $course_process->save();
                    }
                }
                foreach ($order->orderdetails as $orderdetail) {
                    $course = Course::where('id', $orderdetail->course->id)->first();
                    $course->lock = 1;
                    $course->save();
                    $noti = new Notify();
                    $noti->status = 0;
                    $noti->user_id = $order->user->id;
                    $noti->pto_id = $orderdetail->course->user->id;
                    $noti->message = $order->user->fullName . ' đã mua khóa học " ' . $orderdetail->course->courseName . ' " của bạn ! Khóa học chuyển sang trạng thái Khóa !';
                    $noti->link = route('order.listCourse');
                    $noti->save();
                }
                return redirect()->back();
            } else {
                return redirect()->back()->with('thongbao', 'Mã kích hoạt không đúng !');
            }
        } else {
            return redirect()->route('pto.error');
        }

    }

    public function manage()
    {
        $order = Order::where([
            ['user_id', Auth::id()],
            ['status', '=', '1'],
        ])->get();
        $sum = Course_Manage::where('user_id', Auth::id())->count();
        $total = 0;
        foreach ($order as $key => $od) {
            $total += $od->total;
        }

        return view('user_manage.order.manage', ['order' => $order, 'sum' => $sum, 'total' => $total]);
    }


    public function listCourse()
    {

        $course = Course::where('idUser', Auth::id())->pluck('id');
        $orderDetail = OrderDetail::whereIn('course_id', $course)->orderBy('order_id')->get();
        $total = 0;
        foreach ($orderDetail as $key => $odt) {
            $total += $odt->price;
        }
        return view('orders.list', ['total' => $total]);
    }

    public function dataCourse()
    {
        $course = Course::where('idUser', Auth::id())->pluck('id');
        return DataTables::of(OrderDetail::whereIn('course_id', $course)->orderBy('order_id')->get())
            ->addIndexColumn()
            ->addColumn('nameCourse', function ($data) {
                return $data->course->courseName;
            })
            ->addColumn('nameUser', function ($data) {
                if ($data->order->name == null)
                    return $data->order->user->fullName;
                else
                    return $data->order->name;
            })
            ->addColumn('email', function ($data) {
                if ($data->order->email == null)
                    return $data->order->user->email;
                else
                    return $data->order->email;
            })
            ->addColumn('phone', function ($data) {
                if ($data->order->phone == null)
                    return $data->order->user->phone;
                else
                    return $data->order->phone;
            })
            ->addColumn('dateBuy', function ($data) {
                return date('d-m-Y H:i', strtotime($data->created_at));
            })
            ->addColumn('price', function ($data) {
                return number_format($data->price) . " ₫";
            })
            ->make(true);
    }


}
