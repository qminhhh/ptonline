<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SendVerificationEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
class RegisController extends Controller
{
    use RegistersUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.register');
//        return view('pages.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'firstName'=>'required|string|max:255',
                'lastName'=>'required|string|max:255',
                'userName'=>'required',
                'image'=> 'required',
                'gender'=> 'required',
                'password'=>'required|string|min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation'=>'required|string|min:6',
                'email'=>'required|email|min:5|unique:users',
                'phone'=>'required',
                'DOB'=>'required|date',
            ],
            [
                'firstName.required'=>'Chưa nhập tên đệm',
                'lastName.required'=>'Chưa nhập tên',
                'userName.required'=>'Chưa nhập tên đăng nhập',
                'image.required'=>'Chưa chọn hình ảnh',
                'gender.required'=>'Chưa chọn giới tính',
                'password.required'=>'Chưa nhập mật khẩu',
                'password.min'=>'Mật khẩu phải có ít nhất 6 kí tự',
                'password.confirmed'=>'Xác nhận mật khẩu không khớp',
                'password_confirmation.required'=>'Chưa nhập mật khẩu',
                'password_confirmation.min'=>'Mật khẩu phải có ít nhất 6 kí tự',
                'email.required'=>'Chưa nhập email',
                'email.min'=>'Email sai định dạng',
                'email.unique'=>'Email đã tồn tại',
                'DOB.required'=>'Chưa nhập ngày sinh',
                'phone.required'=>'Chưa nhập số điện thoại',
            ]);
        $user = new User;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->userName = $request->userName;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->userName).time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath. "/".$name;
            $image->move($destinationPath, $name);
            $user->image = "/upload/users/".$name;
        }
        $confirmation_code = time().uniqid(true);
        $user->gender = $request->gender;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->DOB = $request->DOB;
        $user->phone = $request->phone;
        $user->status = 0;
        $user->email_token = base64_encode($request['email']);
        $user->save();
        dispatch(new SendVerificationEmail($user));
        return view('pages.verification');
//        Mail::to($request->user())->send(new EmailVerification($user));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    protected function registered(Request $request, $user)
    {

    }
    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        $user->verified = 1;
        if ($user->save()) {
            return view('pages.emailconfirm', ['user' => $user]);
        }
    }
}
