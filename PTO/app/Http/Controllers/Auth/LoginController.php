<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
    protected function authenticated($request, $user){
        if($user->verified!=1){
            Auth::logout();
            return redirect()->route('login')->with('thongbao','Vui lòng kiểm tra email để kích hoạt tài khoản');

        }
    }
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $existUser = User::where('email',$googleUser->email)->first();

            if($existUser) {
                Auth::loginUsingId($existUser->id);
            }
            else {
                $user = new User;
                $user->userName = $googleUser->name;
                $user->fullName = $googleUser->name;
                $user->email = $googleUser->email;
                $user->image = $googleUser->avatar;
                $user->password = md5(rand(1,10000));
                $user->verified = '1';
                $user->idRole = '3';
                $user->slug =  str_slug($googleUser->name . ' ' . Auth::id(), '-');
                $user->save();
                $user_slug = User::find($user->id);
                $user_slug->slug = str_slug($googleUser->name . ' ' .$user->id, '-');
                $user_slug->save();
                Auth::loginUsingId($user->id);
            }
            return redirect()->to('/home');
        }
        catch (Exception $e) {
            return 'error';
        }
    }
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function callback()
    {
        try {
            $facebookUser = Socialite::driver('facebook')->user();
            $existUser = User::where('email',$facebookUser->email)->first();

            if($existUser) {
                Auth::loginUsingId($existUser->id);
            }
            else {
                $user = new User;
                $user->userName = $facebookUser->name;
                $user->fullName = $facebookUser->name;
                $user->email = $facebookUser->email;
                $user->image = $facebookUser->avatar;
                $user->password = md5(rand(1,10000));
                $user->verified = '1';
                $user->idRole = '3';
                $user->save();
                Auth::loginUsingId($user->id);
            }
            return redirect()->to('/home');
        }
        catch (Exception $e) {
            return 'error';
        }
    }
}
