<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailVerification;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'fullName' => 'required|string|max:255',
                'userName' => 'required|string|max:255',
                'gender' => 'required|not_in:2',
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required_with:password|same:password|min:6',
                'email' => 'required|email|min:5|unique:users,email',
            ],
            [
                'fullName.required' => 'Chưa nhập họ và tên',
                'userName.required' => 'Chưa nhập tên đăng nhập',
                'gender.required' => 'Chưa chọn giới tính',
                'password.required' => 'Chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
                'password.confirmed' => 'Xác nhận mật khẩu không khớp',
                'password_confirmation.required' => 'Chưa nhập mật khẩu',
                'password_confirmation.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
                'email.required' => 'Chưa nhập email',
                'email.min' => 'Email sai định dạng',
                'email.unique' => 'Email đã tồn tại',
            ]);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */

    protected function create(array $data)
    {
        $image = "";
        if (array_key_exists('image', $data)) {
            $image = $data['image'];
            $name = str_slug($data['userName']) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $image = "/upload/users/" . $name;
        }
        $confirmation_code = time() . uniqid(true);

//        event(new Registered($user = $this->create($request->all())));
//        dispatch(new SendVerificationEmail($user));
        $user = User::create([
            'fullName' => $data['fullName'],
            'userName' => $data['userName'],
            'image' => '/upload/users/default.png',
//            'image'=>$image,
            'password' => Hash::make($data['password']),
            'gender' => $data['gender'],
            'email' => $data['email'],
            'status' => 0,
            'idRole' => 3,

            'email_token' => base64_encode($data['email'])
        ]);

        $user_slug = User::find($user->id);
        $user_slug->slug = str_slug($data['fullName'] . ' ' .$user->id, '-');
        $user_slug->save();

        Mail::to($user->email)->send(new EmailVerification($user));
//        return view('pages.verification');
        return $user;

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        return view('pages.verification');

    }

    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        $user->verified = 1;
        if ($user->save()) {
            return view('pages.emailconfirm', ['user' => $user]);
        }
    }
}
