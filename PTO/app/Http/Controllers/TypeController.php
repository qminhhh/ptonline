<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Routing\Route;


class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'typeName' => 'required|unique:types,typeName|min:3',
            ],
            [
                'typeName.required' => 'Chưa nhập tên thể loại',
                'typeName.unique' => 'Tên thể loại không được trùng',
                'typeName.min' => 'Tên thể loại ít nhất 3 kí tự',
            ]);
        $type = new Type();
        $type->typeName = $request->typeName;
        $type->save();
        return redirect()->route('type.list')->with('thongbao', 'Đã thêm thể loại thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $type = Type::find($id);
        $types = Type::all();
        return view('master_layout.page.course_type', ['type' => $type, 'types' => $types]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Type::find($id);
        return view('type.edit', ['type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'typeName' => 'required|unique:types,typeName|min:3',
            ],
            [
                'typeName.required' => 'Chưa nhập tên thể loại',
                'typeName.unique' => 'Tên thể loại không được trùng',
                'typeName.min' => 'Tên thể loại ít nhất 3 kí tự',
            ]);

        $type = Type::find($id);
        $type->typeName = $request->typeName;
        $type->save();
        return redirect()->route('type.list')->with('thongbao', 'Chỉnh sửa thể loại thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Type::destroy($id);
        return redirect()->route('type.list')->with('thongbao', 'Đã xóa thành công');
    }

    public function data()
    {
        return DataTables::of(Type::all())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'type', 'id' => $data->id, 'type' => 'edit-delete']);
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('type.index');
    }
}
