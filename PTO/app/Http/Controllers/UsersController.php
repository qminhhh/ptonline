<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_Manage;
use App\News;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Body_Process;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::whereHas('courses', function ($q) {
            $q->selectRaw('count(id) as total');
            $q->having('total', '>', 1);
        })->where('idRole', 2)->get();
        return view('master_layout.page.index', ['user' => $user]);
    }

    public function showAll()
    {
        $user = User::whereHas('courses', function ($q) {
            $q->selectRaw('count(id) as total');
            $q->having('total', '>', 0);
        })->where('idRole', 2)->get();
        return view('master_layout.page.pto_show', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $id = \Auth::id();
        if (Auth::user()->idRole == 2 && Auth::id() == $id) {
            $user = User::find($id);
            return view('user.show', ['user' => $user]);
            //return view('user.info', ['user' => $user]);
        } else if (Auth::user()->idRole == 1) {
            $user = User::find($id);
            return view('user.show', ['user' => $user]);
        } else {
            return redirect()->route('pto.error');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->idRole == 2 && Auth::id() == $id) {
            $user = User::find($id);
            return view('user.edit', ['user' => $user]);
        } else if (Auth::user()->idRole == 1) {
            $user = User::find($id);
            return view('user.edit', ['user' => $user]);
        } else {
            return redirect()->route('pto.error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,
            [
                'fullName' => 'required|string|max:255',
//                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'phone' => 'required',
                'DOB' => 'required|date',
            ],
            [
                'fullName.required' => 'Chưa nhập họ và tên',
//                'image.required' => 'Chưa chọn ảnh đại diện',
                'phone.required' => 'Chưa nhập số điện thoại',
                'phone.min' => 'Chưa nhập đúng định dạng số điện thoại',
                'phone.max' => 'Chưa nhập đúng định dạng số điện thoại',
                'DOB.required' => 'Chưa nhập sinh nhật',
            ]);
        $user = User::find($id);
        $user->fullName = $request->fullName;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->userName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $user->image = "/upload/users/" . $name;
        };
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->DOB = $request->DOB;
        $user->save();
        return redirect()->route('users.show', $id);
    }

    public function update1(Request $request, $id)
    {
        $this->validate($request,
            [
                'fullName' => 'required|string|max:255',
//                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'phone' => 'min:10|max:11',
                'DOB' => 'date',
            ],
            [
                'fullName.required' => 'Chưa nhập họ và tên',
//                'image.required' => 'Chưa chọn ảnh đại diện',
                'phone.min' => 'Chưa nhập đúng định dạng số điện thoại',
                'phone.max' => 'Chưa nhập đúng định dạng số điện thoại',
            ]);
//        $request->validate([
//            'fullName' => 'required|string|max:255',
//            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
//            'phone' => 'required',
//            'DOB' => 'required|date',
//        ]);
        $user = User::find($id);
        $user->fullName = $request->fullName;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->userName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/users');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $user->image = "/upload/users/" . $name;
        };
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->DOB = $request->DOB;
        $user->save();
        return redirect()->route('users.myAccount', $id);
    }

    public function updatePass(Request $request, $id)
    {

        $this->validate($request,
            [
                'password' => 'required',
                'newpassword' => 'required|string|min:6',
                'password_confirmation' => 'required_with:password|same:newpassword',
            ],
            [
                'password.required' => 'Chưa nhập mật khẩu',
                'newpassword.required' => 'Chưa nhập mật khẩu mới',
                'newpassword.min' => 'Mật khẩu mới phải có ít nhất 6 kí tự',
                'password.confirmed' => 'Xác nhận mật khẩu không khớp',
                'password_confirmation.required' => 'Chưa nhập mật khẩu xác nhận',
            ]
        );
        $user = User::find($id);
        $password = $request->password;
        if (Hash::check($password, $user->password)) {
            $user->password = Hash::make($request['newpassword']);
            $user->save();
            return redirect()->route('users.changePass', $id)->with('thongbao', 'Đã đổi mật khẩu thành công');
        }

        return redirect()->route('users.changePass', $id)->with('thongbao', 'Đổi mật khẩu không thành công');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('TEST');
        User::destroy($id);
        //return redirect()->route('userslist')->with('thongbao', 'Đã xóa thành công');
        return redirect()->route('userslist')->with('thongbao', 'Đã xóa thành công');
    }

    public function data()
    {
        return DataTables::of(User::all())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'users', 'id' => $data->id, 'type' => '']);
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('admin.list');
    }

    public function chart($id)
    {
        if (Auth::id() == $id) {
            $bp = Body_Process::where('idUser', Auth::id())->orderBy('date_update', 'DES')->first();
            $historyChange = Body_Process::where('idUser', Auth::id())->orderBy('date_update', 'ASC')->get();
            $label = [];
            $dataHeight = [];
            $dataWeight = [];
            $dataBust = [];
            $dataWais = [];
            $dataHips = [];
            foreach ($historyChange as $hc) {
                $label[] = date('d-m-Y', strtotime($hc->date_update));

                $dataHeight[] = $hc->height;
                $dataWeight[] = $hc->weight;
                $dataBust[] = $hc->bust;
                $dataWais[] = $hc->wais;
                $dataHips[] = $hc->hips;
            }
            return view('user_manage.body.chart', compact('bp', 'historyChange', 'label', 'dataBust', 'dataHeight', 'dataHips', 'dataWais', 'dataWeight'));
        } else {
            return redirect()->route('pto.error');
        }
    }

    public function updateChart(Request $request)
    {
        $process = new Body_Process();
        $process->idUser = Auth::id();
        $process->height = $request->height;
        $process->weight = $request->weight;
        $process->bust = $request->bust;
        $process->wais = $request->wais;
        $process->hips = $request->hips;
        $process->date_update = $request->date_update;
        $process->save();
        return redirect()->back();

    }

    public function myAccount($id)
    {
        if (Auth::id() == $id) {
            $user = User::find($id);
            return view('user_manage.account.infor', compact('user'));
        } else {
            return redirect()->route('pto.error');
        }
    }

    public function changePass($id)
    {
        if (Auth::id() == $id) {
            $user = User::find($id);
            return view('user_manage.account.password', compact('user'));
        } else {
            return redirect()->route('pto.error');
        }
    }

    public function listTrainee()
    {
        return view('trainee.list');
    }

    public function showTrainee()
    {
        return view('trainee.list');
    }


    public function dataTrainee()
    {
        $course = Course::where('idUser', Auth::id())->pluck('id');
        return DataTables::of(Course_Manage::whereIn('course_id', $course)->orderBy('user_id')->get())
            ->addIndexColumn()
            ->addColumn('nameTrainee', function ($data) {
                return $data->user->fullName;
            })
            ->addColumn('email', function ($data) {
                return $data->user->email;
            })
            ->addColumn('phone', function ($data) {
                return $data->user->phone;
            })
            ->addColumn('nameCourse', function ($data) {
                return $data->course->courseName;
            })
            ->addColumn('process', function ($data) {
                return $data->process;
            })
            ->make(true);
    }


}
