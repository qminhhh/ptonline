<?php

namespace App\Http\Controllers;

use App\Course_Process;
use App\Rating;
use App\Type;
use App\User;
use App\Course;
use App\Lesson;
use App\Order;
use App\Course_Manage;
use App\Notify;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::where([
            ['idUser', \Auth::id()],
            ['status', 1],
            ['lock', 0]
        ])->get();
        return view('courses.index', ['course' => $course]);
    }

    public function deactive()
    {
        $course = Course::where([
            ['idUser', \Auth::id()],
            ['status', 0],
            ['lock', 0],
        ])->get();
        return view('courses.course_deactive', ['course' => $course]);
    }

    public function lock()
    {
        $course = Course::where([
            ['idUser', \Auth::id()],
            ['status', 1],
            ['lock', 1],
        ])->get();
        return view('courses.course_lock', ['course' => $course]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Type::all();
        return view('courses.create', ['type' => $type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'courseName' => 'required|min:3',
                'price' => 'required|integer|min:0',
                'introduce' => 'required|min:30',
                'require' => 'required|min:30',
                'benefit' => 'required|min:30',
                'target' => 'required|min:30',
                'image' => 'required'
            ],
            [
                'courseName.required' => 'Chưa nhập tên khóa học',
                'courseName.min' => 'Tên khóa học ít nhất 3 kí tự',
                'price.required' => 'Chưa nhập giá',
                'price.integer' => 'Học phí là số',
                'price.min' => 'Học phí phải > 0',
                'introduce.required' => 'Chưa nhập tổng quát khóa học',
                'introduce.min' => 'Tổng quát khóa học ít nhất 30 kí tự',
                'require.required' => 'Chưa nhập yêu cầu khóa học',
                'require.min' => 'Yêu cầu khóa học ít nhất 30 kí tự',
                'benefit.required' => 'Chưa nhập lợi ích khóa học',
                'benefit.min' => 'Lợi ích khóa học ít nhất 30 kí tự',
                'target.required' => 'Chưa nhập mục đối tượng mục tiêu',
                'target.min' => 'Đối tượng mục tiêu ít nhất 30 kí tự',
                'image.required' => 'Chưa chọn hình ảnh'
            ]);

        $course = new Course;
        $course->type_id = $request->type;
        $course->courseName = $request->courseName;
        $course->idUser = \Auth::id();
        $course->price = $request->price;
        $course->require = $request->require;
        $course->benefit = $request->benefit;
        $course->introduce = $request->introduce;
        $course->target = $request->target;
        $course->status = $request->has('public') ? true : false;



        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->courseName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/courses');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $course->image = "/upload/courses/" . $name;
            //$course->status = 1;
        }
        $course->lock = 0;
        $course->save();

        $course->slug = str_slug($course->courseName . ' ' . $course->id, '-');

        $course->save();
        if ($course->status == 1) {
            return redirect()->route('courses.index')->with('thongbao', 'Đã thêm khóa học thành công');
        } else {
            return redirect()->route('courses.deactive')->with('thongbao', 'Đã thêm khóa học thành công');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);
        $user = Course::order_details($id);
        $trainees = Course_Manage::where('course_id', $id);
        return view('courses.course_detail', ['course' => $course, 'user' => $user, 'trainees' => $trainees]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        $type = Type::all();
        return view('courses.edit', ['course' => $course, 'type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'courseName' => 'required|min:3',
                'price' => 'required|integer|min:0',
                'introduce' => 'required|min:30',
                'require' => 'required|min:30',
                'benefit' => 'required|min:30',
                'target' => 'required|min:30',
            ],
            [
                'courseName.required' => 'Chưa nhập tên khóa học',
                'courseName.min' => 'Tên khóa học ít nhất 3 kí tự',
                'price.required' => 'Chưa nhập giá',
                'price.integer' => 'Học phí là số',
                'price.min' => 'Học phí phải > 0',
                'introduce.required' => 'Chưa nhập tổng quát khóa học',
                'introduce.min' => 'Tổng quát khóa học ít nhất 30 kí tự',
                'require.required' => 'Chưa nhập yêu cầu khóa học',
                'require.min' => 'Yêu cầu khóa học ít nhất 30 kí tự',
                'benefit.required' => 'Chưa nhập lợi ích khóa học',
                'benefit.min' => 'Lợi ích khóa học ít nhất 30 kí tự',
                'target.required' => 'Chưa nhập mục đối tượng mục tiêu',
                'target.min' => 'Đối tượng mục tiêu ít nhất 30 kí tự',
            ]);
        $course = Course::find($id);
        $course->type_id = $request->type;
        $course->courseName = $request->courseName;
        $course->price = $request->price;
        $course->introduce = $request->introduce;
        $course->require = $request->require;
        $course->benefit = $request->benefit;
        $course->target = $request->target;
        $course->status = $request->has('public') ? true : false;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->courseName) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/courses');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $course->image = "/upload/courses/" . $name;
            //$course->status = 1;
        }
        $course->lock = 0;
        $course->save();
        $course->slug = str_slug($course->courseName . ' ' . $course->id, '-');
        $course->save();
        return redirect()->route('courses.courseDetail', $id)->with('thongbao', 'Đã cập nhật khóa học thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::destroy($id);
        return redirect()->route('courses.index')->with('thongbao', 'Đã xóa khóa học thành công');
    }

    public function list()
    {
        $course = Course::orderBy('price', 'desc')->get();
        return view('courses.course_list', compact('course'));
    }

    public function data()
    {
        return DataTables::of(Course::all())
            ->addColumn('prices', function ($data) {
                return number_format($data->price) . " ₫";
            })
            ->addColumn('type', function ($data) {
                return $data->type->typeName;
            })
            ->addColumn('ptoName', function ($data) {
                return $data->user->fullName;
            })
            ->addColumn('numTrainee', function ($data) {
                return $data->courseMns->count();
            })
            ->addColumn('rate', function ($data) {
                if ($data->rate->count() != 0) {
                    return $data->rating;
                } else
                    return " 0 ";
            })
            ->addColumn('actions', function ($data) {
                return view('partials/actions', ['route' => 'courses', 'id' => $data->id, 'type' => 'only-show']);
            })->rawColumns(['actions'])->make(true);
    }


    public function courseDetail($id)
    {
        $course = Course::find($id);
        if (Auth::id() == $course->idUser) {
            $lesson = Lesson::where('idCourse', $id)->get();
            $countL = $lesson->count();
            $countT = Course_Manage::where('course_id', $id)->count();
            $price = $course->price * 9 / 10;
            $total = $countT * $price;
            return view('courses.show', ['course' => $course, 'lesson' => $lesson,
                'countL' => $countL, 'countT' => $countT,
                'total' => $total, 'price' => $price,
            ]);
        } else {
            return redirect()->route('pto.error');
        }
    }


    public function courseLockDt($id)
    {
        $course = Course::find($id);
        $user = Course::order_details($id);
        $trainees = Course_Manage::where('course_id', $id);
        return view('courses.course_lockDt', ['course' => $course, 'user' => $user, 'trainees' => $trainees]);
    }

    public function openLock(Request $request, $id)
    {
        $course = Course::find($id);
        if ($request->lock == 1) {
            $course->lock = 1;
            $course->status = 1 ;
        } 
        if ($request->lock == 0) {
            $course->lock = 0;
            $course->status = 0;
            $notify = new Notify();
            $notify->user_id = $course->user->id;
            $notify->pto_id = $course->user->id;
            $notify->status = 0;
            $notify->message = 'Khóa học: " ' . $course->courseName . ' " của bạn đã được mở ở trạng thái Chưa kích hoạt !';
            $notify->link = route('courses.deactive');
            $notify->save();
        } 
        if($request->lock == 2){
            $course->lock = 2 ;
            $course->status = 0 ;
            $notify = new Notify();
            $notify->user_id = $course->user->id;
            $notify->pto_id = $course->user->id;
            $notify->status = 0;
            $notify->message = 'Khóa học: " ' . $course->courseName . ' " của bạn đã bị đóng !';
            $notify->link = route('courses.index');
            $notify->save();
            $trainees = Course_Manage::where('course_id',$course->id)->get();
            foreach ($trainees as $trainee) {
                $notifies = new Notify();
                $notifies->user_id = $trainee->user_id;
                $notifies->seen = 0;
                $notifies->noti = 'Khóa học " ' . $course->courseName . ' " của bạn hiện đang đóng. Chúng tôi sẽ mở lại trong thời gian sớm nhất !';
                $notifies->route = route('courses.detail',$course->id);
                $notifies->save();
            }
        }
        $course->save();
        return redirect()->back()->with('thongbao', 'Xử lý trạng thái thành công');
    }


    public function showCourse($slug)
    {
        $course = Course::where('slug', $slug)->first();
        $rating = Rating::where('idCourse', $course->id)->get();

        if ($rating->count() != 0) {
            $a = 0;
            $rate = 0;
            foreach ($rating as $key => $r) {
                $a += $r->star;
            }
            $rate = ceil($a / $rating->count());
        } else {
            $rate = 0;
        }


        return view('master_layout.page.course_detail', ['course' => $course,
            'rate' => $rate,
            'rating' => $rating]);
    }


    public function showall()
    {

        $course = Course::where('status', 1)->get();

        $type = Type::all();
        return view('master_layout.page.course', ['course' => $course, 'type' => $type]);
    }

    public function myCourse()
    {
        $courseStudying = Course_Manage::where([
            ['user_id', Auth::id()],
            ['status', 0]
        ])->get();
        $courseDone = Course_Manage::where([
            ['user_id', Auth::id()],
            ['status', 1]
        ])->get();
        $orderCourse = Order::where([
            ['status', '<', '2'],
            ['status_active', '=', '0'],
            ['user_id', '=', Auth::id()],
        ])->orderBy('created_at', 'DESC')->get();
        return view('user_manage.course.myCourses',
            ['orderCourse' => $orderCourse,
                'courseStudying' => $courseStudying,
                'courseDone' => $courseDone
            ]);
    }

    public function detail($id)
    {
        $course = Course::find($id);

        $process = Course_Manage::where([
            ['course_id', $id],
            ['user_id', Auth::id()]
        ])->first();

        if (Auth::id() == $process->user_id) {
            $lessons = Course_Process::where([
                ['status', 1],
                ['user_id', Auth::id()],
            ])->get();
            $lessonUserLearn = [];
            foreach ($lessons as $lesson) {
                $lessonUserLearn[] = $lesson->lesson_id;
            }
            if ($process->process < 100)
                return view('user_manage.course.courseDetail', ['course' => $course, 'process' => $process, 'lessonUserLearn' => $lessonUserLearn]);
            else {
                $ratings = Rating::where('idCourse', $course->id)->get();
                $rate = Rating::where([
                    ['idUser', Auth::id()],
                    ['idCourse', $id]
                ])->first();
                return view('user_manage.course.courseDone', ['course' => $course, 'process' => $process,
                    'lessonUserLearn' => $lessonUserLearn, 'rate' => $rate, 'ratings' => $ratings]);
            }
        } else {
            return redirect()->route('pto.error');
        }

    }

    public function courseDone($id)
    {
        $cm = Course_Manage::where('course_id', $id)->first();

    }

    public function rating($id)
    {
        $course = Course::find($id);
        $ratings = Rating::where('idCourse', $course->id)->get();
        $rate = Rating::where([
            ['idUser', Auth::id()],
            ['idCourse', $id]
        ])->first();
        return view('user_manage.course.courseRating', ['course' => $course, 'rate' => $rate, 'ratings' => $ratings]);
    }

    public function storeRate(Request $request, $id)
    {

        $rate = new Rating();
        $rate->idUser = Auth::id();
        $rate->idCourse = $id;
        $rate->comment = $request->comment;
        $rate->star = $request->rate;
        $rate->status = 1;
        $rate->save();


        $course = Course::find($id);

        if ($course->rate->count() == 0) {
            $course->rating = $request->rate;
        } else {
            $a = 0;
            foreach ($course->rate as $r) {
                $a += $r->star;
            }
            $course->rating = $a / $course->rate->count();
        }
        $course->save();
        $noti = new Notify();
        $noti->status = 0;
        $noti->user_id = $course->user->id;
        $noti->pto_id = $course->user->id;
        $noti->message = Auth::user()->fullName . ' vừa đánh giá khóa học " ' . $course->courseName . ' " của bạn !';
        $noti->link = route('courses.courseDetail',$course->id);
        $noti->save();
        return redirect()->back()->with('thongbao1', 'Hoàn thành đánh giá');
    }

    public function dataTrainee($id)
    {
        return DataTables::of(Course_Manage::where('course_id', $id)->get())
            ->addIndexColumn()
            ->addColumn('nameTrainee', function ($data) {
                return $data->user->fullName;
            })
            ->addColumn('email', function ($data) {
                return $data->user->email;
            })
            ->addColumn('phone', function ($data) {
                return $data->user->phone;
            })
            ->addColumn('process', function ($data) {
                return $data->process;
            })
            ->make(true);
    }

    public function display()
    {

    }

}