<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Course;
use Carbon\Carbon;
use App\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rules\In;

class DashboardController extends Controller
{
    public function index()
    {
        $user = User::all();
//        $start = Input::get('start');
//        $end = Input::get('end');
//        $count = Order::whereBetween('created_at', [$start, $end])->count();
//        $count1 = User::whereBetween('created_at', [$start, $end])->count();
//        $today = Carbon::today();
//        $count = Order::whereBetween('created_at', [$today->startOfMonth(), $today->endOfMonth()])->count();
//        $count1 = User::whereBetween('created_at', [$today->startOfMonth(), $today->endOfMonth()])->count();
//        $count = Order::where('status', 1)->count();
//        $count1 = User::where('status', 1)->count();
//        return view('dashboard.dashboard',compact('user'),compact('count','count1'));
        return view('dashboard.dashboard');

    }

    public function data()
    {
        $start = Input::get('start');
        $end = Input::get('end');


        $startDate = Carbon::createFromFormat('d/m/Y H:i:s', $start . " 00:00:00");
        $endDate = Carbon::createFromFormat('d/m/Y H:i:s', $end . " 23:59:59");
        $objReturn = new \stdClass();

        if (Auth::user()->idRole == 1) {
            $countOrder = Order::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->count();
            $countUser = User::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->count();
//        $countPrice = OrderDetail::whereBetween('price', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->count();
            $purchases = DB::table('order_detail')
                ->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                ->sum('price');
            $countCourse = Course::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->count();

            $regis = DB::table('users')
                ->selectRaw("DATE(created_at),count(id)")
                ->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                ->groupBy('DATE(created_at)')
                ->get();
        } else {
            $course = Course::where('idUser',\Auth::id())->get();
            $courses=[];
            foreach ($course as $c){
                $courses[]= $c->id;
            }
//            dd($courses);
            if(count($courses)>0){
                $countOrder = OrderDetail::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->wherein('course_id',$courses)->count();
//              $countUser = User::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->where('id',Auth::id())->count();
//              $countPrice = OrderDetail::whereBetween('price', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->count();
                $countUser = DB::table('orders AS a')
                    ->leftJoin('order_detail AS b', 'a.id', '=', 'b.order_id')
                    ->whereBetween('a.created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                    ->wherein('b.course_id',$courses)
                    ->groupBy('a.user_id')
                    ->count();

                $purchases = DB::table('order_detail AS a')
                    ->leftJoin('orders AS b', 'a.order_id', '=', 'b.id')
                    ->whereBetween('a.created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                    ->wherein('a.course_id',$courses)
                    ->where('b.status',1)
                    ->sum('price');
                $countCourse = Course::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->wherein('id',$courses)->count();
                $regis = DB::table('users')
                    ->selectRaw("DATE(created_at),count(id)")
                    ->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                    ->groupBy('DATE(created_at)')
                    ->get();
            }else{
                $countOrder = 0;
            }

        }


        //       dd($startDate->toDateTimeString(),$endDate->toDateTimeString());


        $period = new \DatePeriod(
            $startDate,
            new \DateInterval('P1D'),
            $endDate
        );


        $dateList = [];
        foreach ($period as $day) {
//            echo $day->format('Y-m-d')."<br>";

            $objDate = new \stdClass();
            $objDate->date = $day->format('Y-m-d');
            $objDate->count = 0;
            $dateList[] = $objDate;
        }
        // dd($dateList);
        foreach ($regis as $re) {

            $date = 'DATE(created_at)';
            $total = 'count(id)';
            foreach ($dateList as $key => $day) {
                if ($day->date == $re->{$date}) {
                    $dateList[$key]->count = $re->$total;
                }
            }
//            echo $re->$date."<br>";
//            echo $re->$total."<br>";
        }


        $label = [];
        $data = [];
        foreach ($dateList as $date) {
            $label[] = $date->date;
            $data[] = $date->count;
        }
        $objReturn->countOrder = $countOrder;
        $objReturn->countUser = $countUser;
        $objReturn->purchases = number_format($purchases);
        $objReturn->countCourse = $countCourse;
        $objReturn->label = $label;
        $objReturn->data = $data;

        return response()->json($objReturn);
    }
}
