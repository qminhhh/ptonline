<?php

namespace App\Http\Controllers;

use App\Permissions;
use Illuminate\Http\Request;
use App\Role;
use App\RolePermissions;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        return view('role.index', ['role' => $role]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all();
        $permission = Permissions::all();
        return view('role.create', ['role' => $role, 'permissions' => $permission]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'roleName' => 'required',
                'slug' => 'required',
                'listPermission' => 'required',
            ],
            [
                'roleName.required' => 'Chưa nhập tên khóa học',
                'slug.required' => 'Chưa nhập tên slug',
                'listPermission.required' => 'Chưa nhập tên permission',
            ]);
//        dd($request->listPermission);
        $role = new Role;
        $role->roleName = $request->roleName;
        $role->slug = $request->slug;
        $role->save();
        foreach ($request->listPermission as $rp) {
            $rolePermissions = new RolePermissions;
            $rolePermissions->idRole = $role->id;
            $rolePermissions->idPermission = $rp;
            $rolePermissions->save();
        }


        return redirect()->route('admin.index')->with('thongbao', 'Đã thêm nhóm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return view('role.show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permissions::all();

        $role_list_id = [];
        $role_list = $role->RolePermissions;
        foreach ($role_list as $list) {
            $role_list_id[] = $list->idPermission;
        }
//        dd($role_list_id);
        return view('role.edit', ['role' => $role, 'permissions' => $permission, 'role_list_id' => $role_list_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'roleName' => 'required',
                'slug' => 'required',
                'listPermission' => 'required',
            ],
            [
                'roleName.required' => 'Chưa nhập tên khóa học',
                'slug.required' => 'Chưa nhập tên slug',
                'listPermission.required' => 'Chưa nhập tên permission',
            ]);
        $deletedRows = RolePermissions::where('idRole', $id)->delete();
        $role = Role::find($id);
        $role->roleName = $request->roleName;
        $role->slug = $request->slug;
        $role->save();
        foreach ($request->listPermission as $rp) {
            $rolePermissions = new RolePermissions;
            $rolePermissions->idRole = $role->id;
            $rolePermissions->idPermission = $rp;
            $rolePermissions->save();
        }

        return redirect()->route('admin.show', $id)->with('thongbao', 'Đã cập nhật nhóm thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect()->route('admin.index')->with('thongbao', 'Đã xóa nhóm thành công');
    }

    public function find(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $permissions = Permissions::search($term)->limit(5)->get();

        $formatted_permissions = [];

        foreach ($permissions as $permission) {
            $formatted_permissions[] = ['id' => $permissions->id, 'text' => $permissions->name];
        }

        return \Response::json($formatted_permissions);
    }


}
