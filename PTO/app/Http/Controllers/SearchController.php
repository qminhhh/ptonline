<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $name = $request->search;
        $searchCourse = Course::where('courseName', 'LIKE', "%$name%")->paginate(10);
        $searchPTO = User::where('fullName', 'LIKE', "%$name%")->where('idRole', 'LIKE', '2')->paginate(10);
        return view('pages.search', ['name' => $name, 'searchCourse' => $searchCourse,'searchPTO' => $searchPTO]);
    }
}
