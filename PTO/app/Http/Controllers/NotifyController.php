<?php

namespace App\Http\Controllers;

use App\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifies = Notify::where([
            ['pto_id', Auth::id()],
            ['message','<>',null]
        ])->orderBy('created_at', 'DESC')->get();
        $count = Notify::where([
            ['pto_id', Auth::id()],
            ['status',0],
            ['message','<>',null]
        ])->count();
        return view('master_layout.notify.notify', ['notifies' => $notifies,'count'=>$count]);
    }

    public function showNotify(){
        $notifies = Notify::where([
          ['user_id',Auth::id()],
            ['noti','<>',null]
        ])->orderBy('created_at', 'DESC')->get();
        $count = Notify::where([
            ['user_id',Auth::id()],
            ['seen',0],
            ['noti','<>',null]
        ])->count();
        return view('user_manage.notify.notify',['notifies' => $notifies,'count'=>$count]);
    }


    public function seen(Request $request,$id)
    {
        $notify = Notify::find($id);
        if ($notify->status == 0) {
            $notify->status = 1;
            $notify->save();
        }

        $link= $request->link;
        return redirect($link);
    }

    public function status(Request $request,$id)
    {
        $notify = Notify::find($id);
        if ($notify->seen == 0) {
            $notify->seen = 1;
            $notify->save();
        }
        $route= $request->route;
        return redirect($route);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
