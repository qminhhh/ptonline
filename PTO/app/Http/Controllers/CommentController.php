<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Lesson;
use App\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $comment = new Comment();
        $notify = new Notify();

        $lesson = Lesson::where('id', $id)->first();
        $comment->idUser = Auth::id();
        $comment->idLesson = $id;
        $comment->content = $request->comment;
        $comment->parent_id = $request->parent_id;

        $comment->save();

        $notify->user_id = Auth::id();
        $notify->pto_id = $lesson->course->user->id;
        $notify->link = route('lessons.showNotifice',$lesson->id);
        $notify->comment_id = $comment->id;
        if ($request->parent_id == 0) {
            $notify->message = Auth::user()->fullName . ' đã bình luận về một bài học của bạn( Bài: ' . $lesson->lessonName . '- Khóa: ' . $lesson->course->courseName . ' )';
        } else {
            $notify->message = Auth::user()->fullName . ' đã phản hồi một bình luận trong bài học của bạn( Bài: ' . $lesson->lessonName . '- Khóa: ' . $lesson->course->courseName . ' )';
        }
        $notify->status = 0;
        $notify->save();
        return redirect()->back();
    }


    public function saveCmt(Request $request, $id)
    {
        $comment = new Comment();
        $notify = Notify::where('comment_id', $request->comment_id)->first();
        $lesson = Lesson::where('id', $id)->first();
        $comment->idUser = Auth::id();
        $comment->idLesson = $id;
        $comment->content = $request->comment;
        $comment->parent_id = $request->parent_id;
        $comment->save();


        $notify->route = route('lessons.show', $lesson->id);
        $notify->noti = 'PTO ' . Auth::user()->fullName . ' đã trả lời một bình luận của bạn( Bài: ' . $lesson->lessonName . '- Khóa: ' . $lesson->course->courseName . ' )';
        $notify->seen = 0;
        $notify->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::destroy($id);
        Comment::where('parent_id', $id)->delete();
        Notify::where('comment_id', $id)->delete();
        return redirect()->back();
    }
}
