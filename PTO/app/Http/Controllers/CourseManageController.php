<?php

namespace App\Http\Controllers;

use App\Course_Manage;
use Illuminate\Http\Request;
use App\Course_Process;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use function Sodium\compare;
use Yajra\DataTables\Facades\DataTables;

class CourseManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lessonDone($id)
    {
        $cp = Course_Process::where([
            ['lesson_id', $id],
            ['user_id', Auth::id()]
        ])->first();
        $cp->status = 1;
        $cp->save();


        $lesson_id = [];
        $lessonCount = $cp->lesson->course->lessons;
        foreach ($lessonCount as $lc) {
            $lesson_id[] = $lc->id;
        }

        $a = Course_Process::whereIn('lesson_id', $lesson_id)->where('user_id', Auth::id())->where('status', 1)->count();
        $b = $cp->lesson->course->lessons->count();

        $cm = Course_Manage::where([
            ['course_id', $cp->lesson->course->id],
            ['user_id', Auth::id()]
        ])->first();
        $cm->process = $a / $b * 100;
        if ($cm->process == 100) {
            $cm->status = 1;
        }
        $cm->save();
        if ($cm->process == 100) {
            return redirect()->route('courses.detail', $cp->lesson->course->id)->with('thongbao', 'Chúc mừng bạn đã hoàn thành bài học ! Đánh giá khóa học nhé !');
        } else {
            return redirect()->route('courses.detail', $cp->lesson->course->id)->with('thongbao', 'Đã hoàn thành bài học !');
        }
    }


}
