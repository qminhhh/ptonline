<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Yajra\DataTables\Facades\DataTables;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master_layout.page.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $contact = new Contact();
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->content = $request->contents;
        $contact->status = 0;

        $params = $request->all();


        if ($params['g-recaptcha-response'] == null) {
            return response()->json([
                'code' => 'error',
                'message' => 'Chưa nhập capcha '
            ]);
        }
        $captchaParams = [
            'secret' => config('app.google_secret'),
            'response' => $params['g-recaptcha-response']
        ];
        //dd($captchaParams);
        $verifyCaptcha = json_decode($this->verifyCaptcha($captchaParams));
        if ($verifyCaptcha->success) {
            $contact->save();
            return response()->json([
                'code' => 'success',
                'message' => 'Gửi thành công'
            ]);
        } else {
            return response()->json([
                'code' => 'error',
                'message' => 'Gửi thất bại'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        if ($contact->status == 0) {
            $contact->status = 1;
            $contact->save();
        }
        return view('contact.show', ['contact' => $contact]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::destroy($id);
        return redirect()->route('contact.list')->with('thongbao', 'Đã xóa thành công');
    }


    public function verifyCaptcha($params)
    {
        $verifyUrl = env('GOOGLE_CAPTCHA_VERIFY_LINK', 'https://www.google.com/recaptcha/api/siteverify');
        $ch = curl_init($verifyUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function data()
    {
        return DataTables::of(Contact::all())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'contact', 'id' => $data->id, 'type' => 'show-delete']);
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('contact.index');
    }
}
