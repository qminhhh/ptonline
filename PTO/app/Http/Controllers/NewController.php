<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Yajra\DataTables\Facades\DataTables;

class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::where('status', 1)->get();
        $category = Category::all();
        return view('master_layout.page.new', ['news' => $news, 'category' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('news.create', ['category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title' => 'required|unique:news,title|min:3',
                'abbreviate' => 'required|min:30',
                'contents' => 'required|min:60',
                'image' => 'required'
            ],
            [
                'title.required' => 'Chưa nhập tiêu đề',
                'title.min' => 'Tên tiêu đề ít nhất 3 kí tự',
                'title.unique' => 'Tên tiêu đề không được trùng',
                'abbreviate.required' => 'Chưa nhập nội dung tóm tắt',
                'abbreviate.min' => 'Nội dung tóm tắt ít nhất 30 kí tự',
                'contents.required' => 'Chưa nhập mục nội dung chi tiết',
                'contents.min' => 'Nội dung chi tiết ít nhất 60 kí tự',
                'image.required' => 'Chưa chọn hình ảnh',
            ]);
        $new = new News();
        $new->category_id = $request->category;
        $new->title = $request->title;
        $new->abbreviate = $request->abbreviate;
        $new->content = $request->contents;
        $new->status = $request->has('public') ? true : false;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->title) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/news');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $new->image = "/upload/news/" . $name;
        }
        $new->save();
        $new->slug = str_slug($new->title . ' ' . $new->id, '-');
        $new->save();
        return redirect()->route('new.list')->with('thongbao', 'Đã thêm tin tức thành công');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = News::find($id);

        return view('news.show', compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = News::find($id);
        $category = Category::all();
        return view('news.edit', ['new' => $new, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'title' => 'required|min:3',
                'abbreviate' => 'required|min:30',
                'contents' => 'required|min:60',
            ],
            [
                'title.required' => 'Chưa nhập tiêu đề',
                'title.min' => 'Tên tiêu đề ít nhất 3 kí tự',
                'abbreviate.required' => 'Chưa nhập nội dung tóm tắt',
                'abbreviate.min' => 'Nội dung tóm tắt ít nhất 30 kí tự',
                'contents.required' => 'Chưa nhập mục nội dung chi tiết',
                'contents.min' => 'Nội dung chi tiết ít nhất 60 kí tự',
            ]);
        $new = News::find($id);
        $new->category_id = $request->category;
        $new->title = $request->title;
        $new->abbreviate = $request->abbreviate;
        $new->content = $request->contents;
        $new->status = $request->has('public') ? true : false;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = str_slug($request->title) . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/news');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $new->image = "/upload/news/" . $name;
        }
        $new->save();
        $new->slug = str_slug($new->title . ' ' . $new->id, '-');
        $new->save();
        return redirect()->route('new.list')->with('thongbao', 'Đã cập nhật tức thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::destroy($id);
        return redirect()->route('new.list')->with('thongbao', 'Đã xóa thành công');
    }

    public function data()
    {
        return DataTables::of(News::all())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'new', 'id' => $data->id, 'type' => '']);
        })->addColumn('cat_name', function ($data) {
            return $data->category->categoryName;
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('news.index');
    }


    public function new_detail($slug)
    {
        $new = News::where('slug', $slug)->first();
        $category = Category::all();
        return view('master_layout.page.new_detail', ['new' => $new,'category'=>$category]);
    }
}
