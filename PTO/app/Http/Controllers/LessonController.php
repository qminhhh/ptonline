<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Course;
use App\Course_Manage;
use App\Course_Process;
use App\Lesson;
use App\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use League\Flysystem\Exception;
use Whoops\Exception\ErrorException;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showNotifice($id)
    {
        $lesson = Lesson::find($id);
        $comment = Comment::where([
            ['idLesson', $id],
            ['parent_id', 0]
        ])->orderBy('created_at', 'DESC')->get();
        return view('lessons.notify', ['comment' => $comment, 'lesson' => $lesson]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->id;
        return view('lessons.create', ['id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'lessonName' => 'required|min:3',
                'contentL' => 'required|min:10',
                'linkVideo' => 'required',
                'menu' => 'required|min:10',
            ],
            [
                'lessonName.required' => 'Chưa nhập tên bài học',
                'lessonName.min' => 'Tên bài học ít nhất 3 kí tự',
                'contentL.required' => 'Chưa nhập nội dung',
                'contentL.min' => 'Nội dung bài học ít nhất 10 kí tự',
                'linkVideo.required' => 'Chưa nhập link video',
                'menu.required' => 'Chưa nhập thực đơn',
                'menu.min' => 'Mục tiêu ít nhất 10 kí tự',
            ]);
        $lesson = new Lesson();
        $lesson->lessonName = $request->lessonName;
        $lesson->idCourse = $request->idCourse;
        $lesson->content = $request->contentL;
        $url = $request->video;
        if ($url != null) {
            try {
                parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                $lesson->video = $my_array_of_vars['v'];
            } catch (\Exception $exception) {
                return redirect()->back()->withInput(Input::all())->with('message', 'Hiện tại hệ thống đang bảo trì nên
                chỉ nhận được youtube,bạn vui lòng đăng lại link youtube nhé !');
            }

        } else {
            $lesson->video = $url;
        }
        $lesson->linkVideo = $request->linkVideo;
        $lesson->menu = $request->menu;
        $lesson->status = 1;
//        dd($request->all());
        $lesson->save();
        return redirect()->route('courses.courseDetail', $request->idCourse)->with('thongbao', 'Đã tạo bài học thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Responsedetail
     */
    public function show($id)
    {
        $status = Course_Process::where([
            ['lesson_id', $id],
            ['user_id', Auth::id()]
        ])->first();
        $lesson = Lesson::find($id);
        $comment = Comment::where([
            ['idLesson', $id],
            ['parent_id', 0]
        ])->orderBy('created_at', 'DESC')->get();
        return view('user_manage.lesson.show', ['lesson' => $lesson, 'status' => $status, 'comment' => $comment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $idCourse = $request->idCourse;
        $course = Course::find($idCourse);
        $lesson = Lesson::find($id);
        $user_id = $lesson->course->user->id;
        $comment = Comment::where([
            ['idLesson', $id],
            ['parent_id', 0]
        ])->orderBy('created_at', 'DESC')->get();
        if (Auth::id() == $user_id) {
            return view('lessons.edit', ['lesson' => $lesson, 'id' => $idCourse, 'comment' => $comment]);
        } else {
            return redirect()->route('pto.error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'lessonName' => 'required|min:3',
                'contentL' => 'required|min:10',
                'linkVideo' => 'required',
                'menu' => 'required|min:10',
            ],
            [
                'lessonName.required' => 'Chưa nhập tên bài học',
                'lessonName.min' => 'Tên bài học ít nhất 3 kí tự',
                'contentL.required' => 'Chưa nhập nội dung',
                'contentL.min' => 'Nội dung bài học ít nhất 10 kí tự',
                'linkVideo.required' => 'Chưa nhập link video',
                'menu.required' => 'Chưa nhập thực đơn',
                'menu.min' => 'Mục tiêu ít nhất 10 kí tự',
            ]);

        $lesson = Lesson::find($id);
        $user_id = $lesson->course->user->id;
        if (Auth::id() == $user_id) {
            $lesson->lessonName = $request->lessonName;
            $lesson->content = $request->contentL;
            $url = $request->video;
            if ($url != null) {
                try {
                    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
                    $lesson->video = $my_array_of_vars['v'];
                } catch (\Exception $exception) {
                    return redirect()->back()->withInput(Input::all())->with('message', 'Hiện tại hệ thống đang bảo trì nên
                chỉ nhận được youtube,bạn vui lòng đăng lại link youtube nhé !');
                }

            } else {
                $lesson->video = $url;
            }
            $lesson->linkVideo = $request->linkVideo;
            $lesson->menu = $request->menu;
            $lesson->save();

            $trainees = Course_Manage::where('course_id', $lesson->course->id)->get();
            foreach ($trainees as $trainee) {
                $notify = new Notify();
                $notify->user_id = $trainee->user_id;
                $notify->seen = 0;
                $notify->noti = 'PTO ' . $lesson->course->user->fullName . ' đã chỉnh sửa bài học: ' . $request->lessonName . ' (Khóa học: ' . $lesson->course->courseName . ') .Hãy cập nhật nội dung mới nhất !';
                $notify->route = route('lessons.show', $lesson->id);
                $notify->save();
            }
            return redirect()->route('courses.courseDetail', $request->idCourse)->with('thongbao', 'Đã cập nhật bài học thành công');
        } else {
            return redirect()->route('pto.error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Lesson::destroy($id);
        return redirect()->route('courses.show', $request->idCourse)->with('thongbao', 'Đã xóa bài học thành công');
    }
}
