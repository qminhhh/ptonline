<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Routing\Route;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'categoryName'=>'required|unique:categories,categoryName|min:3',
            ],
            [
                'categoryName.required'=>'Chưa nhập tên thể loại',
                'categoryName.unique'=>'Tên thể loại không được trùng',
                'categoryName.min'=>'Tên thể loại ít nhất 3 kí tự',
            ]);
        $category = new Category();
        $category->categoryName = $request->categoryName;
        $category->save();
        return redirect()->route('category.list')->with('thongbao','Đã thêm thể loại thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        return view('master_layout.page.new_cate',['category'=>$category,'categories'=>$categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'categoryName'=>'required|unique:categories,categoryName|min:3',
            ],
            [
                'categoryName.required'=>'Chưa nhập tên thể loại',
                'categoryName.unique'=>'Tên thể loại không được trùng',
                'categoryName.min'=>'Tên thể loại ít nhất 3 kí tự',
            ]);

        $category = Category::find($id);
        $category->categoryName = $request->categoryName;
        $category->save();
        return redirect()->route('category.list')->with('thongbao','Chỉnh sửa thể loại thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect()->route('category.list')->with('thongbao','Đã xóa thành công');
    }

    public function data()
    {
        return DataTables::of(Category::all())->addColumn('actions', function ($data) {
            return view('partials/actions', ['route' => 'category', 'id' => $data->id, 'type' => 'edit-delete']);
        })->rawColumns(['actions'])->make(true);
    }

    public function list()
    {
        return view('category.index');
    }
}
