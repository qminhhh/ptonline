<?php

namespace App\Http\Controllers;

use App\Mail\OrderPaypal;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\PayPalService as PayPalSvc;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use App\User;
use App\Order;
use App\OrderDetail;

class PayPalTestttController extends Controller
{
    private $paypalSvc;

    public function __construct(PayPalSvc $paypalSvc)
    {
//        parent::__construct();

        $this->paypalSvc = $paypalSvc;
    }

    public function index()
    {
        $data = [];
        $i = 1 ;
        foreach(Cart::content() as $key=>$row){
            $item =  [
                'name' => $row->name,
                'quantity' => 1,
                'price' => $row->price/20000,
                'sku' => $i++,
            ];
            $data[]=$item;
        };

        $transactionDescription = "Tobaco";

        $paypalCheckoutUrl = $this->paypalSvc
            // ->setCurrency('eur')
            ->setReturnUrl(url('paypal/status'))
            // ->setCancelUrl(url('paypal/status'))
            ->setItem($data)
            // ->setItem($data[0])
            // ->setItem($data[1])
            ->createPayment($transactionDescription);

        if ($paypalCheckoutUrl) {
            return redirect($paypalCheckoutUrl);
        } else {
            dd(['Error']);
        }
    }

    public function status()
    {
        $paymentStatus = $this->paypalSvc->getPaymentStatus();
        if($paymentStatus == false){
            return redirect()->route('order.showcard')->with('thongbao', 'Bạn đã hủy thanh toán online ! Hãy chọn hình thức thanh toán khác !');
        }else{
            $order = new Order();
            $user = User::find(Auth::id());
            $order->user_id = Auth::id();
            $order->total = str_replace(',', '', Cart::subtotal());
            $order->status = 1;
            $order->active_code = rand(100000, 999999);
            $order->save();
            foreach (Cart::content() as $row) {
                $order_detail = new OrderDetail();
                $order_detail->course_id = $row->id;
                $order_detail->order_id = $order->id;
                $order_detail->price = $row->price;
                $order_detail->save();
            }
            Mail::to($user->email)->send(new OrderPaypal($order,$user));
            Nexmo::message()->send([
                'to'   => $user->phone,
                'from' => '841684603855',
                'text' => 'Ban da mua thanh cong don hang #'.$order->id.' tren trang PTO.com.
                           Tong tien : '.number_format($order->total)
                           .' Ma kich hoat khoa hoc cua ban la : '.$order->active_code,
            ]);
            return redirect()->route('courses.myCourse');
        }
    }

    public function paymentList()
    {
        $limit = 10;
        $offset = 0;

        $paymentList = $this->paypalSvc->getPaymentList($limit, $offset);

        dd($paymentList);
    }

    public function paymentDetail($paymentId)
    {
        $paymentDetails = $this->paypalSvc->getPaymentDetails($paymentId);

        dd($paymentDetails);
    }
}
