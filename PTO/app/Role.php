<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table='roles';

    public function users(){
        return $this->hasMany('App\User','idRole','id');

    }
//    public function permissions(){
//        return $this->hasManyThrough('App\Permissions','App\RolePermissions','idRole',
//            'id','id','id');
//    }
    public function RolePermissions(){
        return $this->hasMany('App\RolePermissions','idRole','id');


    }
}
