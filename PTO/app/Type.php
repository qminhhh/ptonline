<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "types";

    public function courses()
    {
        return $this->hasMany('App\Course', 'type_id', 'id');

    }
}
