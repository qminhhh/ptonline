<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\User;

class Address extends Model
{
    protected $table = "address";

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }


}
