<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    protected $table='role_permissions';

    public function Permission(){
        return $this->belongsTo('App\Permissions','idPermission','id');

    }
}
