<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Process extends Model
{
    protected $table='course_process';

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function lesson(){
        return $this->belongsTo('App\Lesson','lesson_id','id');
    }


}
