<?php

namespace App\Providers;

use App\Notify;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Role;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function($view) {
                if (Auth::check()){
                $role = Role::find(Auth::user()->idRole);
                $permissions=[];
                foreach ($role->RolePermissions as $r){
                    $permissions[]=$r->Permission->slug;

                }
                $view->with('menu', $permissions);


                    $notifies = Notify::where([
                        ['pto_id', Auth::id()],
                        ['status',0],
                        ['message','<>',null]
                    ])->count();

                    $view->with('g_notifies', $notifies);

                    $countNoti = Notify::where([
                        ['user_id',Auth::id()],
                        ['seen',0],
                        ['noti','<>',null]
                    ])->count();
                    $view->with('u_notifies', $countNoti);

            }




        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
