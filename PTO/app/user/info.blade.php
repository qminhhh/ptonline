@extends('res')
@section('content')

    <div class="card-outside">
        <div class="cart_status row">
            <div class="col-sm-3 menu_left">
                <div class="row avatar_user">
                    <div>
                        <p>
                            <img src="images/image_cart/a.jpg">
                        </p>
                    </div>
                    <div class="textUser">
                        <div>
                            <label>Yen ne</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </div>
                            <div>
                                <a href="#"> <label class="pointer-user">Sửa thông tin</label></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="change-user-infor">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            </div>
                            <div>
                                <a href="#"> <label class="pointer-user">Thông tin cá nhân</label></a>
                            </div>
                        </div>
                        <div class="menu-child">
                            <label style="color: #FF6600; text-transform: capitalize">Tài khoản của tôi</label>
                        </div>
                        <div class="menu-child">
                            <a href="change_password.html"><label>Đổi mật khẩu</label></a>
                        </div>
                    </div>
                    <div class="lesson-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-book" aria-hidden="true"></i>
                            </div>
                            <div>
                                <a href="table-learning.html"> <label class="pointer-user">Khoá học</label></a>
                            </div>
                        </div>
                    </div>
                    <div class="chart-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-line-chart" aria-hidden="true"></i>
                            </div>
                            <div>
                                <a href="chart_body.html"> <label class="pointer-user">Thể trạng cơ thể</label></a>
                            </div>
                        </div>
                    </div>
                    <div class="payment-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-usd" aria-hidden="true"></i>
                            </div>
                            <div>
                                <a href="#"> <label class="pointer-user">Quản lí thanh toán</label></a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <!--user information-->
            <div class="menu-right-chgpass">
                <div class="col-sm-12">
                    <div>
                        <div>
                            <strong><label>Tài khoản của tôi</label></strong>
                        </div>
                        <div class="textChgPass">
                            <label>Quản lí thông tin cá nhân để bảo mật tài khoản</label>
                        </div>
                    </div>
                    <div class="second-part-chgpass row">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-3 input-with-label">
                                    <label>email</label>
                                </div>
                                <div class=" input-with-label-right">
                                    <label>ir******@gmail.com</label>
                                </div>
                                <div class=" input-with-label-right" style="text-decoration: underline">
                                    <a href="#">Thay đổi</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-with-label">
                                    <label>số điện thoại</label>
                                </div>
                                <div class="input-with-label-right">
                                    <label>*********778</label>
                                </div>
                                <div class=" input-with-label-right" style="text-decoration: underline">
                                    <a href="">Thay đổi</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-with-label">
                                    <label>tên shop</label>
                                </div>
                                <div class="col-sm-6 input-with-content">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-with-label">
                                    <label>tên shop</label>
                                </div>
                                <div class="col-sm-4 radio-outside">
                                    <div class=" row">
                                        <div class="col-sm-1 radio-but" style="font-size: 16px">
                                            <input type="radio" name="sex">
                                        </div>
                                        <div class="radio-text">Nam</div>
                                        <div class="col-sm-1 radio-but">
                                            <input type="radio" name="sex">
                                        </div>
                                        <div class="radio-text">Nữ</div>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 input-with-label">
                                    <label>ngày sinh</label>
                                </div>
                                <div class="col-sm-8 input-with-content row">
                                    <div class="col-sm-4">
                                        <select class="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control">
                                            <option>Tháng 1</option>
                                            <option>Tháng 2</option>
                                            <option>Tháng 3</option>
                                            <option>Tháng 4</option>
                                            <option>Tháng 5</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control">
                                            <option>2018</option>
                                            <option>2017</option>
                                            <option>2016</option>
                                            <option>2015</option>
                                            <option>2014</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 choose-image">
                            <div>
                                <p>
                                    <img src="images/image_cart/a.jpg">
                                </p>
                            </div>
                            <div>
                                <input type="file" name="uploadfile" id="img" style="display:none;"/>
                                <div>
                                    <label for="img">chọn ảnh</label>
                                </div>

                            </div>
                            <div>
                                <label>Dụng lượng file tối đa 1 MB</label>
                                <label>Định dạng:.JPEG, .PNG</label>
                            </div>

                        </div>


                    </div>

                    <div class="col-sm-3" style="margin-left: 10px">
                        <button class="btn btn-danger">Lưu</button>
                    </div>
                </div>
            </div>


            <!--END user information-->
        </div>

    </div>
@endsection