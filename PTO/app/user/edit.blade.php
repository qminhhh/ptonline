@extends('master_layout.admin.index')
@section('titles')
    Chỉnh sửa thông tin
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa thông tin
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('users.edit',$user)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="{{route('users.update',$user->id)}}">
                    @method('POST')
                    @csrf
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group row">
                                <label for="fullName"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Họ và tên') }}</label>

                                <div class="col-md-6">
                                    <input id="fullName" type="text"
                                           class="form-control{{ $errors->has('fullName') ? ' is-invalid' : '' }}"
                                           name="fullName" placeholder="{{$user->fullName}}"
                                           value="{{$user->fullName}}">

                                    @if ($errors->has('fullName'))
                                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('fullName') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Ảnh đại diện') }}</label>

                                <div class="col-md-6">
                                    <input type="image" src="{{$user->image}}" width="200px">
                                    <input id="image" type="file"
                                           class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                           name="image">

                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label>Giới tính</label>
                                </div>
                                <div class="col-md-6">
                                    <input name="gender" value="0"
                                           @if($user->gender == 0)
                                           {{"checked"}}
                                           @endif
                                           type="radio">Nữ
                                    <input name="gender" value="1"
                                           @if($user->gender == 1)
                                           {{"checked"}}
                                           @endif
                                           type="radio">Nam
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="phone"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Số điện thoại') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text"
                                           class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           name="phone" placeholder="{{$user->phone}}" value="{{$user->phone}}">

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="DOB"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Ngày sinh nhật') }}</label>

                                <div class="col-md-6">
                                    <input id="DOB" type="date"
                                           class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}"
                                           name="DOB" placeholder="{{$user->DOB}}" value="{{$user->DOB}}">

                                    @if ($errors->has('DOB'))
                                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('DOB') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 pull-right">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Chỉnh sửa') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection