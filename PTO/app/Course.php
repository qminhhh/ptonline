<?php

namespace App;
use App\User;
use App\Lesson;
use App\Order;
use App\OrderDetail;
use DB;



use Illuminate\Database\Eloquent\Model;


class Course extends Model
{
    protected $table='courses';

    public function lessons(){
        return $this->hasMany('App\Lesson','idCourse','id');
    }

    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }

    public function type(){
        return $this->belongsTo('App\Type','type_id','id');
    }


    public static function  order_details($id){
        $users = DB::table('courses')
            ->join('order_detail','courses.id','=','order_detail.course_id')
            ->join('orders','order_detail.order_id', '=', 'orders.id')
            ->join('users','orders.user_id','=','users.id')
            ->where('courses.id', '=', $id)
            ->groupby('users.id')
            ->select('users.*')
            ->get();
        return $users;
    }


    public function rate(){
        return $this->hasMany('App\Rating','idCourse','id');
    }

    public function courseMns(){
        return $this->hasMany('App\Course_Manage','course_id','id');
    }
}
