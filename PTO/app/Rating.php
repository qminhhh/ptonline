<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'rate';

    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }

    public function course(){
        return $this->belongsTo('App\Course','idCourse','id');
    }
}
