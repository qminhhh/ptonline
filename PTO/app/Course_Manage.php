<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Manage extends Model
{
    protected $table='course_manage';

    public function order(){
        return $this->belongsTo('App\Order','order_id','id');
    }

    public function course(){
        return $this->belongsTo('App\Course','course_id','id');
    }

     public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }


}
