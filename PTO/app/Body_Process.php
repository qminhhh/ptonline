<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Body_Process extends Model
{
    protected $table='body_process';

    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }
}
