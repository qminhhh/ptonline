<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Lesson;

class Comment extends Model
{
    protected $table='comment';

    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }

    public function lesson(){
        return $this->belongsTo('App\Lesson','idLesson','id');
    }

    public function replies(){
        return $this->hasMany('App\Comment','parent_id','id');
    }
}
