<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table="order_detail";

    public function order(){
        return $this->belongsTo('App\Order','order_id','id');
    }


    public function course(){
        return $this->belongsTo('App\Course','course_id','id');
    }
}
