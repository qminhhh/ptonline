<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Address;
use App\Order;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullName', 'userName','image', 'password', 'gender', 'email', 'phone', 'DOB', 'height',
        'weight', 'bust', 'wais', 'hips','status','introduce','email_token','idRole','slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function courses(){
        return $this->hasMany('App\Course','idUser','id');
    }
//    public function role(){
//        return $this->hasMany('App\Role','idRole','id');
//    }
    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'idRole');
    }
    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        // Check if the user is a root account
        if($this->have_role->name == 'admin') {
            return true;
        }
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }
    private function getUserRole()
    {
        return $this->role()->getResults();
    }
    private function checkIfUserHasRole($need_role)
    {
        return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
    }

    public function address(){
        return $this->hasManyThrough('App\Address','App\Order','user_id','order_id','id');
    }

    public function bodyProcesses(){
        return $this->hasMany('App\User','idUser','id');
    }

    public function rate(){
        return $this->hasMany('App\Rating','idUser','id');
    }

    public function courseManage(){
        return $this->hasMany('App\Course_Manage','user_id','id');
    }

    public function notifies(){
        return $this->hasMany('App\Notify','user_id','id');
    }

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }
}
