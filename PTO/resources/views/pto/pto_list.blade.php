@extends('master_layout.admin.index')
@section('titles')
    Danh sách PTO
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Huấn Luyện Viên
            <small>danh sách</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách huấn luyện viên</div>
                    <div class="panel-body">
                        <table id="ptoo-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ảnh</th>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Ngày đăng ký</th>
                                <th>Số khóa học</th>
                                <th>Chi tiết</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#ptoo-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('pto.dataPT') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: function (data) {
                            if (data.image != null) {
                                return "<img width=\"100px\" src='" + data.image + "'>";
                            } else {
                                return "";
                            }
                        }, name: 'image'
                    },
                    {data: 'fullName', name: 'fullName'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'numCourse', name: 'numCourse'},
                    {data: 'actions', name: 'actions'},
                ],
            });
        });
    </script>
@endpush
