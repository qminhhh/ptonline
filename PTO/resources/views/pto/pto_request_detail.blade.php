@extends('master_layout.admin.index')
@section('content')
    <div class="box box-primary">
        <div class="box-body">

            <div class="form-group row">
                <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Ảnh đại diện') }}</label>

                <div class="col-md-6">
                    <input type="image" src="{{$user->image}}" width="200px">
                </div>
            </div>

            <div class="form-group row">

                <label for="firstName" class="col-md-4 col-form-label text-md-right">{{ __('Họ Tên') }}</label>

                <div class="col-md-6">
                    <label>{{$user->firstName}} {{$user->lastName}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="userName" class="col-md-4 col-form-label text-md-right">{{ __('Tên đăng nhập') }}</label>

                <div class="col-md-6">
                    <label> {{$user->userName}}</label>
                </div>
            </div>

            <div class="form-group row">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Giới tính</label>
                    </div>
                    <div class="col-md-6">
                        @if($user->gender == 0)
                            <label class="text-md-right">Nữ</label>
                        @endif

                        @if($user->gender == 1)
                            <label class="text-md-right">Nam</label>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Địa chỉ email') }}</label>

                <div class="col-md-6">
                    <label>{{$user->email}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Số điện thoại') }}</label>

                <div class="col-md-6">
                    <label>{{$user->phone}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="height" class="col-md-4 col-form-label text-md-right">{{ __('Chiều cao') }}</label>

                <div class="col-md-6">
                    <label>{{$user->height}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="weight" class="col-md-4 col-form-label text-md-right">{{ __('Cân nặng') }}</label>

                <div class="col-md-6">
                    <label>{{$user->weight}}</label>
                </div>
            </div>


            <div class="form-group row">
                <label for="bust" class="col-md-4 col-form-label text-md-right">{{ __('Số đo 3 vòng') }}</label>

                <div class="col-md-6">
                    <label>{{$user->bust}} - {{$user->wais}} - {{$user->hips}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="DOB" class="col-md-4 col-form-label text-md-right">{{ __('Giới thiệu bản thân') }}</label>

                <div class="col-md-6">
                    <label>{!!$user->introduce!!}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="DOB" class="col-md-4 col-form-label text-md-right">{{ __('Lý do bạn muốn trở thành PTO') }}</label>

                <div class="col-md-6">
                    <label>{!! $user->reason !!}</label>
                </div>
            </div>

            <div class="form-group row">
                <label for="DOB" class="col-md-4 col-form-label text-md-right">{{ __('Bằng cấp, chứng chỉ') }}</label>

                <div class="col-md-6">
                    <label>{!!$user->certificate!!}</label>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4 pull-right">
                    <a href="" class="btn btn-success btn-flat">Phê duyệt</a>
                    <a href="" class="btn btn-danger btn-flat">Không phê duyệt</a>
                </div>
            </div>
        </div>

    </div>
@endsection