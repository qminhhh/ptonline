
@extends('master_layout.admin.index')
@section('titles')
    Chỉnh sửa PTO
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Chỉnh sửa thông tin PTO
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('pto.edited',$user)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <form method="POST" enctype="multipart/form-data" action="{{route('ptoupdate',$user->id)}}">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="userName" value="{{$user->userName}}">
                            <div class="form-group">
                                <label for="fullName" >{{ __('Họ và tên') }}</label>

                                <div>
                                    <input id="fullName" type="text" class="form-control{{ $errors->has('fullName') ? ' is-invalid' : '' }}"
                                           name="fullName" placeholder="{{$user->fullName}}" value="{{$user->fullName}}">

                                    @if ($errors->has('fullName'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('fullName') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="image">{{ __('Ảnh đại diện') }}</label>

                                <div>
                                    <input type="image" src="{{$user->image}}" width="200px">
                                    <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  >
                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                                             <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <label>Giới tính</label>
                                </div>
                                <div>
                                    <input name="gender" value="0"
                                           @if($user->gender == 0)
                                           {{"checked"}}
                                           @endif
                                           type="radio">Nữ
                                    <input name="gender" value="1"
                                           @if($user->gender == 1)
                                           {{"checked"}}
                                           @endif
                                           type="radio">Nam
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone">{{ __('Số điện thoại') }}</label>

                                <div>
                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           name="phone" placeholder="{{$user->phone}}" value="{{$user->phone}}">

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="DOB">{{ __('Ngày sinh nhật') }}</label>

                                <div>
                                    <input id="DOB" type="date" class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}"
                                           name="DOB" placeholder="{{$user->DOB}}" value="{{$user->DOB}}">

                                    @if ($errors->has('DOB'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('DOB') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Giới thiệu bản thân</label>
                                <textarea name="introduce" id="introduce" class="form-control ckeditor {{ $errors->has('introduce') ? ' is-invalid' : '' }}"
                                          rows="1">{{$user->introduce}}</textarea>
                                @if ($errors->has('introduce'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('introduce') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Chiều cao (cm)</label>
                                <input id="height" type="text"
                                       class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" value="{{$user->height}}">
                                @if ($errors->has('height'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('height') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Cân nặng (kg) </label>
                                <input id="weight" type="text"
                                       class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" value="{{$user->weight}}">
                                @if ($errors->has('weight'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Vòng 1 (cm)</label>
                                <input id="bust" type="text"
                                       class="form-control{{ $errors->has('bust') ? ' is-invalid' : '' }}" name="bust" value="{{$user->bust}}">
                                @if ($errors->has('bust'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('bust') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Vòng 2 )(cm)</label>
                                <input id="bust" type="text"
                                       class="form-control{{ $errors->has('wais') ? ' is-invalid' : '' }}" name="wais" value="{{$user->wais}}">
                                @if ($errors->has('wais'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wais') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Vòng 3 (cm)</label>
                                <input id="bust" type="text"
                                       class="form-control{{ $errors->has('hips') ? ' is-invalid' : '' }}" name="hips" value="{{$user->hips}}">
                                @if ($errors->has('hips'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('hips') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Bằng cấp, chứng chỉ</label>
                                <textarea name="certificate" id="certificate" class="form-control ckeditor {{ $errors->has('certificate') ? ' is-invalid' : '' }}"
                                          rows="1"  value="{{old('certificate')}}" >{{$user->certificate}}</textarea>
                                @if ($errors->has('certificate'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('certificate') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Lý do để chúng tôi chọn bạn làm PTO</label>
                                <textarea name="reason" id="reason" class="form-control ckeditor {{ $errors->has('reason') ? ' is-invalid' : '' }}"
                                          rows="1"  value="{{old('reason')}}" >{{$user->reason}}</textarea>
                                @if ($errors->has('reason'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('reason') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection