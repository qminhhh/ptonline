@extends('res')
@section('titles')
    Đăng ký PTO
@endsection
@section('content')
    <div id="container" class="gym-outside" style="padding: 30px">
        <div class="w3layoutscontaineragileits">
            <h2>Đăng ký để trở thành PTO</h2>
            <form method="POST" enctype="multipart/form-data" action="{{route('pto.update',Auth::id())}}">
                @method('PUT')
                @csrf
                <div class="form-group row">
                    <textarea rows="8" id="introduce" type="text"
                              style="background: none; border: 2px solid #537b35; color: #fff"
                              class="form-control{{ $errors->has('introduce') ? ' is-invalid' : '' }}"
                              name="introduce"
                              placeholder="Giới thiệu bản thân">{{ old('introduce') }}</textarea>

                    @if ($errors->has('introduce'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('introduce') }}</strong>
                             </span>
                    @endif
                </div>
                <div class="form-group row">
                    <input id="phone" type="text"
                           class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                           name="phone" value="{{old('phone')}}" placeholder="Điện thoại">

                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input placeholder="Ngày sinh" class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}"
                           type="text" onfocus="(this.type='date')" onblur="(this.type='text')" name="DOB" id="DOB"
                           value="{{old('DOB')}}">
                    {{--<input id="DOB" type="date"--}}
                    {{--class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}" name="DOB"--}}
                    {{--value="{{old('DOB')}}" placeholder="Ngày sinh">--}}

                    @if ($errors->has('DOB'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('DOB') }}</strong>
                            </span>
                    @endif
                </div>


                <div class="form-group row">
                    <input id="height" type="text"
                           class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}"
                           name="height" value="{{ old('height') }}" placeholder="Chiều cao (cm)">

                    @if ($errors->has('height'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('height') }}</strong>
                             </span>
                    @endif
                </div>
                <div class="form-group row">
                    <input id="weight" type="text"
                           class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}"
                           name="weight" value="{{ old('weight') }}" placeholder="Cân nặng (kg)">

                    @if ($errors->has('weight'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('weight') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="bust" type="text"
                           class="form-control{{ $errors->has('bust') ? ' is-invalid' : '' }}"
                           name="bust" value="{{ old('bust') }}" placeholder="Vòng 1 (cm)">

                    @if ($errors->has('bust'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('bust') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="wais" type="text"
                           class="form-control{{ $errors->has('wais') ? ' is-invalid' : '' }}"
                           name="wais" value="{{ old('wais') }}" placeholder="Vòng 2 (cm)">

                    @if ($errors->has('wais'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('wais') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="hips" type="text"
                           class="form-control{{ $errors->has('hips') ? ' is-invalid' : '' }}"
                           name="hips" value="{{ old('hips') }}" placeholder="Vòng 3 (cm)">

                    @if ($errors->has('hips'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('hips') }}</strong>
                            </span>
                    @endif
                </div>


                <div class="form-group row">
                    <textarea rows="8" id="certificate" type="text"
                              style="background: none;  border: 2px solid #537b35;color: #fff"
                              class="form-control{{ $errors->has('certificate') ? ' is-invalid' : '' }}"
                              name="certificate"
                              placeholder="Bằng cấp, chứng chỉ">{{ old('certificate') }}</textarea>

                    @if ($errors->has('certificate'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('certificate') }}</strong>
                             </span>
                    @endif
                </div>

                <div class="form-group row">
                    <textarea rows="8" id="reason" type="text"
                              style="background: none; border: 2px solid #537b35;color: #fff"
                              class="form-control{{ $errors->has('reason') ? ' is-invalid' : '' }}"
                              name="reason"
                              placeholder="Lý do để chúng tôi chọn bạn trở thành PTO">{{ old('reason') }}</textarea>

                    @if ($errors->has('reason'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('reason') }}</strong>
                             </span>
                    @endif
                </div>
                <ul class="agileinfotickwthree">
                    <li>
                        <span>
                            <input type="checkbox" class="{{ $errors->has('rule') ? ' is-invalid' : '' }}"
                                   style="display: block" name="rule" value="1">
                        </span>
                        <span class="test" style="margin-left: 20px">
                            <a href="{{route('pto.rule')}}" class="reset-pass"
                               style="float: left;margin-left: 18px;color: #fff;font-size: 15px" target="_blank">
                                Tôi đã đọc và đồng ý với điều khoản => <strong style="color: #0ade3a">TẠI ĐÂY</strong>
                            </a>
                        </span>
                         <br>
                        <div class="form-group row">
                            @if ($errors->has('rule'))
                                <span class="invalid-feedback" style="display: block">
                                <strong>{{ $errors->first('rule')}}</strong>
                             </span>
                            @endif
                        </div>
                    </li>
                </ul>
                <div class="aitssendbuttonw3ls">
                    <input type="submit" value="Đăng ký">

                </div>
            </form>
        </div>
    </div>
@endsection



