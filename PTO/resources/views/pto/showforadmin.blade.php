@extends('master_layout.admin.index')
@section('titles')
    Thông tin PTO
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Thông tin PTO
        </h1>
        <ol class="breadcrumb">
            {{ Breadcrumbs::render('pto.accept', $user) }}

        </ol>
        <br/>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group row">

                            <label for="firstName"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Họ và tên') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->fullName}}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="userName"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Tên đăng nhập') }}</label>

                            <div class="col-md-6">
                                <label> {{$user->userName}}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Ảnh đại diện') }}</label>

                            <div class="col-md-6">
                                <input type="image" src="{{$user->image}}" width="200px">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label>Giới tính</label>
                                </div>
                                <div class="col-md-6">
                                    @if($user->gender == 0)
                                        <label class="text-md-right">Nữ</label>
                                    @endif

                                    @if($user->gender == 1)
                                        <label class="text-md-right">Nam</label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Đại chỉ email') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->email}}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Số điện thoại') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->phone}}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DOB"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Ngày sinh nhật') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->DOB}}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DOB"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Giới thiệu bản thân') }}</label>

                            <div class="col-md-6">
                                <label>{!!$user->introduce!!}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="height"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Chiều cao') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->height}} (cm)</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="weight"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Cân nặng') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->weight}} (kg)</label>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="bust" class="col-md-4 col-form-label text-md-right">{{ __('Vòng 1') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->bust}} (cm)</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="wais" class="col-md-4 col-form-label text-md-right">{{ __('Vòng 2') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->wais}} (cm)</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="hips" class="col-md-4 col-form-label text-md-right">{{ __('Vòng 3') }}</label>

                            <div class="col-md-6">
                                <label>{{$user->hips}} (cm)</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DOB"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Bằng cấp, chứng chỉ') }}</label>

                            <div class="col-md-6">
                                <label>{!!$user->certificate!!}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DOB"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Lý do để chúng tôi cho bạn làm PTO') }}</label>

                            <div class="col-md-6">
                                <label>{!! $user->reason !!}</label>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4 pull-right">
                                <form class="form-group" action="{{route('pto.update1',$user->id)}}" method="POST">
                                    @csrf
                                    {{method_field('PUT') }}
                                    <button id class="btn btn-success btn-flat"><i
                                                class="glyphicon glyphicon-ok"></i> Đồng ý
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection