@extends('master_layout.admin.index')
@section('titles')
    Danh sách PTO chờ duyệt
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Danh sách PTO chờ duyệt
        </h1>
        <ol class="breadcrumb">
            {{ Breadcrumbs::render('pto.list') }}
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="panel-body">
                <div>
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                </div>
                <table id="ptotable" class="table table-bordered table-hover dataTable" style="width:100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ảnh</th>
                        <th>Tên tài khoản</th>
                        <th>Ngày đăng ký</th>
                        <th>Phê duyệt</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>

        $(document).ready(function () {

            $(document).on('click', '.btn-danger', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this).parent('form');
                swal({
                    title: "Chú ý: bạn muốn xóa?",
                    text: "Bạn sẽ không thể khôi phục khi đã xóa!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No! don't delete",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                }).then(function (result) {
                        if (result) {
                            form.submit();
                        } else {
                            swal("Bản ghi vẫn an toàn!");
                        }

                    },
                );
            });
            $('#ptotable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('pto.data1') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: function (data) {
                            if (data.image != null) {
                                return "<img width=\"100px\" src='" + data.image + "'>";
                            } else {
                                return "";
                            }
                        }, name: 'image'
                    },
                    {data: 'userName', name: 'userName'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>
@endpush


