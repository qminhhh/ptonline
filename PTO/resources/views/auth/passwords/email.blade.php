@extends('res')
@section('title')
    Quên mật khẩu
@endsection
@section('content')
    <!--login form-->
    <div id="container" class="gym-outside" style="padding: 30px">
        <div class="w3layoutscontaineragileits">
            <div>
                @if (session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
                @endif
            </div>
            <h2>QUÊN MẬT KHẨU</h2>
            <form method="POST" action="{{ route('password.email')}}">
                @csrf
                <div class="form-group row">
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="aitssendbuttonw3ls">
                    <input type="submit" value="Nhận mật khẩu">
                </div>
            </form>
        </div>
    </div>

    <!--end login form-->

@endsection


