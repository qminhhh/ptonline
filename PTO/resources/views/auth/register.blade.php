
@extends('res')
@section('titles')
    Đăng ký
@endsection
@section('content')
    <!--login form-->
    <div id="container" class="gym-outside" style="padding: 30px">
        <div class="w3layoutscontaineragileits">
            <h2>Đăng ký</h2>
            <form action="{{route('register')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <input id="fullName" type="text"
                           class="form-control{{ $errors->has('fullName') ? ' is-invalid' : '' }}"
                           name="fullName" value="{{ old('fullName') }}" placeholder="Họ và tên">

                    @if ($errors->has('fullName'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('fullName') }}</strong>
                             </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="userName" type="text"
                           class="form-control{{ $errors->has('userName') ? ' is-invalid' : '' }}"
                           name="userName" value="{{ old('userName') }}" placeholder="Tên đăng nhập">

                    @if ($errors->has('userName'))
                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('userName') }}</strong>
                            </span>
                    @endif
                </div>

                {{--<div class="form-group row">--}}
                    {{--<input id="image" type="file"--}}
                           {{--class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">--}}

                    {{--@if ($errors->has('image'))--}}
                        {{--<span class="invalid-feedback">--}}
                            {{--<strong>{{ $errors->first('image') }}</strong>--}}
                            {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                <div class="form-group row" >
                    <select id="gender" name="gender" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}">
                        <option value="" hidden  style="color: #495057">Giới tính </option>
                        <option value="0"  style="color: black" {{old('gender')=='0' ? 'selected':''}}>Nữ</option>
                        <option value="1"  style="color: black" {{old('gender')=='1' ? 'selected':''}}>Nam</option>
                    </select>
                    @if ($errors->has('gender'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="email" type="text"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" placeholder="Mật khẩu">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group row">
                    <input id="password-confirm" type="password"
                           class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                           name="password_confirmation" placeholder="Xác nhận lại mật khẩu">

                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                    @endif
                </div>

                {{--<div class="form-group row">--}}
                    {{--<input id="phone" type="text"--}}
                           {{--class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"--}}
                           {{--name="phone" value="{{old('phone')}}" placeholder="Điện thoại">--}}

                    {{--@if ($errors->has('phone'))--}}
                        {{--<span class="invalid-feedback">--}}
                            {{--<strong>{{ $errors->first('phone') }}</strong>--}}
                            {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                {{--<div class="form-group row">--}}
                    {{--<input id="DOB" type="date"--}}
                           {{--class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}" name="DOB"--}}
                           {{--value="{{old('DOB')}}" placeholder="Ngày sinh">--}}

                    {{--@if ($errors->has('DOB'))--}}
                        {{--<span class="invalid-feedback">--}}
                            {{--<strong>{{ $errors->first('DOB') }}</strong>--}}
                            {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}


                <div class="aitssendbuttonw3ls">
                    <input type="submit" value="Đăng ký">
                    <p> Bạn đã có tài khoản? <span>&rarr;</span> <a class="w3_play_icon1" href="{{route('login')}}">
                            Click vào đây</a></p>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>

    <!--end login form-->

@endsection


