@extends('res')
@section('titles')
    Đăng nhập
@endsection
@section('content')
    <!--login form-->
    <div id="container" class="gym-outside" style="padding: 30px">
        <div class="w3layoutscontaineragileits">
            <div>
                @if(session('thongbao'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{session('thongbao')}}
                    </div>
                @endif
            </div>
            <h2>Đăng nhập</h2>
            <div class="social">
                <a id="google_login" class="circle google" href="{{ route('login.google')}}">
                    <i class="fa fa-google-plus fa-fw"></i>
                </a>
                {{--<a id="facebook_login" class="circle facebook" href="{{ route('login.facebook')}}">--}}
                    {{--<i class="fa fa-facebook fa-fw"></i>--}}
                {{--</a>--}}
            </div>
            <form method="POST" action="{{ route('login')}}">
                @csrf
                <div class="form-group row">
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group row">
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" value="{{ old('password') }}" placeholder="Mật khẩu">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                {{--<div class="form-group row">--}}
                    {{--<div class="col-md-6 offset-md-4">--}}
                        {{--<div class="checkbox">--}}
                            {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                            {{--<span class="test" style="margin-left: -15px">Ghi nhớ đăng nhập</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <ul class="agileinfotickwthree">
                    <li>
                        <span><input type="checkbox" style="display: block" name="remember" {{ old('remember') ? 'checked' : '' }}></span>
                        <span class="test" style="margin-left: 20px">Ghi nhớ đăng nhập</span>
                        <a href="{{route('password.request')}}" class="reset-pass">Quên mật khẩu?</a>
                    </li>
                </ul>

                <div class="aitssendbuttonw3ls">
                    <input type="submit" value="Đăng nhập" >
                    <p>Đăng ký tài khoản mới<span>&rarr;</span> <a class="w3_play_icon1" href="{{route('register')}}">
                            Click vào đây</a></p>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>

    <!--end login form-->

@endsection


