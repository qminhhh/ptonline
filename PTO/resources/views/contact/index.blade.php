@extends('master_layout.admin.index')
@section('titles')
    Danh sách liên hệ
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liên Hệ
            <small>danh sách</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('contact.list')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách liên hệ</div>
                    <div class="panel-body">
                        <table id="contact-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Nội dung</th>
                                <th>Trạng thái</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-danger', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this).parent('form');
                swal({
                    title: "Bạn có muốn xóa liên hệ?",
                    text: "Dữ liệu sẽ mất!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Xóa!",
                    cancelButtonText: "Hủy",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                }).then(function (result) {
                        if (result) {
                            form.submit();
                        } else {
                            swal("Liên hệ không xóa!");
                        }

                    },
                );
            });
            $('#contact-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('contact.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {
                        data: function (data) {

                            var str = data.content;
                            var maxLength = 50;

                            var trimmedString = str.substr(0, maxLength);

                            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))

                            return trimmedString + '...';

                        }, name: 'content'
                    },
                    {
                        data: function (data) {
                            if (data.status != null) {
                                if (data.status == 0) {
                                    return '<span class="badge bg-blue">Chưa xem</span>';
                                }
                                else {
                                    return '<span class="badge bg-green">Đã xem</span>';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>
@endpush
