@extends('master_layout.admin.index')
@section('titles')
    Chi tiết liên hệ
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liên Hệ
            <small>chi tiết</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('contact.show',$contact)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Chi tiết liên hệ</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><i class="fa fa-envelope margin-r-5"></i> Email : </strong>

        <p class="text-muted">
              {{$contact->email}}
        </p>

        <hr>

        <strong><i class="fa fa-phone margin-r-5"></i> Số điện thoại</strong>

        <p class="text-muted">
            {{$contact->phone}}
        </p>

        <hr>

        <strong><i class="fa fa-send margin-r-5"></i> Nội dung </strong>

        <p class="text-muted">
            {!! $contact->content !!}
        </p>

    </div>
    <!-- /.box-body -->
</div>
            </div>
        </div>
    </section>

@endsection