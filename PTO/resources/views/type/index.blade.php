@extends('master_layout.admin.index')
@section('titles')
    Thể loại khóa học
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thể loại
            <small>danh sách</small>
        </h1>
        <br>
        <a title="Show" href="{{route('type.create')}}" class="btn btn-xs btn-info"><i
                    class="glyphicon glyphicon-plus"></i>Thêm thể loại</a>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('type.list')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Danh sách thể loại
                    </div>
                    <div class="panel-body">
                        <table id="type-table" class="table table-bordered table-hover dataTable"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên thể loại</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-danger', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this).parent('form');
                swal({
                    title: "Bạn có muốn xóa thể loại",
                    text: "Dữ liệu sẽ mất!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Xóa!",
                    cancelButtonText: "Hủy",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                }).then(function (result) {
                        if (result) {
                            form.submit();
                        } else {
                            swal("Thể loại không xóa!");
                        }

                    },
                );
            });
            $('#type-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('type.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'typeName', name: 'typeName'},
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>
@endpush
