@extends('master_layout.admin.index')
@section('content')

    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thể Loại
            <small>thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('type.create')}}</li>
        </ol>
    </section>
    <section class="content" >
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>+</strong> Thêm thể loại</h3>
                    </div>
                    <form action="{{route('type.store')}}" enctype="multipart/form-data" method="POST" role="form" >
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tên thể loại</label>
                                <input type="text" class="form-control{{ $errors->has('typeName') ? ' is-invalid' : '' }}" id="" placeholder="Nhập tên thể loại" name="typeName">
                                @if ($errors->has('typeName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('typeName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tạo</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
