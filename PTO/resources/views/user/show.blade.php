@extends('master_layout.admin.index')
@section('titles')
    Thông tin người dùng
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thông tin người dùng
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('users.show',$user)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="{{ route('register') }}">
                    @csrf
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group row">


                                <label for="fullName"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Họ và tên') }}</label>

                                <div class="col-md-6">
                                    <label>{{$user->fullName}}</label>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="userName"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Tên đăng nhập') }}</label>

                                <div class="col-md-6">
                                    <label> {{$user->userName}}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Ảnh đại diện') }}</label>

                                <div class="col-md-6">
                                    <input type="image" src="{{$user->image}}" width="200px">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label>Giới tính</label>
                                    </div>
                                    <div class="col-md-6">
                                        @if($user->gender == 0)
                                            <label class="text-md-right">Nữ</label>
                                        @endif

                                        @if($user->gender == 1)
                                            <label class="text-md-right">Nam</label>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Đại chỉ email') }}</label>

                                <div class="col-md-6">
                                    <label>{{$user->email}}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Số điện thoại') }}</label>

                                <div class="col-md-6">
                                    <label>{{$user->phone}}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="DOB"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Ngày sinh nhật') }}</label>

                                <div class="col-md-6">
                                    <label>{{$user->DOB}}</label>
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 pull-right">
                                    <a href="{{route('users.edit',$user->id)}}" class="btn btn-default btn-flat">
                                        Chỉnh sửa thông tin
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection