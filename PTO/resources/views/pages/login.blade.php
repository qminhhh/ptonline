@extends('res')
@section('titles')
    Đăng nhập
@endsection
@section('content')
    <!--login form-->
    <div id="container" class="gym-outside" style="padding: 30px">
        <div class="w3layoutscontaineragileits">
            <h2>Đăng nhập</h2>
            <form method="POST" action="{{ route('login')}}">
                @csrf
                <div class="form-group row">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" placeholder="Email">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group row">
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" placeholder="Mật khẩu">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="test" style="margin-left: -42px">Remember me</span>
                        </div>
                    </div>
                </div>

                <div class="aitssendbuttonw3ls">
                    <input type="submit" value="Đăng nhập">
                    <p>Đăng ký tài khoản mới<span>&rarr;</span> <a class="w3_play_icon1" href="{{route('regis.index')}}">
                            Click vào đây</a></p>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>

    <!--end login form-->

@endsection


