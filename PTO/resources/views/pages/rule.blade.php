@extends('res')
@section('titles')
    Điều khoản
@endsection
@section('content')
    <div id="container" class="gym-outside" style="padding: 165px">
        <div class="w3layoutscontaineragileits" style="width: 65%">
            <div class="panel-heading"><h1>ĐIỀU KHOẢN DỊCH VỤ</h1></div>
            <br/>
            <div class="panel-body text-left">
                1. GIỚI THIỆU<br/>
                <b>PTO.com</b> là một trang web cung cấp dịch vụ nền tảng trực tuyến kết nối người học và những người dạy thể hình với nhau nhằm mang đến cơ hội tìm kiếm học viên và thầy tập cho chính mình. Các khóa học được tạo lập trực tiếp giữa Học viên và PTO. <b>PTO.com</b> có trách nhiệm liên kết hai bên liên quan và đảm bảo quyền lợi hai bên trong suốt thời gian của khóa học.<br/>
                Điều khoản dịch vụ này là một phần không thể tách rời của “Quy chế hoạt động trang mạng trực tuyến <b>PTO.com</b>” dành cho những ứng viên muốn đăng ký trở thành PTO trên website.<br/>
                1.1	Chào mừng bạn đến với <b>PTO.com</b>.<br/>
                1.2	Trước khi ứng tuyển đăng ký làm PTO tại trang web của chúng tôi, vui lòng đọc kỹ các Điều Khoản Dịch Vụ dưới đây và toàn văn “Quy chế hoạt động trang mạng trực tuyến <b>PTO.com</b>” để hiểu rõ quyền lợi và nghĩa vụ hợp pháp của mình.<br/>
                1.3	Dịch Vụ chúng tôi cung cấp hoặc sẵn có bao gồm<br/>
                (a) Trang Web<br/>
                (b) các dịch vụ cung cấp bởi Trang Web dành cho PTO<br/>
                (c) tất cả các thông tin, đường dẫn, tính năng, số liệu, văn bản, hình ảnh, biểu đồ, tags, nội dung, chương trình, phần mềm. Bất kỳ tính năng mới nào được bổ sung đều thuộc phạm vi điều chỉnh của Điều Khoản Dịch Vụ. Điều Khoản Dịch Vụ này là hướng dẫn cơ bản khi bạn sử dụng các dịch vụ cung cấp bởi <b>PTO.com</b>.<br/>
                1.4 Trước khi trở thành thành viên chính thức trong đội ngũ PTO, bạn cần đọc và chấp nhận mọi điều khoản và điều kiện được quy định trong/liên quan đến Điều Khoản Dịch Vụ và Chính Sách Bảo Mật.<br/><br/>
                2. NHỮNG ĐIỀU PTO KHÔNG ĐƯỢC PHÉP THỰC HIỆN:<br/>
                •	Tải lên, đăng, chuyển giao hoặc công khai bất cứ nội dung nào trái pháp luật, có hại, đe dọa, lạm dụng, quấy rối, nói xấu, khiêu dâm, bôi nhọ, xâm phạm riêng tư người dùng khác, phân biệt chủng tộc, dân tộc hoặc bất kỳ hành vi gây khó chịu nào khác;<br/>
                •	Gây tổn hại cho trẻ vị thành niên dưới bất kỳ hình thức nào;<br/>
                •	Sử dụng dịch vụ để mạo danh bất kỳ cá nhân/ tổ chức nào, hoặc xuyên tạc cá nhân/ tổ chức;<br/>
                •	Sửa đổi tiêu đề hoặc chỉnh sửa định dạng để che giấu nguồn gốc của bất kỳ nội dung nào của dịch vụ;<br/>
                •	Sử dụng Dịch Vụ cho mục đích lừa đảo.<br/>
                •	Có hành vi phá hoại hệ thống xếp hạng hoặc ghi nhận phản hồi của<b>PTO.com</b><br/>
                •	Cố tình đảo ngược, tháo gỡ, hack các công cụ của Dịch Vụ (hoặc bất cứ hợp phần nào); phá bỏ hàng rào công nghệ mã hóa, hàng rào an ninh liên quan đến các Dịch Vụ của <b>PTO.com</b> hoặc các thông tin được chuyển giao, xử lý, lưu giữ bởi <b>PTO.com</b><br/>
                <b>PTO.com</b> có quyền (không bắt buộc) sàng lọc, từ chối, xóa, gỡ bỏ hoặc di chuyển những nội dung có sẵn trên Trang Web, bao gồm tất cả các nội dung hoặc thông tin bạn đã đăng. Ngoài ra, <b>PTO.com</b> có quyền gỡ bỏ những nội dung<br/>
                (i)	xâm phạm đến Điều Khoản Dịch Vụ<br/>
                (ii)	bị Người Dùng khác khiếu nại/ tố cáo<br/>
                (iii)	vi phạm quyền sở hữu trí tuệ được thông báo hoặc yêu cầu từ phía cơ quan có thẩm quyền<br/>
                3. VI PHẠM ĐIỀU KHOẢN DỊCH VỤ<br/>
                Việc vi phạm những quy định này có thể dẫn tới những hậu quả sau:<br/>
                • Bị xóa danh mục khóa học<br/>
                • Giới hạn quyền sử dụng tài khoản<br/>
                • Đình chỉ và chấm dứt quyền sử dụng tài khoản<br/>
                • Cáo buộc hình sự;<br/>
                • Áp dụng biện pháp dân sự bao gồm nhưng không giới hạn khiếu nại bồi thường thiệt hại hoặc áp dụng biện pháp khẩn cấp tạm thời.<br/>
                Nếu bạn phát hiện Người Dùng có hành vi vi phạm Điều Khoản Dịch Vụ, vui lòng liên hệ qua email pto_support@gmail.com .<br/><br/>

                4. QUYỀN LỢI DÀNH CHO PTO TRÊN WEBSITE<br/>
                <b>PTO.com</b> hỗ trợ cho các PTO nơi tìm kiếm các học viên tiềm năng, cung cấp một hệ thống đầy đủ chức năng và dễ dàng sử dụng trong việc quản lý học viên, quản lý khóa học và quản lý tài chính của mình. Đồng thời, <b>PTO.com</b> không ngừng đẩy mạnh việc phát triển trên toàn mạng lưới giáo dục và thương mại điện tử, giúp quảng bá các khóa học và toàn thể PTO đến rộng rãi toàn xã hội.
                Mỗi PTO sau khi đăng tải một khóa học bất kì, dưới sự kiểm duyệt chất lượng của đội ngũ quản trị, mỗi khóa học sau khi được giao dịch thành công với Học viên, PTO SẼ NHẬN ĐƯỢC 90% HỌC PHÍ KHÓA HỌC.<br/><br/>
                Tuy nhiên, có một số trường hợp ngoại lệ như sau:<br/>
                1.	PTO có tổng lượt đánh giá cao nhất mỗi tháng được nhận 150% với 5 khóa học có giá trị cao nhất trong tổng số khóa học đã bán trong tháng của PTO đó.<br/>
                2.	PTO đăng số khóa học nhiều nhất mỗi tháng được nhận 150% với 3 khóa học bất kỳ do BQT lựa chọn trong tổng số khóa học của PTO đó.<br/>
                3.	PTO đăng tải nội dung phản cảm bị Người dùng khác tố cáo.<br/>
                -	Trường hợp vi phạm lần 1: xóa khóa học có nội dung xấu, trừ 20% doanh thu toàn bộ khóa học được bán trong tháng đó.<br/>
                -	Trường hợp vi phạm lần 2: vĩnh viễn bị xóa tài khoản trên PTO.com, mọi liên hệ khác như khóa học và giao dịch cũng không tồn tại.<br/><br/>

                KHI SỬ DỤNG DỊCH VỤ HAY TẠO TÀI KHOẢN TẠI <b>PTO.com</b>, BẠN ĐÃ HOÀN TOÀN CHẤP NHẬN NHỮNG ĐIỀU KHOẢN ĐƯỢC QUY ĐỊNH TRONG THỎA THUẬN NÀY, BAO GỒM NHỮNG ĐIỀU KHOẢN, ĐIỀU KIỆN, VÀ CHÍNH SÁCH BỔ SUNG CÓ LIÊN QUAN TỚI/CÓ SẴN THEO CÁC ĐƯỜNG DẪN.<br/>
                NẾU BẠN KHÔNG ĐỒNG Ý VỚI NHỮNG ĐIỀU KHOẢN TRÊN, VUI LÒNG KHÔNG SỬ DỤNG DỊCH VỤ HOẶC TRUY CẬP VÀO TRANG WEB.

            </div>
        </div>
    </div>
    </div>
@endsection
