@extends('welcome')
@section('titles')
    Tìm kiếm
@endsection
@section('content')
    <!-- Title page -->


    <!-- Content page -->
    <section class="bg0 p-t-75 p-b-120" style="background-color: #f1f1f1">
        <div class="container">
            {{--<div class="p-b-148">--}}
            <form class="wrap-search-header flex-w p-l-15"
                  style="width: 1110px;height: 65px;margin-bottom: 75px;border-radius: 23px"
                  action="{{route('sreach')}}" method="get">
                @csrf
                @method("get")
                <button class="flex-c-m trans-04" type="submit">
                    <i class="zmdi zmdi-search"></i>
                </button>
                <input class="plh3" type="text" name="search" placeholder={{$name}}>
            </form>
            {{--</div>--}}
        </div>
        <div style="background-color: #fff!important;">
            @if($searchCourse->count() != 0)
                <div class="container">
                    <h1> {!!"Tìm thấy " .$searchCourse->count()." khóa học"!!}</h1>
                    @foreach($searchCourse as $s)
                        <div class="p-b-63 row">
                            <div class="col-md-4">
                                <a href="{{route('courses.showCourse',$s->slug)}}" class="hov-img0 how-pos5-parent">
                                    <img src="{{$s->image}}" alt="IMG-BLOG">
                                </a>
                            </div>
                            <div class="col-md-8">

                                <h4 class="p-b-15">
                                    <a href="{{route('courses.showCourse',$s->slug)}}"
                                       class="ltext-108 cl2 hov-cl1 trans-04">
                                        {!! $s->courseName !!}
                                    </a>
                                </h4>

                                <p class="stext-117 cl6">
                                    {!! $s->introduce !!}
                                </p>

                                <div class="flex-w flex-sb-m p-t-18">
                                    <a href="{{route('courses.showCourse',$s->slug)}}"
                                       class="stext-101 cl2 hov-cl1 trans-04 m-tb-10">
                                        Chi tiết

                                        <i class="fa fa-long-arrow-right m-l-9"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
        @endif
        <div style="background-color: #f1f1f1">
            @if($searchPTO->count() != 0)
                <div class="container">
                    <h1> {!!"Tìm thấy " .$searchPTO->count() ." PTO"!!}</h1>
                    @foreach($searchPTO as $pto)
                        <div class="p-b-63 row">
                            <div class="col-md-4">
                                <a href="{{route('ptodetail',$pto->slug)}}" class="hov-img0 how-pos5-parent">
                                    <img src="{{$pto->image}}" alt="IMG-BLOG">
                                </a>
                            </div>
                            <div class="col-md-8">

                                <h4 class="p-b-15">
                                    <a href="{{route('ptodetail',$pto->slug)}}"
                                       class="ltext-108 cl2 hov-cl1 trans-04">
                                        {!! $pto->fullName !!}
                                    </a>
                                </h4>

                                <p class="stext-117 cl6">
                                    {!! $pto->introduce!!}
                                </p>

                                <div class="flex-w flex-sb-m p-t-18">

                                    <a href="{{route('ptodetail',$pto->slug)}}"
                                       class="stext-101 cl2 hov-cl1 trans-04 m-tb-10">
                                        Chi tiết

                                        <i class="fa fa-long-arrow-right m-l-9"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>
        </div>
        @endif
    </section>

@endsection