@extends('res')
@section('titles')
    Xác nhận email
@endsection
@section('content')
    <div id="container" class="gym-outside" style="padding: 165px">
        <div class="w3layoutscontaineragileits" style="width: 55%">
            <div class="panel-heading">Đăng ký xác nhận</div>
            <div class="panel-body">
                Email của bạn đã được xác minh thành công. Bấm vào đây để <a href="{{route('login')}}">đăng nhập</a>
            </div>
        </div>
    </div>
    </div>
@endsection

