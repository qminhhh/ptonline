@extends('welcome')
@section('title')
    Thanh toán
@endsection
@section('content')

    <!--mail hien thi-->
    <div  class="card-outside">
        <div class="cart_status">
            <div style="margin-left: 15px">
                <label class="text-uppercase "><strong>Xác nhận mua khóa học thành công</strong></label>
            </div>
            <br>
            <div style="margin-left: 15px">
                <label class=""> Kính chào quý khách <strong>{{$order->name}}</strong>,</label>
                <br>
                <label for="">
                    <strong>PTO.com </strong> vừa nhận được đơn hàng của quý khách đặt ngày
                    <strong>{{$order->created_at}}</strong> với hình thức thanh toán là <strong>Thanh toán khi nhận hàng</strong>.
                    Chúng tôi sẽ gửi thông báo đến quý khách qua một email khác ngay khi sản phẩm
                    được giao cho đơn vị vận chuyển.
                </label>
            </div>
            <br>
            <div style="margin-left: 15px">
                <label class=""> <strong>Đơn hàng được giao đến</strong>
                </label>
                <label for="">
                    {{$order->name}} <br>
                    {{$order->address}} {{$order->district}} {{$order->province}}<br>
                    Phone:  {{$order->phone}}
                </label>
            </div>
            <br>
            <div style="margin-left: 15px">
                <label class=""> <strong>Chi tiết đơn hàng</strong>
                </label>
                <br>
                <label class=" ">Bạn đã mua {{Cart::count()}} khóa
                    học thành công : </label>
                <br>
                @foreach(Cart::content() as $row)
                    <div style="margin-left: 15px">
                        <label class="text-uppercase "> {{$row->name}} - {{$row->price}} ₫</label>
                        <hr>
                    </div>
                @endforeach
                <label class="text-uppercase"> Tổng tiền : <strong>{{Cart::subtotal()}} ₫ </strong></label>
            </div>
            <br>
            <div style="margin-left: 15px">
                <label class=""> “Mã kích hoạt khoá học” sẽ được gửi tới địa chỉ của bạn qua
                    đường chuyển phát nhanh(COD), thu tiền trực tiếp với nhân
                    giao vận. Thời gian vận chuyển có thể lên tới 3 ngày và muộn nhất là 1
                    tuần. Quý khách vui lòng chuẩn bị sẵn số tiền mặt tương ứng để thuận tiện cho việc thanh toán.
                </label>
            </div>
            <div style="margin-left: 15px">
                <label class="">Trân trọng,
                </label>
                <br>
                <label for=""><strong>PTO team</strong></label>
            </div>

        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.btn-add').click(function(e) {
                e.preventDefault();
                var data = {
                    id:$(this).data('id'),
                };
                $.ajax({
                    url: '{{route('delete-cart')}}',
                    type: 'post',
                    data: data,
                })
                    .done(function() {
                        window.location.reload();
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });


            });


        });
    </script>
@endpush