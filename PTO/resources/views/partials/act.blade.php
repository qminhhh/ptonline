<form class="form-group" action="{{route("{$route}.delete",['id'=>$id])}}" method="POST">
    @csrf
    {{method_field('PUT') }}
    <a title="Chi tiết" href="{{ route("{$route}.accept",['id'=>$id]) }}" class="btn btn-xs btn-info"><i
                class="glyphicon glyphicon-edit"></i>Chi tiết</a>
    <button id class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i>Xóa</button>
</form>
