@if($type == 'only-show')
    <a title="Show" href="{{ route("{$route}.show",['id'=>$id]) }}" class="btn btn-xs btn-info"><i
                class="glyphicon glyphicon-edit"></i>Chi tiết</a>
@elseif($type == 'show-delete')
    <form class="form-group" action="{{route("{$route}.destroy",['id'=>$id])}}" method="POST">
        @csrf
        {{method_field('DELETE') }}
        <a title="Show" href="{{ route("{$route}.show",['id'=>$id]) }}" class="btn btn-xs btn-info"><i
                    class="glyphicon glyphicon-edit"></i>Chi tiết</a>

        <button id class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i>Xóa</button>
    </form>
@elseif($type == 'edit-delete')
    <form class="form-group" action="{{route("{$route}.destroy",['id'=>$id])}}" method="POST">
        @csrf
        {{method_field('DELETE') }}

        <a title="Edit" href="{{ route("{$route}.edit",['id'=>$id])}}" class="btn btn-xs btn-primary"><i
                    class="glyphicon glyphicon-edit"></i>Sửa</a>
        <button id class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i>Xóa</button>
    </form>
@else
    <form class="form-group" action="{{route("{$route}.destroy",['id'=>$id])}}" method="POST">
        @csrf
        {{method_field('DELETE') }}
        <a title="Show" href="{{ route("{$route}.show",['id'=>$id])}}" class="btn btn-xs btn-info"><i
                    class="glyphicon glyphicon-edit"></i>Chi tiết</a>
        <a title="Edit" href="{{ route("{$route}.edit",['id'=>$id])}}" class="btn btn-xs btn-primary"><i
                    class="glyphicon glyphicon-edit"></i>Sửa</a>

        <button id class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i>Xóa</button>
    </form>

@endif



