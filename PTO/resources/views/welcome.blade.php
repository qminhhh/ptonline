<!DOCTYPE html>
<html lang="en">
<head>
    <title> @yield('title')</title>
    <meta charset="UTF-8">
    <meta name="_token" content="{{csrf_token()}}"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="249754165671664"/>
    <!--===============================================================================================-->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

    <link rel="icon" type="image/png" href="/images/homepage/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">--}}
    {{--<!--===============================================================================================-->--}}
    <link rel="stylesheet" type="text/css" href="/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/fonts/linearicons-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/MagnificPopup/magnific-popup.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/css/util.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">

    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/style2.css" type="text/css" media="all">
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="/js/readmore.min.js"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
        })
    </script>
    @yield('css')
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
</head>
<body class="animsition">
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="1860377377392008"
     theme_color="#0084ff"
     logged_in_greeting="PTO.com có thể giúp gì cho bạn "
     logged_out_greeting="PTO.com có thể giúp gì cho bạn ">
</div>
<!-- Header -->
<header class="header-v4">
    <!-- Header desktop -->
    <div class="container-menu-desktop">
    @if(Auth::check())
        <!-- Topbar -->
            <div class="top-bar">
                <div class="content-topbar flex-sb-m h-full m-l-93">
                    <div class="left-top-bar  flex-w h-full">
                        {{--@if(Auth::user()->idRole ==2)--}}
                        {{--<a href="{{route('pto.show',Auth::id())}}" class="flex-c-m trans-04 p-lr-25" target="_blank">--}}
                        {{--Kênh PTO--}}
                        {{--</a>--}}
                        {{--@elseif(Auth::user()->idRole ==1)--}}
                        {{--<a href="{{route('users.show',Auth::id())}}" class="flex-c-m trans-04 p-lr-25" target="_blank">--}}
                        {{--Kênh PTO--}}
                        {{--</a>--}}
                        {{--@else--}}
                        {{--<a href="{{route('pto.create')}}" class="flex-c-m trans-04 p-lr-25">--}}
                        {{--Kênh PTO--}}
                        {{--</a>--}}
                        {{--@endif--}}
                        @if(in_array('read-dashboard',$menu))
                            <a href="{{route('dashboard.index')}}" class="flex-c-m trans-04 p-lr-25" target="_blank">
                                @if(Auth::user()->idRole ==2)
                                   <b>Kênh PTO </b> 
                                @elseif(Auth::user()->idRole ==1)
                                    <b> Kênh quản trị </b> 
                                @endif
                            </a>
                        @else
                            <a href="{{route('pto.create')}}" class="flex-c-m trans-04 p-lr-25">
                                <b> Đăng ký thành PTO </b>
                            </a>
                        @endif
                    </div>

                    <div class="right-top-bar flex-w h-full dropdown-hover">
                        <a href="#" class="fullName flex-c-m trans-04 p-lr-25" style="margin-right: 150px;">
                            {{\Illuminate\Support\Facades\Auth::user()->fullName}}
                        </a>
                        <div class="dropdown_infor">
                            <!--<div  class="dropdown_infor">-->
                            <div class="triangle-up"></div>
                            <div>
                                <ul class="" style="padding-left: 0px">
                                    <li class="course"><a href="{{route('courses.myCourse')}}"> Khóa học </a></li>
                                    <li class="account"><a href="{{route('users.myAccount',Auth::id())}}"> Tài khoản của tôi</a></li>
                                    <li>
                                        {{--bỏ thẻ a đi--}}
                                        <form method="POST" action="{{route('logout')}}">
                                            @csrf
                                            <button  type="submit">Đăng xuất</button>
                                        </form>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    @else
        <!-- Topbar -->
            <div class="top-bar">
                <div class="content-topbar flex-sb-m h-full container ">
                    <div class="left-top-bar  flex-w h-full">
                        {{--<a href="" class="flex-c-m trans-04 p-lr-25" target="_blank">--}}
                        {{--Kênh PTO--}}
                        {{--</a>--}}
                    </div>
                    <div class="right-top-bar flex-w h-full ">
                        <a href="{{route('login')}}" class="flex-c-m p-lr-10 trans-04">
                            Đăng nhập
                        </a>
                        <a href="{{route('register')}}" class="flex-c-m trans-04 p-lr-25">
                            Đăng ký
                        </a>
                    </div>

                </div>
            </div>

        @endif

        <div class="wrap-menu-desktop how-shadow1">
            <nav class="limiter-menu-desktop container">
                <!-- Logo desktop -->
                <a href="{{route('homepage')}}" class="logo">
                    <img src="/images/image1/logo.jpg" alt="IMG-LOGO">
                </a>
                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li>
                            <a href="{{route('homepage')}}">Trang chủ</a>

                        </li>

                        <li>
                            <a href="{{route('showall')}}">PTO</a>
                        </li>

                        <li>
                            <a href="{{route('courses.showAll')}}">Khóa học</a>
                        </li>
                        <li>
                            <a href="{{route('new.index')}}">Tin tức</a>
                        </li>

                        <li>
                            <a href="{{route('about')}}">Về chúng tôi</a>
                        </li>

                        <li>
                            <a href="{{route('contact.index')}}">Liên hệ</a>
                        </li>
                    </ul>
                </div>

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m">
                    <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
                        <i class="zmdi zmdi-search"></i>
                    </div>

                    @if(Auth::check())
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                             data-notify="{{Cart::count()}}">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    @else
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                             data-notify="0">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    @endif

                </div>
            </nav>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="{{route('homepage')}}"><img src="/images/update/logo.png" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m m-r-15">
            <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
                <i class="zmdi zmdi-search"></i>
            </div>

            @if(Auth::check())
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart"
                     data-notify="{{Cart::count()}}">
                    <a href="shoping-cart.html"></a>
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            @else
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart"
                     data-notify="0">
                    <a href="shoping-cart.html"></a>
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            @endif
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
           <span class="hamburger-box">
              <span class="hamburger-inner"></span>

           </span>
        </div>
    </div>

    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <div class="container-search-header">
            <button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
                <img src="/images/homepage/icons/icon-close2.png" alt="CLOSE">
            </button>

            <form class="wrap-search-header flex-w p-l-15" action="{{route('sreach')}}" method="get">
                @csrf
                @method("get")
                <button class="flex-c-m trans-04" type="submit">
                    <i class="zmdi zmdi-search"></i>
                </button>
                <input class="plh3" type="text" name="search" placeholder="Tìm kiếm...">
            </form>
        </div>
    </div>

</header>


<!-- Gio hang -->

<div class="wrap-header-cart js-panel-cart {{ app('request')->input('open')=='true'?'show-header-cart':'' }}" id="cart">
    <div class="s-full js-hide-cart"></div>

    <div class="header-cart flex-col-l p-l-65 p-r-25">
        <div class="header-cart-title flex-w flex-sb-m p-b-8">
           <span class="mtext-103 cl2">
              Giỏ hàng của bạn

           </span>

            <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                <i class="zmdi zmdi-close"></i>
            </div>
        </div>
        <div class="header-cart-content flex-w js-pscroll ps" style="position: relative; overflow: hidden;">
            @foreach(Cart::content() as $row)
                <ul class="header-cart-wrapitem w-full">
                    <li class="header-cart-item flex-w flex-t m-b-12">
                        <div class="header-cart-item-img">
                            <img src="{{$row->options->has('image')?$row->options->image:''}}" alt="IMG">
                        </div>

                        <div class="header-cart-item-txt p-t-8">
                            <a href="{{route('courses.showCourse',$row->options->has('slug')?$row->options->slug:'')}}" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                                {{$row->name}}
                            </a>

                            <span class="header-cart-item-info">
								<span><b>{{ number_format($row->price) }} ₫</b></span>
                                <span style="margin-left: 70px;"><a href="#"><label class="btn-delete"
                                                                                    data-id="{{$row->rowId}}">Xóa</label></a></span>


							</span>

                        </div>

                    </li>

                </ul>
            @endforeach
            <div class="w-full">
                <div class="header-cart-total w-full p-tb-40">
                    Tổng cộng: {{Cart::subtotal()}} ₫
                </div>

                <div class="header-cart-buttons flex-w w-full">
                    <a href="{{route('order.showcard')}}"
                       class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                        Xem giỏ hàng
                    </a>


                </div>
            </div>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
            </div>
        </div>
    </div>
</div>

<!-- Gio hang -->


<section class="content">
    @yield('content')
</section>


<div id="endfloat" class="m-b-50">
</div>

<div class="homepage">

</div>

<footer class="bg3 p-t-75 p-b-32" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Tên công ty
                </h4>

                <ul>
                    <li class="p-b-10">
                        <p class="stext-107 cl7 hov-cl1 trans-04">
                            PTO - Personal Trainer Online
                        </p>
                        <p class="stext-107 cl7 hov-cl1 trans-04">
                            KM29 Đại Lộ Thăng Long, Thạch Hòa, Thạch Thất, Khu Công Nghê cao Hòa Lạc, Hà Nội
                        </p>

                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Huấn luyện viên cá nhân
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="{{route('showall')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Huấn luyện viên nữ
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="{{route('showall')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Huấn luyện viên nam
                        </a>
                    </li>


                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Liên lạc
                </h4>

                <p class="stext-107 cl7 size-201">
                    Bạn có thắc mắc?
                </p>
                <p class="stext-107 cl7 size-201">
                    Hãy đến liên lạc:
                    0978813796
                </p>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    liên kết ngoài
                </h4>

                <div>
                    <a href="https://www.facebook.com/cskh.ptovn/" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="https://www.instagram.com/cskh.ptovn/" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="p-t-40">
            <div class="flex-c-m flex-w p-b-18">
                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-01.png" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-02.png" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-03.png" alt="ICON-PAY">
                </a>
            </div>

            <p class="stext-107 cl6 txt-center">
                <a
                        href="https://www.facebook.com/cskh.ptovn/" target="_blank">
                    PTO - Personal Trainer Online
                </a>
            </p>
            <p class="stext-107 cl6 txt-center" style="color: white;">
                Website được phát triển bởi ĐỖ ĐỨC & QUANG MINH - học viên DEVMASTER
            </p>
        </div>
    </div>
</footer>


<!-- Slider -->

<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
     <span class="symbol-btn-back-to-top">
        <i class="zmdi zmdi-chevron-up"></i>
     </span>
</div>

{{--<!-- PTA -->--}}
{{--<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">--}}
    {{--<div class="overlay-modal1 js-hide-modal1"></div>--}}

    {{--<div class="container">--}}
        {{--<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">--}}
            {{--<button class="how-pos3 hov3 trans-04 js-hide-modal1">--}}
                {{--<img src="images/homepage/icons/icon-close.png" alt="CLOSE">--}}
            {{--</button>--}}

            {{--<div class="row">--}}
                {{--<div class="col-md-6 col-lg-7 p-b-30">--}}
                    {{--<div class="p-l-25 p-r-30 p-lr-0-lg">--}}
                        {{--<div class="wrap-slick3 flex-sb flex-w">--}}
                            {{--<div class="wrap-slick3-dots" style="display: none;"></div>--}}
                            {{--<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>--}}

                            {{--<div class="slick3 gallery-lb">--}}
                                {{--<div class="item-slick3" data-thumb="images/homepage/product-detail-01.jpg">--}}
                                    {{--<div class="wrap-pic-w pos-relative">--}}
                                        {{--<img src="/images/homepage/product-detail-01.jpg" alt="IMG-PRODUCT">--}}

                                        {{--<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04"--}}
                                           {{--href="images/product-detail-01.jpg">--}}
                                            {{--<i class="fa fa-expand"></i>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-6 col-lg-5 p-b-30">--}}
                    {{--<div class="p-r-50 p-t-5 p-lr-0-lg">--}}
                        {{--<h4 class="mtext-105 cl2 js-name-detail p-b-14">--}}
                            {{--PT A--}}
                        {{--</h4>--}}


                        {{--<p class="stext-102 cl3 p-t-23">--}}
                            {{--3 vòng--}}
                        {{--</p>--}}

                        {{--<!--  -->--}}
                        {{--<div class="p-t-33">--}}
                            {{--<div class="flex-w flex-r-m p-b-10">--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<!--===============================================================================================-->

<!--===============================================================================================-->
<script src="/vendor/animsition/js/animsition.min.js"></script>
<script src="/js/tab-switch.js"></script>
<!--===============================================================================================-->
<script src="/vendor/bootstrap/js/popper.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/vendor/select2/select2.min.js"></script>
<script>
    $(".js-select2").each(function () {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
<!--===============================================================================================-->
<script src="/vendor/daterangepicker/moment.min.js"></script>
<script src="/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="/vendor/slick/slick.min.js"></script>
<script src="/js/slick-custom.js"></script>
<!--===============================================================================================-->
<script src="/vendor/parallax100/parallax100.js"></script>
<script>
    $('.parallax100').parallax100();
</script>
<!--===============================================================================================-->
<script src="/vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
<script>
    $('.gallery-lb').each(function () { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: 'a', // the selector for gallery item
            type: 'image',
            gallery: {
                enabled: true
            },
            mainClass: 'mfp-fade'
        });
    });
</script>
<!--===============================================================================================-->
<script src="/vendor/isotope/isotope.pkgd.min.js"></script>
<!--===============================================================================================-->
<script src="/vendor/sweetalert/sweetalert.min.js"></script>
<script>
    $('.js-addwish-b2').on('click', function (e) {
        e.preventDefault();
    });

    $('.js-addwish-b2').each(function () {
        var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");

            $(this).addClass('js-addedwish-b2');
            $(this).off('click');
        });
    });

    $('.js-addwish-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");

            $(this).addClass('js-addedwish-detail');
            $(this).off('click');
        });
    });

    /*---------------------------------------------*/

    $('.js-addcart-detail').each(function () {
        var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });

</script>
<!--===============================================================================================-->
<script src="/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script>
    $('.js-pscroll').each(function () {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });

        $(window).on('resize', function () {
            ps.update();
        })
    });
</script>

<script>
    $(document).ready(function () {

        $('.btn-delete').click(function (e) {
            e.preventDefault();
            var data = {
                id: $(this).data('id'),
            };
            $.ajax({
                url: '{{route('order.deleteCart')}}',
                type: 'post',
                data: data,
            })
                .done(function () {
                    window.location.assign('{{\Illuminate\Support\Facades\Request::url()}}' + '?open=true')
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        });


        $('.btn-add').click(function (e) {
            e.preventDefault();
            // $('.js-panel-cart').addClass('show-header-cart');
            var data = {
                id: $(this).data('id'),
                courseName: $(this).data('name'),
                price: $(this).data('price'),
                image: $(this).data('image'),
                namept: $(this).data('namept'),
                slug: $(this).data('slug'),
            };
            $.ajax({
                url: '{{route('order.addcart')}}',
                type: 'post',
                data: data,
            })
                .done(function (result) {
                    if (result.status == 4) {
                        swal({
                            title: result.message,
                            text: '',
                            icon: 'error',
                            button: "OK",
                        });
                    }

                    if (result.status == 3) {
                        swal({
                            title: result.message,
                            text: '',
                            icon: 'error',
                            button: "OK",
                        });
                    }
                    if (result.status == 2) {
                        swal({
                            title: result.message,
                            text: '',
                            icon: 'error',
                            button: "OK",
                        });
                    }
                    if (result.status == 0) {
                        swal({
                            title: result.message,
                            text: '',
                            icon: 'error',
                            button: "OK",
                        });
                    }
                    if (result.status == 1) {
                        window.location.assign('{{\Illuminate\Support\Facades\Request::url()}}' + '?open=true')
                    }
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        });
    });
</script>

<script src="/bower_components/chart.js/Chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

@stack('scripts')

<script src="/js/main.js"></script>


</div>
</body>
</html>

