@extends('master_layout.admin.index')
@section('titles')
    Chỉnh sửa khóa học
@endsection
@section('content')
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Khóa học
            <small>Chỉnh sửa</small>
        </h1>
        <ol class="breadcrumb">

        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="col-lg-12">
                        <h1 class="page-header">Khóa học {{$course->courseName}}
                        </h1>
                    </div>
                    <form action="{{route('courses.update',$course->id)}}" enctype="multipart/form-data" method="POST" role="form" >
                        @csrf
                        @method("put")
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Thể loại</label>
                                <select class="form-control" name="type" id="type">
                                    @foreach($type as $t)
                                        <option
                                                @if($course->type->id == $t->id)
                                                {{"selected"}}
                                                @endif value="{{$t->id}}">{{$t->typeName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tên khóa học</label>
                                <input type="text" class="form-control{{ $errors->has('courseName') ? ' is-invalid' : '' }}" id="" placeholder="Nhập tên khóa học" name="courseName" value="{{$course->courseName}}">
                                @if ($errors->has('courseName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('courseName') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Học phí (VNĐ)</label>
                                <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" id="" placeholder="Nhập giá khóa học" name="price" value="{{$course->price}}">
                                @if ($errors->has('price'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Hình ảnh</label>
                                <p>
                                    <img width="300px" src="{{$course->image}}" alt="">
                                </p>
                                <input type="file" name="image" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Tổng quát</label>
                                <textarea name="introduce" id="input" class="form-control ckeditor {{ $errors->has('introduce') ? ' is-invalid' : '' }}" rows="1" required="required">{{$course->introduce}}</textarea>
                                @if ($errors->has('introduce'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('introduce') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Yêu cầu của khóa học</label>
                                <textarea name="require" id="input" class="form-control ckeditor {{ $errors->has('require') ? ' is-invalid' : '' }}" rows="1" required="required">{{$course->require}}</textarea>
                                @if ($errors->has('require'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('require') }}</strong>
                                </span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="">Lợi ích từ khóa học</label>
                                <textarea name="benefit" id="input" class="form-control ckeditor {{ $errors->has('benefit') ? ' is-invalid' : '' }}" rows="1" required="required">{{$course->benefit}}</textarea>
                                @if ($errors->has('benefit'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('benefit') }}</strong>
                                </span>
                                @endif
                            </div>
 
                            <div class="form-group">
                                <label for="">Đối tượng mục tiêu</label>
                                <textarea name="target" id="input" class="form-control ckeditor {{ $errors->has('target') ? ' is-invalid' : '' }}" rows="1" required="required">{{$course->target}}</textarea>
                                @if ($errors->has('target'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('target') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="public" 
                                       @if($course->status == 1) checked @endif>
                                <label for="option"><span></span> <p>Kích hoạt</p></label>
                                <br>

                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>

                        </div>
                    </form>
                <div class="box-footer">
                    <form action="{{route('courses.destroy',$course->id)}}" method="post">
                        @csrf
                        @method("DELETE")
                        <button type="button" class="btn btn-primary btn-del" > Xóa </button>
                    </form>
                </div>

                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>

    <script>
        $(document).ready(function(){
            $('.btn-del').click(function () {
                var form=$(this).parent('form');
                swal({
                    title: "Bạn có chắc chắn xóa khóa học {{$course->courseName}}?",
                    text: "Một khi xóa sẽ không lấy lại được dữ liệu , các bài học cũng bị xóa !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Xóa!",
                    cancelButtonText: "Hủy",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        });
    </script>
    <!-- /.content -->
@endsection
