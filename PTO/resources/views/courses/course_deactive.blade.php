@extends('master_layout.admin.index')
@section('titles')
    Khóa học chưa kích hoạt
@endsection
@section('content')

    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Khóa học chưa kích hoạt
            <small>danh sách</small>
        </h1>
        <br>
        {{--<a title="Show" href="{{route('courses.create')}}" class="btn btn-xs btn-info"><i--}}
        {{--class="glyphicon glyphicon-plus"></i>Thêm Khóa học--}}
        {{--</a>--}}
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('courses.deactive')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
            </div>
            @foreach($course as $c )
                <div class="col-md-4">
                    <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-black"  style="background: url('{{$c->image}}'); background-size: cover;">
                            <a href="{{route('courses.courseDetail',$c->id)}}">
                                <h3 class="widget-user-username"><i class="fa fa-tag"></i>{{$c->courseName}}</h3>
                            </a>
                            {{-- <h5 class="widget-user-desc">Web Designer</h5>--}}
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">{{number_format($c->price)}}</h5>
                                        <span class="description-text">Giá(VNĐ) </span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">0</h5>
                                        <span class="description-text">Số học viên</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header">{{$c->lessons->count()}}</h5>
                                        <span class="description-text">Số bài học</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-4">
                <div class="box box-widget widget-user" id="div-add">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black"  style="background: url('/images/homepage/add1.png'); background-size: cover;">
                        <a href="{{route('courses.create')}}">
                            <h3 class="widget-user-username" id="add">Thêm mới khóa học</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
