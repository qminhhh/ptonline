@extends('master_layout.admin.index')
@section('titles')
    Chi tiết khóa học
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chi tiết Khóa học
        </h1>
        <ol class="breadcrumb">
            {{ Breadcrumbs::render('courses.courseDetail',$course) }}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" style="height: 150px;"
                             src="{{$course->image}}" alt="User profile picture">

                        <h3 class="profile-username text-center">{{$course->courseName}}</h3>

                        <ul class="list-group list-group-unbordered">

                            <li class="list-group-item">
                                <b>Giá</b> <a class="pull-right">{{number_format($course->price)}} ₫</a>
                            </li>
                            <li class="list-group-item">
                                <b>Số bài học</b> <a class="pull-right">{{$course->lessons->count()}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Số học viên</b> <a class="pull-right">{{$trainees->count()}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Đánh giá</b> <a class="pull-right">
                                    @if($course->rate != null)
                                        {{$course->rating }} ( {{$course->rate->count()}} đánh giá )
                                    @else
                                        Chưa có đánh giá
                                    @endif </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">Thông tin khóa học</a></li>
                        <li><a href="#trainee" data-toggle="tab">Danh sách Học viên</a></li>
                        <li><a href="#timeline" data-toggle="tab">Danh sách bài học</a></li>
                        <li><a href="#settings" data-toggle="tab">Doanh thu</a></li>
                        {{--<li><a href="#comment" data-toggle="tab">Ý kiến học viên</a></li>--}}

                    </ul>
                    <div class="active tab-content">
                        <div class="active tab-pane" id="activity">
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box-body">
                                            <strong><i class="fa fa-pencil margin-r-5"></i>Tổng quát khóa học</strong>
                                            <p class="text-muted">{!!$course->introduce !!}</p>
                                            <strong><i class="fa fa-pencil margin-r-5"></i>Yêu cầu khóa học</strong>
                                            <p class="text-muted">{!!$course->require !!}</p>
                                            <strong><i class="fa fa-pencil margin-r-5"></i>Lợi ích khóa học</strong>
                                            <p class="text-muted">{!!$course->benefit !!}</p>
                                            <strong><i class="fa fa-pencil margin-r-5"></i>Đối tượng mục tiêu</strong>
                                            <p class="text-muted">{!! $course->target !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </section>
                        </div>

                        <div class="tab-pane" id="trainee">
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title"></h3>

                                                <div class="box-tools">
                                                    <div class="input-group input-group-sm" style="width: 150px;">
                                                        <input type="text" name="table_search"
                                                               class="form-control pull-right" placeholder="Search">

                                                        <div class="input-group-btn">
                                                            <button type="submit" class="btn btn-default"><i
                                                                        class="fa fa-search"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                {{--<div class="panel-heading">Danh sách học viên</div>--}}
                                                <div class="panel-body">
                                                    <table id="trainee-table"
                                                           class="table table-bordered table-hover dataTable"
                                                           style="width:100%">
                                                        <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Họ và tên</th>
                                                            <th>Email</th>
                                                            <th>SĐT</th>
                                                            <th>Tiến trình</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="timeline">
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title"></h3>

                                                <div class="box-tools">
                                                    <div class="input-group input-group-sm" style="width: 150px;">
                                                        <input type="text" name="table_search"
                                                               class="form-control pull-right" placeholder="Search">

                                                        <div class="input-group-btn">
                                                            <button type="submit" class="btn btn-default"><i
                                                                        class="fa fa-search"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>STT</th>
                                                        <th>Tên bài học</th>
                                                        <th>Nội dung</th>
                                                        <th>Ngày tạo</th>
                                                    </tr>
                                                    @php $i = 1 @endphp
                                                    @foreach($course->lessons as $l)
                                                        <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{!! $l->lessonName !!}</td>
                                                            <td style="width: 50%">{!! $l->content !!}</td>
                                                            <td>{{date('d-m-Y H:i',strtotime($l->created_at))}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="settings">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                    <span class="info-box-icon bg-yellow"><i
                                                class="ion ion-ios-people-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Số học viên</span>
                                            <span class="info-box-number">{{$trainees->count()}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                    <span class="info-box-icon bg-aqua"><i
                                                class="ion ion-ios-pricetag-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Lợi nhuận</span>
                                            <span class="info-box-text"><small>( 10%/Khóa học )</small></span>
                                            <span class="info-box-number">{{number_format($course->price*90/100 )}}
                                                ₫</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i
                                                    class="ion ion-ios-cart-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Tổng thu</span>
                                            <span class="info-box-number">{{number_format($course->price*90/100*$trainees->count() )}}
                                                ₫</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        {{--<div class="tab-pane" id="comment">--}}
                            {{--<ul class="timeline timeline-inverse">--}}

                            {{--@foreach($course->lessons as $l)--}}
                                {{--<!-- timeline item -->--}}

                                    {{--@if($l->comments != null )--}}
                                        {{--<li>--}}
                                            {{--<i class="fa fa-book bg-aqua"></i>--}}

                                            {{--<div class="timeline-item">--}}
                                                {{--<h3 class="timeline-header no-border"><a--}}
                                                            {{--href="{{route('lessons.showNotifice',$l->id)}}">{{$l->lessonName}}</a>--}}
                                                {{--</h3>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                {{--@endif--}}
                                {{--<!-- END timeline item -->--}}
                            {{--@endforeach--}}

                            {{--<!-- timeline item -->--}}

                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<!-- /.tab-pane -->--}}
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection

@push('script_footer')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#trainee-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('courses.dataTrainee',$course->id) !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'nameTrainee', name: 'nameTrainee'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {
                        data: function (data) {
                            if (data.process == null) {
                                return '<div class="clearfix">\n' +
                                    '<span class="pull-left">Chưa học</span>\n' +
                                    '<small class="pull-right">0%</small>\n' +
                                    '</div>\n' +
                                    '<div class="progress xs">\n' +
                                    '<div class="progress-bar progress-bar-green" style="width: 0%;"></div>\n' +
                                    '</div>';
                            } else {
                                if (data.process == 100) {
                                    return '<div class="clearfix">\n' +
                                        '<span class="pull-left">Hoàn thành</span>\n' +
                                        '<small class="pull-right">100%</small>\n' +
                                        '</div>\n' +
                                        '<div class="progress xs">\n' +
                                        '<div class="progress-bar progress-bar-green" style="width: 100%;"></div>\n' +
                                        '</div>';
                                } else {
                                    return `<div class="clearfix">
                                    <span class="pull-left">Đang học</span>
                                <small class="pull-right">` + data.process + `%</small>
                                    </div>
                                    <div class="progress xs">
                                    <div class="progress-bar progress-bar-red" style="width: ` + data.process + `%;"></div>
                                    </div>`;
                                }
                            }
                        }, name: 'process'
                    },
                ]
            });


        });
    </script>
@endpush