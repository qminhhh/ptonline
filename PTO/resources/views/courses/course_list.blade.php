@extends('master_layout.admin.index')
@section('titles')
    Danh sách bài học
@endsection
@section('content')
    {{--@php(--}}
    {{--var_dump($menu)--}}
    {{--)--}}
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Khóa học
            <small>danh sách</small>
        </h1>
        <ol class="breadcrumb">
            {{ Breadcrumbs::render('courses.list') }}

        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách khóa học</div>
                    <div class="panel-body">
                        <table id="course-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Thể loại</th>
                                <th>Tên khóa học</th>
                                <th>PTO</th>
                                <th>Giá</th>
                                <th>Số học viên</th>
                                <th>Đánh giá</th>
                                <th>Trạng thái</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td></td>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#course-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('courses.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'type', name: 'type'},
                    {data: 'courseName', name: 'courseName'},
                    {data: 'ptoName', name: 'ptoName'},
                    {data: 'prices', name: 'prices'},
                    {data: 'numTrainee', name: 'numTrainee'},
                    {data: 'rate', name: 'rate'},
                    {
                        data: function (data) {
                            if (data.lock != null) {
                                if (data.lock == 0) {
                                    return '<span class="label label-success">Mở chỉnh sửa</span>';
                                }
                                else if (data.lock == 1) {
                                    return '<span class="label label-warning">Khóa chỉnh sửa</span> ';
                                } else {
                                    return '<span class="label label-danger">Đóng</span> ';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {data: 'actions', name: 'actions'},
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        console.log(this[0][0]);
                        var input = document.createElement("input");
                        input.style.width = "75px";
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });
        });
    </script>
@endpush
