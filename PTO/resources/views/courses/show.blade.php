@extends('master_layout.admin.index')
@section('titles')
    Chi tiết khóa học
@endsection
@section('content')
    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Khóa học
            <small>chi tiết</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('courses.courseDetail',$course)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
            </div>
            <!-- left column -->
            <div class="col-md-4">
                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <img class="profile-user-img img-responsive img-circle" style="height: 150px;"
                             src="{{$course->image}}" alt="User profile picture">

                        <h3 class="profile-username text-center">{{$course->courseName}}</h3>

                        <p class="text-muted text-center">{{Auth::user()->fullName}}</p>

                        <p class="text-muted text-center">Thể loại: <strong>{{$course->type->typeName}}</strong></p>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="glyphicon glyphicon-hand-right margin-r-5"></i> Học phí</strong>

                        <p class="text-muted">
                            {{number_format($course->price)}} VNĐ
                        </p>

                        <hr>

                        <strong><i class="glyphicon glyphicon-hand-right margin-r-5"></i> Tổng quát </strong>


                        <div class="article text-muted" style="overflow: hidden;">{!! $course->introduce !!}</div>

                        <hr>

                        <strong><i class="glyphicon glyphicon-hand-right margin-r-5"></i> Yêu cầu của khóa học </strong>

                        <div class="article text-muted" style="overflow: hidden;">{!! $course->require !!}</div>

                        <hr>

                        <strong><i class="glyphicon glyphicon-hand-right margin-r-5"></i> Lợi ích từ khóa học </strong>

                        <div class="article text-muted" style="overflow: hidden;">{!! $course->benefit !!}</div>

                        <hr>

                        <strong><i class="glyphicon glyphicon-hand-right margin-r-5"></i> Đối tượng mục tiêu</strong>
                        <p class="text-muted">{!!$course->target!!}</p>
                        <br/>

                        <a href="{{route('courses.edit',$course->id)}}" class="btn btn-primary btn-block "><b>Cập
                                nhật</b></a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-8">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">Danh sách bài học</a></li>
                        <li><a href="#timeline" data-toggle="tab">Học viên</a></li>
                        <li><a href="#settings" data-toggle="tab">Tài chính</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            <!-- The timeline -->
                            <ul class="timeline timeline-inverse">
                            @foreach($lesson as $l)
                                <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-book bg-aqua"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i>{{$l->created_at}}</span>

                                            <h3 class="timeline-header no-border"><a
                                                        href="{{route('lessons.edit',$l->id).'?idCourse='.$course->id}}">{{$l->lessonName}}</a>
                                            </h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                            @endforeach
                            <!-- timeline item -->

                                <li>
                                    <i class="fa fa-plus bg-aqua"></i>

                                    <div class="timeline-item">
                                        <h3 class="timeline-header no-border"><a
                                                    href="{{route('lessons.create').'?id='.$course->id}}">Thêm bài
                                                học</a>
                                        </h3>
                                    </div>
                                </li>

                                <!-- END timeline item -->

                            </ul>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="timeline">
                            <div class="panel panel-default">
                                {{--<div class="panel-heading">Danh sách học viên</div>--}}
                                <div class="panel-body">
                                    <table id="trainee-table" class="table table-bordered table-hover dataTable"
                                           style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Họ và tên</th>
                                            <th>Email</th>
                                            <th>SĐT</th>
                                            <th>Tiến trình</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="settings">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Số học viên</span>
                                            <span class="info-box-number">{{$countT}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-pricetag-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Đơn giá <small> -10%</small></span>
                                            <span class="info-box-number">{{number_format($price)}} ₫</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i
                                                    class="ion ion-ios-cart-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Tổng tiền</span>
                                            <span class="info-box-number">{{number_format($total)}} ₫</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->

        </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@push('script_footer')
    <script src="/js/readmore.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {

            $('.article').readmore({
                speed: 75,
                lessLink: '<a href="#">Thu gọn</a>',
                moreLink: '<a href="#">Xem thêm</a>',
                collapsedHeight: 120,
            });

            $('#trainee-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('courses.dataTrainee',$course->id) !!}',
                columns: [
                    {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {data: 'nameTrainee', name: 'nameTrainee'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {
                        data: function (data) {
                            if (data.process == null) {
                                return '<div class="clearfix">\n' +
                                    '<span class="pull-left">Chưa học</span>\n' +
                                    '<small class="pull-right">0%</small>\n' +
                                    '</div>\n' +
                                    '<div class="progress xs">\n' +
                                    '<div class="progress-bar progress-bar-green" style="width: 0%;"></div>\n' +
                                    '</div>';
                            } else {
                                if (data.process == 100) {
                                    return '<div class="clearfix">\n' +
                                        '<span class="pull-left">Hoàn thành</span>\n' +
                                        '<small class="pull-right">100%</small>\n' +
                                        '</div>\n' +
                                        '<div class="progress xs">\n' +
                                        '<div class="progress-bar progress-bar-green" style="width: 100%;"></div>\n' +
                                        '</div>';
                                } else {
                                    return `<div class="clearfix">
                                    <span class="pull-left">Đang học</span>
                                <small class="pull-right">` + data.process + `%</small>
                                    </div>
                                    <div class="progress xs">
                                    <div class="progress-bar progress-bar-red" style="width: ` + data.process + `%;"></div>
                                    </div>`;
                                }
                            }
                        }, name: 'process'
                    },
                ]
            });
        });
    </script>

@endpush
