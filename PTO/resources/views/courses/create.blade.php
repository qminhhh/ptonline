@extends('master_layout.admin.index')
@section('titles')
    Thêm mới khóa học
@endsection
@section('content')

    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Khóa học
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('courses.create')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>+</strong> Thêm khóa học</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{route('courses.store')}}" enctype="multipart/form-data" method="POST" role="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Thể loại</label>
                                <select class="form-control" name="type" id="typeName">
                                    @foreach($type as $t)
                                        <option value="{{$t->id}}">{{$t->typeName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tên khóa học</label>
                                <input type="text"
                                       class="form-control{{ $errors->has('courseName') ? ' is-invalid' : '' }}" id=""
                                       placeholder="Nhập tên khóa học" name="courseName"
                                       value="{{ old('courseName') }}">
                                @if ($errors->has('courseName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('courseName') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Học phí (VNĐ)</label>
                                <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                       id="" placeholder="Nhập giá khóa học" name="price" value="{{ old('price') }}">
                                @if ($errors->has('price'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Hình ảnh</label>
                                <input type="file" name="image"
                                       class="form-control {{ $errors->has('image') ? ' is-invalid' : '' }}">
                                @if ($errors->has('image'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Tổng quát</label>
                                <textarea name="introduce" id="input"
                                          class="form-control ckeditor {{ $errors->has('introduce') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('introduce') }}</textarea>
                                @if ($errors->has('introduce'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('introduce') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Yêu cầu của khóa học</label>
                                <textarea name="require" id="input"
                                          class="form-control ckeditor {{ $errors->has('require') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('require') }}</textarea>
                                @if ($errors->has('require'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('require') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Lợi ích từ khóa học</label>
                                <textarea name="benefit" id="input"
                                          class="form-control ckeditor {{ $errors->has('benefit') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('benefit') }}</textarea>
                                @if ($errors->has('benefit'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('benefit') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Đối tượng mục tiêu</label>
                                <textarea name="target" id="input"
                                          class="form-control ckeditor {{ $errors->has('target') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('target') }}</textarea>
                                @if ($errors->has('target'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('target') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="checkbox" name="public" value="">
                                <label for="option"><span></span> <p>Kích hoạt khóa học</p></label>
                                <br>

                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Tạo</button>
                            <button type="reset" class="btn btn-primary">Làm mới</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
