@extends('user_manage.master_manage')
@section('title')
    Thông tin tài khoản
@endsection
@section('css')

@endsection
@section('content-user')
    <!--change password-->
    <div class=" menu-right-chgpass">
        <div class="col-sm-12">
            <div>
                <div>
                    <strong><label>Đổi mật khẩu</label></strong>
                </div>
                <div class="textChgPass">
                    <label>Để bảo mật tài khoản, vui lòng không chia sẻ mật khẩu với người khác</label>
                </div>
            </div>
            <div>
                @if(session('thongbao'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{session('thongbao')}}
                    </div>
                @endif
            </div>
            <form method="POST" enctype="multipart/form-data" action="{{route('users.updatePass',Auth::id())}}">
                @method('POST')
                @csrf
                <div class="second-part-chgpass">
                    <div class="row padding-pwd">
                        <div class="col-sm-3 input-with-label">
                            <label>mật khẩu hiện tại</label>
                        </div>
                        <div class="col-sm-4 input-with-content">
                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password"
                                   value="{{ old('password') }}">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <p>{{ $errors->first('password') }}</p>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row padding-pwd">
                        <div class="col-sm-3 input-with-label">
                            <label>mật khẩu mới</label>
                        </div>
                        <div class="col-sm-4 input-with-content">
                            <input id="newpassword" type="password"
                                   class="form-control{{ $errors->has('newpassword') ? ' is-invalid' : '' }}"
                                   name="newpassword"
                                   value="{{ old('newpassword') }}">

                            @if ($errors->has('newpassword'))
                                <span class="invalid-feedback">
                                    <p>{{ $errors->first('newpassword') }}</p>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row padding-pwd">
                        <div class="col-sm-3 input-with-label">
                            <label>nhập lại mật khẩu mới</label>
                        </div>
                        <div class="col-sm-4 input-with-content">
                            <input id="password_confirmation" type="password"
                                   class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                   name="password_confirmation"
                                   value="{{ old('password_confirmation') }}">

                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    <p>{{ $errors->first('password_confirmation') }}</p>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row padding-pwd">
                        <div class="col-sm-3 input-with-label">

                        </div>
                        <div class="col-sm-4 input-with-content">
                            <button type="submit" class="btn btn-danger">Đổi mật khẩu</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!--END change password-->
@endsection
@push('scripts')
    <script>

    </script>
@endpush