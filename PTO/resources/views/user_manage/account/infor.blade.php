@extends('user_manage.master_manage')
@section('title')
    Thông tin tài khoản
@endsection
@section('css')

@endsection
@section('content-user')
    <!--user information-->
    <div class="menu-right-chgpass">
        <div class="col-sm-12">
            <div>
                <div>
                    <strong><label>Tài khoản của tôi</label></strong>
                </div>
                <div class="textChgPass">
                    <label>Quản lí thông tin cá nhân để bảo mật tài khoản</label>
                </div>
            </div>
            <form method="POST" enctype="multipart/form-data" action="{{route('users.update1',Auth::id())}}">
                @method('POST')
                @csrf
                <div class="second-part-chgpass row">

                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-3 input-with-label">
                                <label>Họ và tên</label>
                            </div>
                            <div class=" col-sm-6 input-with-content">
                                <input id="fullName" type="text"
                                       class="form-control{{ $errors->has('fullName') ? ' is-invalid' : '' }}"
                                       name="fullName" placeholder="{{$user->fullName}}"
                                       value="{{$user->fullName}}">

                                @if ($errors->has('fullName'))
                                    <span class="invalid-feedback">
                                    <p>{{ $errors->first('fullName') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 input-with-label">
                                <label>Giới tính</label>
                            </div>
                            <div class="col-sm-6 input-with-content">
                                <div class=" row" style="margin: 0px">
                                    <div class="radio-text">
                                        <input name="gender" value="0"
                                               @if($user->gender == 0)
                                               {{"checked"}}
                                               @endif
                                               type="radio">Nữ

                                    </div>
                                    <div class="radio-text">
                                        <input name="gender" value="1"
                                               @if($user->gender == 1)
                                               {{"checked"}}
                                               @endif
                                               type="radio">Nam
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 input-with-label">
                                <label>Số điện thoại</label>
                            </div>
                            <div class="col-sm-6 input-with-content">
                                <input id="phone" type="text"
                                       class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                       name="phone" placeholder="{{$user->phone}}"
                                       value="{{$user->phone}}">

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                    <p>{{ $errors->first('phone') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 input-with-label">
                                <label>Ngày sinh</label>
                            </div>
                            <div class="col-sm-6 input-with-content">
                                <input id="DOB" type="date"
                                       class="form-control{{ $errors->has('DOB') ? ' is-invalid' : '' }}"
                                       name="DOB" placeholder="{{$user->DOB}}"
                                       value="{{$user->DOB}}">

                                @if ($errors->has('DOB'))
                                    <span class="invalid-feedback">
                                    <p>{{ $errors->first('DOB') }}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 choose-image">
                        <div>
                            <img src="{{$user->image}}">
                        </div>
                        <div>
                            <input id="image" style=" display:block;width: 220%;margin-left: -75px;"
                                   type="file"
                                   class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                                   name="image">

                            @if ($errors->has('image'))
                                <span class="invalid-feedback">
                            <p>{{ $errors->first('image') }}</p>
                        </span>
                            @endif
                        </div>


                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-3 input-with-label">

                            </div>
                            <div class=" col-sm-6 input-with-content">
                                <button type="submit" class="btn btn-danger">Lưu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--END user information-->
@endsection
@push('scripts')
    <script>

    </script>
@endpush