@extends('user_manage.master_manage')
@section('title')
    Thông báo
@endsection
@section('css')
    <style>
        body {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif;
            color: #34495e;
            -webkit-font-smoothing: antialiased;
            line-height: 1.6em;
        }

        p {
            margin: 0;
        }

        .notice {
            position: relative;
            margin: 1em;
            background: #ece7e7;
            padding: 1em 1em 1em 2em;
            border-left: 4px solid #DDD;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.125);
        }

        .notice:before {
            position: absolute;
            top: 50%;
            margin-top: -17px;
            left: -17px;
            background-color: #DDD;
            color: #FFF;
            width: 30px;
            height: 30px;
            border-radius: 100%;
            text-align: center;
            line-height: 30px;
            font-weight: bold;
            font-family: Georgia;
            text-shadow: 1px 1px rgba(0, 0, 0, 0.5);
        }

        .info {
            border-color: #0074D9;
        }

        .info:before {
            content: "i";
            background-color: #0074D9;
        }

        .success {
            border-color: #2ECC40;
        }

        .success:before {
            content: "√";
            background-color: #2ECC40;
        }

        .warning {
            border-color: #FFDC00;
        }

        .warning:before {
            content: "!";
            background-color: #FFDC00;
        }

        .error {
            border-color: #FF4136;
        }

        .error:before {
            content: "X";
            background-color: #FF4136;
        }
    </style>
@endsection
@section('content-user')
    <!--user chart-->
    <div class="menu-right-chgpass">
        <div>
            <div>
                <div class="textChgPass">
                    <label>Bạn có {{$count}} thông báo mới / {{$notifies->count()}} thông báo.</label>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                @foreach($notifies as $notify)
                    @if($notify->noti != null)
                        <div @if($notify->seen == 1) class="notice success" style="background: #ffffff;"  @else  class="notice info" @endif>
                            <a href="{{route('notify.status',$notify->id).'?route='.$notify->route}}" style="color: #000000;">{!! $notify->noti !!}</a></div>
                    @endif
                @endforeach
            </div>

        </div>
    </div>


    <!--END user chart-->
@endsection

@push('scripts')

@endpush