@extends('user_manage.master_manage')
@section('title')
    Thể trạng cơ thể
@endsection
@section('css')

@endsection
@section('content-user')
    <!--user chart-->
    <div class="menu-right-chgpass">
        <div>
            <div>
                <strong><label>Chỉ số cơ thể hiện tại của bạn</label></strong>
                <div class="textChgPass">
                    <label>Biểu đồ thế hiện thể trạng cơ thể của bạn</label>
                </div>
            </div>

        </div>
        @if($bp != null)
            <div class="row">
                <div class="col-sm-12">
                    <canvas style="width: 100%;min-height: 300px;" id="userchart"></canvas>
                </div>

            </div>
        @endif

        <form id="frm" action="{{route('users.updateChart',Auth::id())}}" method="post">
            @csrf
            <div class="col-sm-12">
                <div class="chart-infor-left row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-3 input-with-label font-weight-bold">
                                <label>Chiều cao</label>
                            </div>
                            <div class="col-sm-6 input-with-content left-text-chart">
                                <input type="number" value="{{$bp!=null?$bp->height:''}}" name="height" @if($bp!=null) readonly @endif >
                                <span>(cm)</span>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3 input-with-label font-weight-bold">
                                <label>Cân nặng</label>
                            </div>
                            <div class="col-sm-6 input-with-content left-text-chart">
                                <input type="number" value="{{$bp!=null?$bp->weight:''}}" name="weight" required>
                                <span>(kg)</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 input-with-label font-weight-bold">
                                <label>Ngày :</label>
                            </div>
                            <div class="col-sm-6 input-with-content left-text-chart">
                                <input type="date" class="form-control" value="{{date('Y-m-d')}}" name="date_update">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 m-t-10">
                        <div class="row">
                            <div class=" input-with-label font-weight-bold text-center">
                                <label>Số đo 3 vòng</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class=" input-with-content " style="margin-left: 15px">
                                <div class="row right-text-chart">
                                    <div class="text-capitalize"><label>vòng 1</label></div>
                                    <div>
                                        <input type="number" value="{{$bp!=null?$bp->bust:''}}" name="bust" required>
                                        <span>(cm)</span>
                                    </div>
                                </div>
                                <div class="row right-text-chart">
                                    <div class="text-capitalize"><label>vòng 2</label></div>
                                    <div>
                                        <input type="number" value="{{$bp!=null?$bp->wais:''}}" name="wais" required>
                                        <span>(cm)</span>
                                    </div>
                                </div>
                                <div class="row right-text-chart">
                                    <div class="text-capitalize"><label>vòng 3</label></div>
                                    <div class="">
                                        <input type="number" value="{{$bp!=null?$bp->hips:''}}" name="hips" required>
                                        <span>(cm)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-capitalize update-button">
                    <button type="submit" class="btn btn-success text-capitalize">Cập nhật</button>
                </div>
            </div>
        </form>


    </div>


    <!--END user chart-->
@endsection

@push('scripts')
    @if($bp != null)
        <script>

            var ctx = document.getElementById("userchart");
            var userchart = new Chart(ctx, {
                type: 'line',
                options: {
                    elements: {
                        line: {
                            tension: 0, // disables bezier curves
                        }
                    },
                    yAxisID:'Ngày'
                },
                data: {
                    labels: {!! json_encode($label) !!},
                    datasets: [{
                        label: "Cân nặng(kg)",
                        fill:false,

                        backgroundColor: "rgba(231, 76, 60,0.31)",
                        borderColor: "rgba(231, 76, 60,0.7)",
                        pointBorderColor: "rgba(231, 76, 60,0.7)",
                        pointBackgroundColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(231, 76, 60,1)",
                        pointBorderWidth: 9,
                        data: {!! json_encode($dataWeight) !!},
                    }, {
                        label: "Vòng 1(cm)",
                        fill:false,
                        backgroundColor: "rgba(38, 185, 154, 0.31)",
                        borderColor: "rgba(38, 185, 154, 0.7)",
                        pointBorderColor: "rgba(38, 185, 154, 0.7)",
                        pointBackgroundColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(38, 185, 154,1)",
                        pointBorderWidth: 9,

                        data: {!! json_encode($dataBust) !!},
                    }, {
                        label: "Vòng 2(cm)",
                        fill:false,
                        backgroundColor: "rgba(255, 255, 0, 0.31)",
                        borderColor: "rgba(255, 255, 0, 0.7)",
                        pointBorderColor: "rgba(255, 255, 0, 0.7)",
                        pointBackgroundColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(255, 255, 0,1)",
                        pointBorderWidth: 9,
                        data: {!! json_encode($dataWais) !!},
                    }, {
                        label: "Vòng 3(cm)",
                        fill:false,
                        backgroundColor: "rgba(0, 255, 0, 0.31)",
                        borderColor: "rgba(0, 255, 0, 0.7)",
                        pointBorderColor: "rgba(0, 255, 0, 0.7)",
                        pointBackgroundColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(0, 255, 0,1)",
                        pointBorderWidth: 9,
                        data: {!! json_encode($dataHips) !!},
                    }]
                },
            });


        </script>
    @endif
@endpush