@extends('user_manage.master_manage')
@section('title')
    Quản lý tài chính
@endsection
@section('css')
    <style>
        #myTable_wrapper {
            width: 100%;
        }

        .table.dataTable tbody tr {
            background-color: #fff;
        }

        .table.dataTable tbody tr.odd {
            background-color: #f9f9f9;
        }

        .dataTables_wrapper .dataTables_filter input {
            display: inline;
            border: 1px solid;
        }

    </style>
    {{--until.css line42--}}
@endsection
@section('content-user')
    <!--user information-->
    <div class="menu-right-chgpass">
        <div class="col-sm-12">
            <div class="second-part-chgpass ">
                <div class="row">
                    {{--<div class="row">--}}
                        <div class="col-sm-4 input-with-label">
                            <label><i class="fa fa-shopping-cart"></i>Tổng đơn đã thanh toán: </label>
                            <label for=""><strong>{{$order->count()}}</strong></label>
                        </div>
                        <div class="col-sm-4 input-with-label">
                            <label><i class="fa fa-book"></i> Tổng khóa học đã mua : </label>
                            <label for=""><strong>{{$sum}}</strong></label>
                        </div>
                        <div class="col-sm-4 input-with-label">
                            <label><i class="fa fa-shopping-bag"></i>Tổng tiền: </label>
                            <label for=""><strong>{{number_format($total)}} </strong> ₫</label>
                        </div>
                    {{--</div>--}}
                </div>
            </div>
            <div class="da-hoc-content row" style="width: 100%">
                <table class="table table-hover text-center" id="myTable">
                    <thead>
                    <tr>
                        <th>Mã đơn hàng</th>
                        <th>Tổng tiền</th>
                        <th>Ngày đặt hàng</th>
                        <th>Trạng thái</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order as $key=> $od)
                        <tr>
                            <td>#{{$od->id}}</td>
                            <td>{{number_format($od->total)}} ₫</td>
                            <td>{{date('d-m-Y H:i',strtotime($od->created_at))}}</td>
                            @if($od->status == 0)
                                <td class="text-left" style="color: red"> Chưa thanh toán</td>
                            @else
                                <td class="text-left" style="color: green"> Đã thanh toán</td>

                            @endif
                            <td class="text-left"><a href="{{route('order.detailOrder',$od->id)}}"><i
                                            class="fa fa-edit"></i>  </a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>

        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush