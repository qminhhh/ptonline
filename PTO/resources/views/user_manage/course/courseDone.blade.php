@extends('welcome')
@section('title')
    Chi tiết bài học
@endsection
@section('content')
    <!--chi tiet khoa hoc-->
    <div id="container" class="detail-outside">
        <div class="detail_day">
            <div style="text-transform: none;">
                <a href="{{route('courses.myCourse')}}">Khóa học đang học</a> &#62; {{$course->courseName}}
            </div>
            <div class="main-detail-day">
                <div>
                    <div class="text-capitalize">
                        <h2>{{$course->courseName}}</h2>
                    </div>
                </div>
                <div class="row content-detail">
                    <div class="col-sm-4 left-detail left-detail-plus finished">
                        <div class="row content-left-detail">
                            <div class="col-sm-12">
                                <div class="font-weight-bold">
                                    <p>Tổng quát :</p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span>

                                     {!! $course->introduce !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Yêu Cầu Của Khóa Học :</p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span>
                                     {!! $course->require !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Lợi Ích Từ Khóa Học : </p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span class="article">
                                     {!! $course->benefit !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Đối Tượng Mục Tiêu :</p>
                                </div>
                                <div>
                                    <div>
                                    <span class="article">
                                     {!! $course->target !!}
                                    </span>
                                    </div>
                                </div>
                                <br>

                                <div class="text-center">
                                    <a href="{{route('courses.showCourse',$course->slug)}}">Thông tin chi tiết</a>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 right-detail right-detail-plus">
                        <div class="row">
                            <div class="de-cuong ">
                                <div class="col-sm-2">
                                    <div class="text-capitalize font-weight-bold btn btn-success">Đề cương khoá học
                                    </div>
                                </div>
                            </div>
                            <div class="custom-fieldset">
                                <div class="de-cuong">
                                    <div class="col-sm-12">
                                        @if(session('thongbao'))
                                            <div class="alert alert-success">
                                                {{session('thongbao')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @php
                                    $weeks=$course->lessons->chunk(7);
                                @endphp
                                @foreach($weeks as $key =>$week)
                                    <div class="row">
                                        <div class="col-sm-2 font-weight-bold">
                                            <div class="week">Tuần {{$key+1}}</div>
                                        </div>
                                        @foreach($week as $key=>$lesson)
                                            @if(!in_array($lesson->id,$lessonUserLearn))
                                                <div class="col-sm-1">
                                                    <a href="{{route('lessons.show',$lesson->id)}}">
                                                        <div class="dayOfWeek">{{$key+1}}</div>
                                                    </a>
                                                </div>
                                            @else
                                                <div class="col-sm-1">
                                                    <a href="{{route('lessons.show',$lesson->id)}}" style="color: white"><i
                                                                class="fa fa-check dayOfWeek " style="background-color: #35d420;"
                                                                aria-hidden="true"></i></a>
                                                </div>
                                            @endif

                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="text-center font-weight-bold" style="color: red;"><span>Tiến trình: </span><span>{{$process->process}}
                                %</span>
                        </div>
                        <div class="row"> <!--2 button ben duoi cung -->
                            <div class="col-sm-12 text-center ">
                                <div class="content-course-learned">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info progress-bar-striped"
                                             role="progressbar"
                                             aria-valuenow="{{$process->process}}" aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$process->process}}%">
                                            <span class="sr-only">{{$process->process}}% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-rate" style="border: 1px solid white;">
                            <br>
                            <div class="row">
                                @if($course->rating > 0)
                                    <div class="col-sm-12">
                                        <div class="text-center font-weight-bold"><strong><h1>{{$course->rating}}</h1></strong></div>
                                        <div class="text-center">
                                            <div class="stars-bottom">
                                                <span class="fa fa-star @if(1 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                <span class="fa fa-star @if(2 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                <span class="fa fa-star @if(3 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                <span class="fa fa-star @if(4 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                <span class="fa fa-star @if($course->rating ==5) checked @endif"></span>
                                            </div>
                                        </div>
                                        <div class="text-center">( {{$ratings->count()}} người đã đánh giá )</div>
                                    </div>
                                @else
                                    <div class="col-sm-12">
                                        <div class="text-center font-weight-bold"><strong><h1>0</h1></strong></div>
                                        <div class="text-center">
                                            <div class="stars-bottom">
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star "></span>
                                            </div>
                                        </div>
                                        <div class="text-center">( 0 người đã đánh giá )</div>
                                    </div>
                                @endif
                            </div>
                            <div> <!-- cmt cua nguoi dung -->
                                @foreach($ratings as $r)
                                    <div class="row user-comment">
                                        <div class="col-sm-2">
                                            <div>
                                                <img src="{{$r->user->image}}"/>
                                            </div>
                                        </div>
                                        <div class="text-capitalize right-user-comment col-sm-8 tip left"
                                             style="padding-bottom: 0px;">
                                            <div class="font-weight-bold col-sm-9 text-left">
                                                <span>{{$r->user->fullName}}</span>
                                            </div>
                                            <div class="col-sm-9 text-left" style="margin-top: 5px">
                                                <div class="stars-bottom-comment">
                                                    <span class="fa fa-star @if($r->star >=1) checked @endif"></span>
                                                    <span class="fa fa-star @if($r->star >=2) checked @endif"></span>
                                                    <span class="fa fa-star @if($r->star >=3) checked @endif"></span>
                                                    <span class="fa fa-star @if($r->star >=4) checked @endif"></span>
                                                    <span class="fa fa-star @if($r->star ==5) checked @endif"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 text-left"  style="margin-top: 5px;text-transform: none;">
                                                {!! $r->comment !!}
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                                {{--<div class="load-more" style="margin-top: 10px"><a href="">Xem thêm</a></div>--}}
                                <br>
                            </div>
                        </div>
                        @if($rate == null )
                            <div class="row">
                                <div class="col-sm-12">
                                    @if(session('thongbao1'))
                                        <div class="alert alert-success">
                                            {{session('thongbao1')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <form action="{{route('courses.storeRate',$course->id)}}" enctype="multipart/form-data"
                                  method="POST" role="form" id="frmRate">
                                @csrf
                                <div class="row text-capitalize rate "> <!--div nay chua star-->
                                    <div class="stars">
                                        @if($rate != null)
                                            @foreach($rate as $r)
                                                <input class="star star-1" id="star-1" type="radio" name="rate"
                                                       value="1"
                                                       checked/>
                                                <label class="star star-1" for="star-1"></label>
                                            @endforeach
                                        @else
                                            <input class="star star-5" id="star-5" type="radio" name="rate"
                                                   value="5"/>
                                            <label class="star star-5" for="star-5"></label>
                                            <input class="star star-4" id="star-4" type="radio" name="rate"
                                                   value="4"/>
                                            <label class="star star-4" for="star-4"></label>
                                            <input class="star star-3" id="star-3" type="radio" name="rate"
                                                   value="3"/>
                                            <label class="star star-3" for="star-3"></label>
                                            <input class="star star-2" id="star-2" type="radio" name="rate"
                                                   value="2"/>
                                            <label class="star star-2" for="star-2"></label>
                                            <input class="star star-1" id="star-1" type="radio" name="rate"
                                                   value="1"
                                                   checked/>
                                            <label class="star star-1" for="star-1"></label>
                                        @endif
                                    </div>
                                </div>
                                <div class="row text-center" style="margin-left: 60px;">
                                    <div class="form-group ">
                                        <p for="">Nhập đánh giá: </p>
                                        @if($rate != null )
                                            <textarea name="comment" id="input"
                                                      class="form-control ckeditor {{ $errors->has('comment') ? ' is-invalid' : '' }}"
                                                      rows="3"
                                                      required="required"
                                                      readonly> {!! $rate->comment !!} </textarea>
                                        @else
                                            <textarea name="comment" id="input"
                                                      class="form-control ckeditor {{ $errors->has('comment') ? ' is-invalid' : '' }}"
                                                      rows="3"
                                                      required="required"></textarea>
                                        @endif
                                        @if ($errors->has('comment'))
                                            <span class="text-danger">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="box-footer" style="margin-left: 300px;">
                                    <button type="submit" class="btn btn-primary">Đánh giá</button>
                                </div>
                            </form>

                        @endif
                    </div>
                </div>
                {{--</div>--}}
                <div class="col-sm-10">

                </div>
            </div>
        </div>
    </div>
    <!--END chi tiet khoa hoc-->
@endsection

@push('scripts')
    <script>
        $('.article').readmore({
            speed: 75,
            lessLink: '<a href="#">Thu gọn</a>',
            moreLink: '<a href="#">Xem thêm</a>',
            collapsedHeight: 110,
        });
    </script>
@endpush