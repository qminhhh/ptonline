@extends('user_manage.master_manage')
@section('title')
    Khóa học
@endsection

@section('css')
    <style>
        #myTable_wrapper {
            width: 100%;
        }

        .table.dataTable tbody tr {
            background-color: #fff;
        }

        .table.dataTable tbody tr.odd {
            background-color: #f9f9f9;
        }

        .dataTables_wrapper .dataTables_filter input {
            display: inline;
            border: 1px solid;
        }

    </style>
    {{--until.css line42--}}
@endsection
@section('content-user')
    <!--user information-->
    <div class="menu-right-tab-course ">
        <div class="row tab-outside">
            <div class="col-sm-4 text-center">
                <div class="tablink " onclick="button_switch(event,'learning-content')" id="defaultOpen">Đang học</div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="tablink" onclick="button_switch(event,'learned-content') ">Đã học</div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="tablink active_tab" onclick="button_switch(event,'pending-content') ">Chờ kích hoạt</div>
            </div>
        </div>
        <div id="learning-content" class=" tab-content" ><!-- div chua noi dung cac khoa dang hoc -->
            <br>
            <div class="text-center margin-10">
                @if($courseStudying->count() == 0 )
                    <h2>Chưa có khóa học nào đang học</h2>
                    <h5>Click <a href="{{route('courses.showAll')}}">vào đây</a> để mua khóa học</h5>
                @else
                    <h2>Chi tiết các khoá học đang học</h2>
                @endif
            </div>
            <br>
            <div class="da-hoc-content row">
                @foreach($courseStudying as $cm)
                    <div class="col-sm-6">
                        <div class="col-sm-12 content-course-learned-image">
                            <img src="{{$cm->course->image}}"/>
                        </div>
                        <div class="content-course-learned-text">
                            <div class="margin-10">
                                <div class="font-weight-bold text-center">{{$cm->course->courseName}}</div>
                            </div>
                            <div class="content-course-learned">
                                <div class="">PTO
                                    : {{$cm->course->user->fullName}}</div>
                            </div>
                            <div class="content-course-learned">
                                <div class="">Thời gian học: {{$cm->course->lessons->count()}} ngày</div>
                            </div>
                            @if($cm->process > 0)
                                <div class="content-course-learned">
                                    <div class="">Tiến trình học : {{$cm->process}}%</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info progress-bar-striped"
                                             role="progressbar"
                                             aria-valuenow="{{$cm->process}}" aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$cm->process}}%">
                                            <span class="sr-only">{{$cm->process}}% Complete</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 btn btn-dahoc">
                                        <a href="{{route('courses.detail',$cm->course->id)}}" style="color: wheat"><i
                                                    class="fa fa-arrow-right"></i>Tiếp tục học</a>
                                    </div>
                                </div>
                            @else
                                <div class="content-course-learned">
                                    <div class="">Tiến trình học : 0%</div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info progress-bar-striped"
                                             role="progressbar"
                                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 btn btn-dahoc">
                                        <a href="{{route('courses.detail',$cm->course->id)}}" style="color: wheat"><i
                                                    class="fa fa-play-circle"></i>Bắt đầu học</a>

                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div id="learned-content" class="tab-content"><!-- div chua noi dung cac khoa da hoc -->
            <br>
            <div class="text-center margin-10">
                @if($courseDone->count() == 0 )
                    <h2>Bạn chưa hoàn thành khóa học nào</h2>
                @else
                    <h2>Chi tiết các khoá học đã hoàn thành</h2>
                @endif

            </div>
            <br>
            <div class="da-hoc-content row">
                @foreach($courseDone as $cdn)
                    <div class="col-sm-6">
                        <div class="col-sm-12 content-course-learned-image">
                            <img src="{{$cdn->course->image}}"/>
                        </div>
                        <div class="content-course-learned-text">
                            <div class="margin-10">
                                <div class="font-weight-bold text-center">{{$cdn->course->courseName}}</div>
                            </div>
                            <div class="content-course-learned">
                                <div class="">PTO
                                    : {{$cdn->course->user->fullName}} </div>
                            </div>
                            <div class="content-course-learned">
                                <div class="">Thời gian học: {{$cdn->course->lessons->count()}} ngày</div>
                            </div>
                            <div class="content-course-learned">
                                <div class="">Tiến trình học : {{$cdn->process}}%</div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info progress-bar-striped"
                                         role="progressbar"
                                         aria-valuenow="{{$cdn->process}}" aria-valuemin="0" aria-valuemax="100"
                                         style="width: {{$cdn->process}}%">
                                        <span class="sr-only">{{$cdn->process}}% Complete</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center">
                                <div class="col-sm-3 btn btn-dahoc">
                                    <a href="{{route('courses.detail',$cdn->course->id)}}" style="color: wheat"><i
                                                class="fa fa-play-circle"></i>Xem lại</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div id="pending-content" class="tab-content" style="display: block;">
            <br>
            <div class="text-center margin-10">
                @if($orderCourse->count() == 0 )
                    <h2>Chưa có khóa học nào chờ kích hoạt</h2>
                    <h5>Click <a href="{{route('courses.showAll')}}">vào đây</a> để mua khóa học</h5>
                @else
                    <h2>Chi tiết các khóa chờ kích hoạt</h2>
                    <br>
                    <div class="da-hoc-content row">
                        <table class="table table-hover text-center" id="myTable">
                            <thead>
                            <tr>
                                <th>Mã đơn hàng</th>
                                <th>Tổng tiền</th>
                                <th>Ngày đặt hàng</th>
                                <th>Trạng thái</th>
                                <th>Kích hoạt</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orderCourse as $key=> $oc)
                                <tr>
                                    <td>#{{$oc->id}}</td>
                                    <td>{{number_format($oc->total)}} ₫</td>
                                    <td>{{date('d-m-Y H:i',strtotime($oc->created_at))}}</td>
                                    @if($oc->status == 0)
                                        <td class="text-left" style="color:red"> Chưa thanh toán</td>
                                    @else
                                        <td class="text-left" style="color: green">Đã thanh toán</td>
                                    @endif
                                    @if($oc->status_active == 0)
                                        @if($oc->tus == 0)

                                            <td><a href="{{route('order.detailOrder',$oc->id)}}">Chờ kích hoạt</a></td>
                                        @else

                                            <td><a href="{{route('order.detailOrder',$oc->id)}}">Kích hoạt ngay</a></td>
                                        @endif
                                    @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                @endif
            </div>

        </div>


    </div>


    <!--END user information-->

@endsection



@push('scripts')
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush