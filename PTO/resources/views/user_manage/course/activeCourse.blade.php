@extends('user_manage.master_manage')
@section('content-user')
    <div class="menu-right-chgpass">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <strong><label>Chi tiết đơn hàng #{{$order->id}}</label></strong>
                    </div>
                </div>

                <div class="col-md-4 m-t-40">
                    <div class="textChgPass">
                        <label>Địa chỉ: </label>
                    </div>
                    <div class="textChgPass">
                        <label>@if($order->name == null){{$order->user->fullName}} @else {{$order->name}}@endif</label>
                    </div>
                    <div class="textChgPass">
                        <label>@if($order->phone == null){{$order->user->phone}} @else {{$order->phone}}@endif</label>
                    </div>
                    <div class="textChgPass">
                        <label>{{$order->address}} {{$order->district}} {{$order->province}}</label>
                    </div>
                </div>
                <div class="col-md-4 m-t-40">
                    <br>
                    <div class="textChgPass">
                        <label>Khóa học : {{$order->orderdetails->count()}}</label>
                    </div>
                    <div class="textChgPass">
                        <label>Ngày đặt : {{date('d-m-Y H:i',strtotime($order->created_at))}}</label>
                    </div>
                    <div class="textChgPass">
                        <label>Tổng tiền : <strong>{{number_format($order->total)}} ₫</strong></label>
                    </div>
                </div>
                <div class="col-md-4 m-t-40">
                    @if($order->tus == 1)
                        @if($order->status_active == 0)
                            <form action="{{route('order.activeOrder',$order->id)}}" method="post" role="form"
                                  id="frm-code">
                                @csrf
                                <div>
                                    <strong><label>Nhập mã kích hoạt</label></strong>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nhập mã" name="active_code"
                                           required>
                                </div>
                                <div>
                                    @if(session('thongbao'))
                                        <div class="alert alert-danger">
                                            {{session('thongbao')}}
                                        </div>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary">Kích hoạt</button>

                            </form>
                        @else
                            <div>
                                <strong><label style="color: green"> Đơn hàng đã được kích hoạt!</label></strong>
                            </div>
                            <div class="row text-center">
                                <a href="{{route('courses.myCourse')}}" class="text-center"><i class="fa fa-angle-double-right"></i>Vào học ngay</a>
                            </div>
                        @endif
                    @else
                        <div>
                            <strong><label style="color: red"> Đơn hàng đang xử lý !</label></strong>
                        </div>
                        <div>
                            <label style=""> * Đơn hàng của bạn đang được xử lý. Chúng tôi sẽ gửi mã kích hoạt sớm nhất cho bạn !</label>
                        </div>
                    @endif
                </div>

            </div>

            <div class="chart-infor-left row">
                @foreach($order->orderdetails as $od)
                    <div class="row">
                        <div class="col-sm-3">
                            <p>
                                <img style="width: 60%;" src="{{$od->course->image}}" alt="IMG">
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <p>
                                <label><b>Khoá học:</b> <a href="#">{{$od->course->courseName}}</a></label>
                            </p>
                            <p>

                            </p>
                            <p>
                                <label><b>PTO :</b> {{$od->course->user->fullName}}
                                </label>
                            </p>
                        </div>
                        <div class="col-sm-3">
                            <span><b>  {{ number_format($od->course->price) }} </b></span><span><b> ₫</b></span><br/>
                        </div>
                    </div>
                    <br>
                @endforeach
            </div>
        </div>

        <div class="col-sm-6 text-capitalize update-button">
            <a href="{{route('courses.myCourse')}}"><i class="fa fa-arrow-circle-left"></i>Quay lại</a>
        </div>
    </div>
    </div>

@endsection

@push('script')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
