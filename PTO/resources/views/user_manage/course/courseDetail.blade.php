@extends('welcome')
@section('title')
    Chi tiết Khóa học
@endsection
@section('content')
    <!--chi tiet khoa hoc-->
    <div id="container" class="detail-outside">
        <div class="detail_day">
            <div  style="text-transform: none;">
                <a href="{{route('courses.myCourse')}}">Khóa học đang học</a> &#62; {{$course->courseName}}
            </div>
            <div class="main-detail-day">
                <div style="border: 1px solid lightgrey;">
                    <div class="text-capitalize">
                        <h2>{{$course->courseName}}</h2>
                    </div>
                </div>
                <div class="row content-detail">
                    <div class="col-sm-4 left-detail left-detail-plus finished">
                        <div class="row content-left-detail">
                            <div class="col-sm-12">
                                <div class="font-weight-bold">
                                    <p>Tổng quát :</p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span>
                                     {!! $course->introduce !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Yêu Cầu Của Khóa Học :</p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span>
                                     {!! $course->require !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Lợi Ích Từ Khóa Học : </p>
                                </div>
                                <div>
                                    <div class="article" style="overflow: hidden;">
                                    <span>
                                     {!! $course->benefit !!}
                                    </span>
                                    </div>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>Đối Tượng Mục Tiêu :</p>
                                </div>
                                <div>
                                    <div>
                                    <span class="article">
                                     {!! $course->target !!}
                                    </span>
                                    </div>
                                </div>
                                <br>

                                <div class="text-center">
                                    <a href="{{route('courses.showCourse',$course->slug)}}">Thông tin chi tiết</a>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 right-detail right-detail-plus">
                        @if($course->lock == 2)
                            <div class="row text-center">
                                <div class="de-cuong text-center">
                                    <div class="col-sm-12 text-center">
                                        <div class="text-center font-weight-bold btn btn-success" style="margin-top: 100px;margin-left:100px ">
                                            Khóa học hiện đang khóa , chúng tôi sẽ mở lại khóa học sớm nhất !
                                            <br>
                                            Bạn có thể tham khảo các khóa học khác <a href="{{route('courses.showAll')}}" style="color: yellow">Tại đây</a>.
                                            <br>
                                            Xin lỗi vì sự bất tiện này !
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="de-cuong ">
                                    <div class="col-sm-2">
                                        <div class="text-capitalize font-weight-bold btn btn-success">đề cương khoá học
                                        </div>
                                    </div>
                                </div>

                                <div class="custom-fieldset">
                                    <div class="de-cuong">
                                        <div class="col-sm-12">
                                            @if(session('thongbao'))
                                                <div class="alert alert-success">
                                                    {{session('thongbao')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @php
                                        $weeks=$course->lessons->chunk(7);
                                    @endphp
                                    @foreach($weeks as $key =>$week)
                                        <div class="row">
                                            <div class="col-sm-2 font-weight-bold">
                                                <div class="week">Tuần {{$key+1}}</div>
                                            </div>
                                            @foreach($week as $key=>$lesson)
                                                @if(!in_array($lesson->id,$lessonUserLearn))
                                                    <div class="col-sm-1">
                                                        <a href="{{route('lessons.show',$lesson->id)}}">
                                                            <div class="dayOfWeek">{{$key+1}}</div>
                                                        </a>
                                                    </div>
                                                @else
                                                    <div class="col-sm-1">
                                                        <a href="{{route('lessons.show',$lesson->id)}}"
                                                           style="color: white"><i
                                                                    class="fa fa-check dayOfWeek "
                                                                    style="background-color: #35d420;"
                                                                    aria-hidden="true"></i></a>
                                                    </div>
                                                @endif

                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="text-center font-weight-bold" style="color: red;"><span>Tiến trình: </span>
                                @if($process->process > 0)
                                    <span>{{$process->process}} %</span>
                                @else
                                    <span>0 %</span>
                                @endif
                            </div>
                            <div class="row"> <!--2 button ben duoi cung -->
                                <div class="col-sm-12 text-center ">
                                    <div class="content-course-learned">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info progress-bar-striped"
                                                 role="progressbar"
                                                 aria-valuenow="{{$process->process}}" aria-valuemin="0"
                                                 aria-valuemax="100"
                                                 style="width: {{$process->process}}%">
                                                <span class="sr-only">{{$process->process}}% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END chi tiet khoa hoc-->
@endsection

@push('scripts')
    <script>
        $('.article').readmore({
            speed: 75,
            lessLink: '<a href="#">Thu gọn</a>',
            moreLink: '<a href="#">Xem thêm</a>',
            collapsedHeight: 110,
        });
    </script>
@endpush