@extends('welcome')
@section('title')
    Chi tiết bài học
@endsection
@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <style>
        /**
 * Oscuro: #283035
 * Azul: #03658c
 * Detalle: #c7cacb
 * Fondo: #dee1e3
 ----------------------------------*/
        * {
            margin: 0;
            padding: 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        a {
            color: #03658c;
            text-decoration: none;
        }

        ul {
            list-style-type: none;
        }

        body {
            font-family: 'Roboto', Arial, Helvetica, Sans-serif, Verdana;
            background: #dee1e3;
        }

        /** ====================
         * Lista de Comentarios
         =======================*/
        .comments-container {
            margin: 60px auto 15px;
            width: 768px;
        }

        .comments-container h1 {
            font-size: 36px;
            color: #283035;
            font-weight: 400;
        }

        .comments-container h1 a {
            font-size: 18px;
            font-weight: 700;
        }

        .comments-list {
            margin-top: 30px;
            position: relative;
        }

        /**
         * Lineas / Detalles
         -----------------------*/
        .comments-list:before {
            content: '';
            width: 2px;
            height: 100%;
            background: #c7cacb;
            position: absolute;
            left: 32px;
            top: 0;
        }

        .comments-list:after {
            content: '';
            position: absolute;
            background: #c7cacb;
            bottom: 0;
            left: 27px;
            width: 7px;
            height: 7px;
            border: 3px solid #dee1e3;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }

        .reply-list:before, .reply-list:after {
            display: none;
        }

        .reply-list li:before {
            content: '';
            width: 60px;
            height: 2px;
            background: #c7cacb;
            position: absolute;
            top: 25px;
            left: -55px;
        }

        .comments-list li {
            margin-bottom: 15px;
            display: block;
            position: relative;
        }

        .comments-list li:after {
            content: '';
            display: block;
            clear: both;
            height: 0;
            width: 0;
        }

        .reply-list {
            padding-left: 88px;
            clear: both;
            margin-top: 15px;
        }

        /**
         * Avatar
         ---------------------------*/
        .comments-list .comment-avatar {
            width: 65px;
            height: 65px;
            position: relative;
            z-index: 99;
            float: left;
            border: 3px solid #FFF;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            overflow: hidden;
        }

        .comments-list .comment-avatar img {
            width: 100%;
            height: 100%;
        }

        .reply-list .comment-avatar {
            width: 50px;
            height: 50px;
        }

        .comment-main-level:after {
            content: '';
            width: 0;
            height: 0;
            display: block;
            clear: both;
        }

        /**
         * Caja del Comentario
         ---------------------------*/
        .comments-list .comment-box {
            width: 680px;
            float: right;
            position: relative;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
        }

        .comments-list .comment-box:before, .comments-list .comment-box:after {
            content: '';
            height: 0;
            width: 0;
            position: absolute;
            display: block;
            border-width: 10px 12px 10px 0;
            border-style: solid;
            border-color: transparent #FCFCFC;
            top: 8px;
            left: -11px;
        }

        .comments-list .comment-box:before {
            border-width: 11px 13px 11px 0;
            border-color: transparent rgba(0, 0, 0, 0.05);
            left: -12px;
        }

        .reply-list .comment-box {
            width: 610px;
        }

        .comment-box .comment-head {
            background: #FCFCFC;
            padding: 10px 12px;
            border-bottom: 1px solid #E5E5E5;
            overflow: hidden;
            -webkit-border-radius: 4px 4px 0 0;
            -moz-border-radius: 4px 4px 0 0;
            border-radius: 4px 4px 0 0;
        }

        .comment-box .comment-head i {
            float: right;
            margin-left: 14px;
            position: relative;
            top: 2px;
            color: #A6A6A6;
            cursor: pointer;
            -webkit-transition: color 0.3s ease;
            -o-transition: color 0.3s ease;
            transition: color 0.3s ease;
        }

        .comment-box .comment-head i:hover {
            color: #03658c;
        }

        .comment-box .comment-name {
            color: #283035;
            font-size: 14px;
            font-weight: 700;
            float: left;
            margin-right: 10px;
        }

        .comment-box .comment-name a {
            color: #283035;
        }

        .comment-box .comment-head span {
            float: left;
            color: #999;
            font-size: 13px;
            position: relative;
            top: 1px;
        }

        .comment-box .comment-content {
            background: #FFF;
            padding: 12px;
            font-size: 15px;
            color: #595959;
            -webkit-border-radius: 0 0 4px 4px;
            -moz-border-radius: 0 0 4px 4px;
            border-radius: 0 0 4px 4px;
        }

        .comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {
            color: #03658c;
        }

        .comment-box .comment-name.by-author:after {
            content: 'PTO';
            background: #03658c;
            color: #FFF;
            font-size: 12px;
            padding: 3px 5px;
            font-weight: 700;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        /** =====================
         * Responsive
         ========================*/
        @media only screen and (max-width: 766px) {
            .comments-container {
                width: 480px;
            }

            .comments-list .comment-box {
                width: 390px;
            }

            .reply-list .comment-box {
                width: 320px;
            }
        }
    </style>
@endsection
@section('content')
    <!--chi tiet ngay hoc-->
    <div id="container" class="detail-outside">
        <div class="detail_day" style="padding-top:0">
            <div style="text-transform: none;background: #f8f8f8;padding-bottom: 20px;padding-top: 20px">
                <a href="{{route('courses.myCourse')}}">Khóa học đang học</a> &#62; <a
                        href="{{route('courses.detail',$lesson->course->id)}}">{{$lesson->course->courseName}}</a> &#62;<span> {{$lesson->lessonName}}</span>
            </div>
            <div class="main-detail-day">
                <div style="border: 1px solid lightgrey">
                    <div>
                        <h2>{{$lesson->lessonName}}</h2>
                    </div>
                </div>
                <div class="row content-detail">
                    <div class="col-sm-4 left-detail left-detail-plus" style="max-height: 100%">
                        <div class="text-center">
                            <h3>Thực đơn</h3>
                        </div>
                        <div class="row content-left-detail">
                            <div class="col-sm-12">
                                <div>
                                    <div>
                                    <span>
                                    {!! $lesson->menu !!}
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 right-detail right-detail-plus">
                        <div class="font-weight-bold h3-margin text-center">
                            <h3>Nội dung bài học</h3>
                        </div>
                        <div>
                            <div class="font-weight-bold">
                                <p>1. Nội dung :</p>
                            </div>
                            <div>
                                <span>
                                  {!! $lesson->content !!}
                                </span>
                            </div>
                            <br>
                            @if($lesson->video != null)
                                <div class="font-weight-bold">
                                    <p>2. Video hướng dẫn :</p>
                                </div>
                                <div>
                                <span>
                                  <iframe width="560" height="315"
                                          src="https://www.youtube.com/embed/{{$lesson->video}}" frameborder="0"
                                          allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </span>
                                </div>
                                <br>
                                <div class="font-weight-bold">
                                    <p>3. Tài liệu : </p>
                                </div>
                                <div>
                                    <div>
                                    <span>
                                       {!!  $lesson->linkVideo!!}
                                    </span>
                                    </div>
                                </div>
                            @else
                                <div class="font-weight-bold">
                                    <p>2. Tài liệu : </p>
                                </div>
                                <div>
                                    <div>
                                    <span>
                                        {!!  $lesson->linkVideo!!}
                                    </span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="text-center ">
                            @if($status->status == 0)
                                <form action="{{route('course_manage.lessonDone',$lesson->id)}}" method="post"
                                      role="form">
                                    @csrf
                                    @method("put")
                                    <button type="submit" class="btn btn-success text-capitalize">Hoàn thành</button>
                                </form>
                            @else
                                <a href="{{route('courses.detail',$lesson->course->id)}}"
                                   class="btn btn-success text-capitalize">Quay lại</a>
                            @endif

                        </div>
                    </div>
                    <div class="col-md-11 col-md-offset-1 right-detail right-detail-plus ">
                        <div class="comments-container">
                            <form action="{{route('comment.store',$lesson->id)}}" enctype="multipart/form-data"
                                  method="POST" role="form">
                                @csrf
                                <input type="hidden" name="parent_id" value="0">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img
                                                src="{{\Illuminate\Support\Facades\Auth::user()->image}}"
                                                style="width: 55%; margin-top: 25px; margin-left: 50px;"
                                                alt="">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group ">
                                            <p for="">Nhập bình luận: </p>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="2" cols="80"
                                                          name="comment"
                                                          placeholder="Ý kiến của bạn về bài học:" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 50px">
                                        <button type="submit" class="btn btn-primary">Đăng</button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            @if($comment != null )
                                <h4>Ý kiến học viên : </h4>
                                <ul id="comments-list" class="comments-list">
                                    @foreach($comment as $key=>$cmt)
                                        <li>
                                            <div class="comment-main-level">
                                                <!-- Avatar -->
                                                <div class="comment-avatar"><img
                                                            src="{{$cmt->user->image}}"
                                                            alt=""></div>
                                                <!-- Contenedor del Comentario -->
                                                <div class="comment-box">
                                                    <div class="comment-head">
                                                        <h6 class="comment-name @if($cmt->user->status == 2) by-author @endif">
                                                            <a
                                                                    href="http://creaticode.com/blog">
                                                                {{$cmt->user->lastName}} {{$cmt->user->firstName}}
                                                            </a></h6>
                                                        <span>{{date('d-m-Y H:i', strtotime($cmt->created_at))}}</span>
                                                        @if(Auth::id() == $cmt->idUser)
                                                            <form class="form-group"
                                                                  action="{{route('comment.destroy',$cmt->id)}}"
                                                                  method="POST">
                                                                @csrf
                                                                @method("DELETE")
                                                                <button type="button" class="btn-del"><i
                                                                            class="fa fa-trash"></i></button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                    <div class="comment-content">
                                                        {!! $cmt->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Respuestas de los comentarios -->
                                            <ul class="comments-list reply-list ">
                                                @foreach($cmt->replies as $reply)
                                                    <li>
                                                        <!-- Avatar -->
                                                        <div class="comment-avatar"><img
                                                                    src="{{$reply->user->image}}"
                                                                    alt=""></div>
                                                        <!-- Contenedor del Comentario -->
                                                        <div class="comment-box">
                                                            <div class="comment-head ">
                                                                <h6 class="comment-name @if($reply->user->status == 2) by-author @endif">
                                                                    <a
                                                                            href="http://creaticode.com/blog">
                                                                        {{$reply->user->fullName}} </a>
                                                                </h6>
                                                                <span>{{date('d-m-Y H:i', strtotime($reply->created_at))}}</span>
                                                                @if(Auth::id() == $reply->idUser)
                                                                    <form class="form-group"
                                                                          action="{{route('comment.destroy',$reply->id)}}"
                                                                          method="POST">
                                                                        @csrf
                                                                        @method("DELETE")
                                                                        <button type="button" class="btn-del"><i
                                                                                    class="fa fa-trash"></i></button>
                                                                    </form>
                                                                @endif
                                                            </div>
                                                            <div class="comment-content">
                                                                {!! $reply->content !!}
                                                            </div>
                                                        </div>
                                                    </li>

                                                @endforeach
                                                <li>
                                                    <form action="{{route('comment.store',$lesson->id)}}"
                                                          enctype="multipart/form-data" method="POST" role="form"
                                                          id="frmRate">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <div class="comment-avatar"><img
                                                                            src="{{\Illuminate\Support\Facades\Auth::user()->image}}"
                                                                            alt=""></div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <input type="hidden" name="parent_id"
                                                                       value="{{$cmt->id}}">
                                                                <div class="form-group">
                                                                    <textarea class="form-control" rows="2" cols="80"
                                                                              name="comment" style="margin-left: 10px;"
                                                                              placeholder="Nhập bình luận"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2" style="margin-top: 15px">
                                                                <button type="submit" class="btn btn-primary">Đăng
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endforeach

                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END chi tiet ngay hoc-->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-del').click(function () {
                var form = $(this).parent('form');
                swal({
                    title: "Bạn muốn xóa bình luận này ?",
                    text: "",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });
    </script>
@endpush