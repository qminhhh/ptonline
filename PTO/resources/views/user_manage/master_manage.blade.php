
@extends('welcome')

@section('content')
    <!--menu-->
    <div  class="card-outside">
        <div class="cart_status row">
            <div class="col-sm-3 menu_left">
                <div class="row avatar_user">
                    <div >
                        <p>
                            <img src="{{\Illuminate\Support\Facades\Auth::user()->image}}">
                        </p>
                    </div>
                    <div class="textUser">
                        <div>
                            <label>{{\Illuminate\Support\Facades\Auth::user()->fullName}}
                            </label>
                        </div>
                        <div  class="row">
                            <div class="col-sm-3">
                                <i class="fa fa-pencil" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <a href="{{route('users.myAccount',Auth::id())}}"> <label class="pointer-user">Sửa thông tin</label></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="change-user-infor">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-user-circle-o" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <a href="#"> <label class="pointer-user">Thông tin cá nhân</label></a>
                            </div>
                        </div>
                        <div class="menu-child" >
                            <a href="{{route('users.myAccount',Auth::id())}}"> <label class="pointer-user">Tài khoản của tôi</label></a>
                        </div>
                        <div class="menu-child">
                            <a href="{{route('users.changePass',Auth::id())}}"><label class="pointer-user">Đổi mật khẩu</label></a>
                        </div>
                    </div>
                    <div class="lesson-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-book" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <label style="color: #FF6600; text-transform: capitalize"><a href="{{route('courses.myCourse')}}">Khoá học</a></label>
                            </div>
                        </div>
                    </div>
                    <div class="chart-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-line-chart" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <a href="{{route('users.chart',Auth::id())}}"> <label class="pointer-user">Thể trạng cơ thể</label></a>
                            </div>
                        </div>
                    </div>
                    <div class="change-user-infor">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-usd" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <a href="{{route('order.manage')}}"> <label class="pointer-user">Quản lý thanh toán</label></a>
                            </div>
                        </div>
                    </div>
                    <div class="lesson-menu">
                        <div class="row">
                            <div class="col-sm-2">
                                <i class="fa fa-bell" aria-hidden="true" ></i>
                            </div>
                            <div >
                                <a href="{{route('notify.showNotify')}}"> <label class="pointer-user">Thông báo </label><label
                                            for="">( {{$u_notifies}} )</label></a>
                            </div>
                        </div>
                    </div>


                </div>

            </div>


            @yield('content-user')



        </div>
    </div>

@endsection

@push('scripts')

@endpush