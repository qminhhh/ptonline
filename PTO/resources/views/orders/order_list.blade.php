@extends('master_layout.admin.index')
@section('titles')
    Danh sách đơn hàng
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Đơn hàng
            <small>danh sách</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('order.list')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách đơn hàng</div>
                    <div class="panel-body">
                        <table id="order-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Người mua</th>
                                <th>Tổng tiền</th>
                                <th>Ngày đặt hàng</th>
                                <th>Hình thức</th>
                                <th>Trạng thái</th>
                                <th>Mã kích hoạt</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script_footer')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('order.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'total_nice', name: 'total_nice'},
                    {data: 'created_at', name: 'created_at'},
                    {
                        data: function (data) {
                            if (data.method == 1) {
                                return '<span class="label label-success">Online</span>';
                            }
                            else {
                                return '<span class="label label-primary">Ship COD</span>';
                            }
                        }, name: 'method'
                    },
                    {
                        data: function (data) {
                            if (data.status != null) {
                                if (data.status == 0) {
                                    return '<span class="label label-danger">Chưa thanh toán</span>';
                                }
                                else {
                                    return '<span class="label label-success">Đã thanh toán</span> ';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {
                        data: function (data) {
                            if (data.tus != null) {
                                if (data.tus == 0) {
                                    return '<span class="label label-danger">Chưa xuất</span>';
                                }
                                else {
                                    return '<span class="label label-success">Đã xuất</span> ';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'tus'
                    },
                    {data: 'actions', name: 'actions'},
                ],
            });
        });
    </script>
@endpush
