@extends('master_layout.admin.index')
@section('titles')
    Chi tiết đơn hàng
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chi tiết đơn hàng <strong>#{{$order->id}}</strong>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('order.show',$order)}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <section class="invoice">
            <form action="{{route('order.orderSave',$order->id)}}" method="POST">
                <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            @method('PUT')
            <!-- title row -->
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            <i class="fa fa-globe"></i>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Người mua :
                        <strong> @if($order->name == null){{$order->user->fullName}} @else {{$order->name}}@endif</strong><br>
                        SĐT :
                        <strong>@if($order->phone == null){{$order->user->phone}} @else {{$order->phone}}@endif</strong><br>
                        Email :
                        <strong>@if($order->email == null){{$order->user->email}} @else {{$order->email}}@endif</strong><br>

                    </div>

                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Order ID:</b> {{$order->id}}<br>
                        <b>Ngày đặt:</b> {{date('d-m-Y H:i',strtotime($order->created_at))}}<br>
                        <b>Mã kích hoạt:</b> @if($order->active_code == 0)  Đơn hàng đã được kích
                        hoạt @else {{$order->active_code}} @endif<br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <br>
                <br>
                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên khóa học</th>
                                <th>Giá</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=1 @endphp
                            @foreach($order->orderdetails as $odt)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$odt->course->courseName}}</td>
                                    <td>{{number_format($odt->price)}} ₫</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <br>
                <br>
                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
                        {{--<p class="lead">Amount Due 2/22/2014</p>--}}

                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="width:50%;padding-top: 20px;">Tổng tiền:</th>
                                    <td><h4>{{number_format($order->orderdetails->sum('price'))}} ₫</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        @if($order->status == 1)
                                            <button type="button" class="btn btn-block btn-success"><i class="icon fa fa-check"></i> Đã thanh toán !</button>

                                        @else
                                            <button type="button" class="btn btn-block btn-danger"><i class="icon fa fa-ban"></i> Chưa thanh toán !</button>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-6">
                        <label>Trạng thái đơn hàng: </label>
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline">

                                    @if($order->tus == 1)
                                        <select class="form-control input-inline" style="width: 200px">
                                            <option readonly="">Đã xuất mã kích hoạt</option>
                                        </select>
                                    @else
                                        <select name="tus" class="form-control input-inline" style="width: 200px">
                                            <option value="1" {{$order->tus==1?'selected':''}} >Xuất mã kích hoạt
                                            </option>
                                            <option value="0" {{$order->tus==0?'selected':''}}>Đang xử lý</option>
                                        </select>
                                        <input type="submit" value="Xử lý" class="btn btn-primary">
                                    @endif

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </form>
        </section>
    </section>

@endsection