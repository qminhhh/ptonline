@extends('master_layout.admin.index')
@section('titles')
    Quản lý tài chính
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tài Chính
            <small>Chi tiết</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('order.listCourse')}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Tổng doanh thu : <strong>{{number_format($total)}} ₫</strong></div>
                    <div class="panel-heading">Tổng doanh thu <strong>-</strong> ví PTO(-10%):
                        <strong>{{number_format($total*90/100) }}₫</strong></div>
                    <div class="panel-body">
                        <table id="payment-table" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên khóa học</th>
                                <th>Người mua</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Ngày mua</th>
                                <th>Giá</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"/>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#payment-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('order.dataCourse') !!}',
                columns: [
                    {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {data: 'nameCourse', name: 'nameCourse'},
                    {data: 'nameUser', name: 'nameUser'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'dateBuy', name: 'dateBuy'},
                    {data: 'price', name: 'price'},
                ],

                footerCallback: function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(4, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(4).footer()).html(
                        '$' + pageTotal + ' ( $' + total + ' total)'
                    );
                }
            });
        });
    </script>
@endpush
