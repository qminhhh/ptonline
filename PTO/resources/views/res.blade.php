<!--header-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title> @yield('titles')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ URL::to('images/icons/favicon.png')}}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('fonts/iconic/css/material-design-iconic-font.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/animsition/css/animsition.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/main.css')}}">
    <!--===============================================================================================-->

    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!--css moi-->

    <!--<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>-->
    <!--<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>-->

    <link rel="stylesheet" href="{{ URL::to('css/style.css')}}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ URL::to('css/style2.css')}}" type="text/css" media="all">
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <style>
        input[type="text"], input[type="password"], input[type="email"], input[type="file"], input[type="date"] {
            width: 100%;
            padding: 15px 10px 15px;
            font-size: 14px;
            background: transparent;
            border: 2px solid #537b35;
            outline: none;
            margin-bottom: 26px;
            color: #fff;
            font-family: Arial, sans-serif;
        }

        input[type="checkbox"] {
            position: absolute;
            margin-left: 3px;
            margin-top: 5px
        }

        #gender {
            background-color: #666666;
            background: transparent;
            color: rgb(134, 142, 138);
            margin-bottom: 26px;
            border: 2px solid #537b35;
        }

        #google_login {
            background-color: #F74933;
            color: #FFFFFF;
            border-radius: 100px;
            display: inline-block;
            margin: 0px 17px 17px 17px;
            padding: 15px;
        }

        #facebook_login {
            background-color: #455CA8;
            color: #FFFFFF;
            border-radius: 100px;
            display: inline-block;
            margin: 0px 17px 17px 17px;
            padding: 15px;
        }

        a:hover {
            color: #537b35 !important;
        }

        b {
            color: #007bff
        }

    </style>
    <!--end css-->
</head>
<body class="animsition">
<!-- Header -->
<header class="header-v4">
    <!-- Header desktop -->
    <div class="container-menu-desktop">
        <!-- Topbar -->
    {{--<div class="top-bar">--}}
    {{--<div class="content-topbar flex-sb-m h-full container">--}}
    {{--<div class="left-top-bar">--}}
    {{--<a href="{{route('home')}}">Kênh PTO</a>--}}
    {{--</div>--}}

    {{--<div class="right-top-bar flex-w h-full">--}}
    {{--<a href="{{route('login')}}" class="flex-c-m p-lr-10 trans-04">--}}
    {{--Đăng nhập--}}
    {{--</a>--}}
    {{--<a href="{{route('register')}}" class="flex-c-m trans-04 p-lr-25">--}}
    {{--Đăng ký--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    @if(Auth::check())
        <!-- Topbar -->
            <div class="top-bar">
                <div class="content-topbar flex-sb-m h-full m-l-93">
                    <div class="left-top-bar  flex-w h-full">

                        @if(in_array('read-dashboard',$menu))
                           <a href="{{route('dashboard.index')}}" class="flex-c-m trans-04 p-lr-25" target="_blank">
                                @if(Auth::user()->idRole ==2)
                                    Kênh PTO
                                @elseif(Auth::user()->idRole ==1)
                                    Kênh Quản trị
                                @endif
                            </a>
                        @else
                            <a href="{{route('pto.create')}}" class="flex-c-m trans-04 p-lr-25">
                                Đăng ký thành PTO
                            </a>
                        @endif

                    </div>

                    <div class="right-top-bar flex-w h-full dropdown-hover">
                        <a href="#" class="flex-c-m trans-04 p-lr-25" style="margin-right: 100px">
                            Tài khoản của tôi
                        </a>
                        <div class="dropdown_infor">
                            <!--<div  class="dropdown_infor">-->
                            <div class="triangle-up"></div>
                            <div>
                                <ul>
                                    <li><a href="{{route('courses.myCourse')}}"> Khóa học </a></li>
                                    <li><a href="{{route('users.myAccount',Auth::id())}}"> Tài khoản của tôi</a></li>
                                    <li>
                                        <a href="">
                                            <form method="POST" action="{{route('logout')}}">
                                                @csrf
                                                <button type="submit">Đăng xuất</button>
                                            </form>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    @else
        <!-- Topbar -->
            <div class="top-bar">
                <div class="content-topbar flex-sb-m h-full container ">
                    <div class="left-top-bar  flex-w h-full">
                        {{--<a href="" class="flex-c-m trans-04 p-lr-25" target="_blank">--}}
                        {{--Kênh PTO--}}
                        {{--</a>--}}
                    </div>
                    <div class="right-top-bar flex-w h-full ">
                        <a href="{{route('login')}}" class="flex-c-m p-lr-10 trans-04">
                            Đăng nhập
                        </a>
                        <a href="{{route('register')}}" class="flex-c-m trans-04 p-lr-25">
                            Đăng ký
                        </a>
                    </div>

                </div>
            </div>

        @endif

        {{--<div class="wrap-menu-desktop how-shadow1">--}}
        {{--<nav class="limiter-menu-desktop container">--}}

        {{--<!-- Logo desktop -->--}}
        {{--<a href="{{route('homepage')}}" class="logo">--}}
        {{--<img src="{{ URL::to('images/homepage/icons/logo-01.png')}}" alt="IMG-LOGO">--}}
        {{--</a>--}}

        {{--<!-- Menu desktop -->--}}
        {{--<div class="menu-desktop">--}}
        {{--<ul class="main-menu">--}}
        {{--<li>--}}
        {{--<a href="{{route('homepage')}}">Trang chủ</a>--}}

        {{--</li>--}}

        {{--<li>--}}
        {{--<a href="pto.html">PTO</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a href="news.html">Tin tức</a>--}}
        {{--</li>--}}

        {{--<li>--}}
        {{--<a href="about.html">Về chúng tôi</a>--}}
        {{--</li>--}}

        {{--<li class="active-menu">--}}
        {{--<a href="contact.html">Liên hệ</a>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}

        {{--<!-- Icon header -->--}}
        {{--<div class="wrap-icon-header flex-w flex-r-m">--}}
        {{--<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">--}}
        {{--<i class="zmdi zmdi-search"></i>--}}
        {{--</div>--}}

        {{--<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"--}}
        {{--data-notify="2">--}}
        {{--<a href="shoping-cart.html">--}}
        {{--<i class="zmdi zmdi-shopping-cart"></i>--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--<!--wishlist -->--}}
        {{--<!-- <a href="#" class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="0">--}}
        {{--<i class="zmdi zmdi-favorite-outline"></i>--}}
        {{--</a> -->--}}
        {{--</div>--}}
        {{--</nav>--}}
        {{--</div>--}}
        <div class="wrap-menu-desktop how-shadow1">
            <nav class="limiter-menu-desktop container">
                <!-- Logo desktop -->
                <a href="{{route('homepage')}}" class="logo">
                    <img src="/images/update/logo.png" alt="IMG-LOGO">
                </a>
                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li>
                            <a href="{{route('homepage')}}">Trang chủ</a>

                        </li>

                        <li>
                            <a href="{{route('showall')}}">PTO</a>
                        </li>

                        <li>
                            <a href="{{route('courses.showAll')}}">Khóa học</a>
                        </li>
                        <li>
                            <a href="{{route('new.index')}}">Tin tức</a>
                        </li>

                        <li>
                            <a href="{{route('about')}}">Về chúng tôi</a>
                        </li>

                        <li>
                            <a href="{{route('contact.index')}}">Liên hệ</a>
                        </li>
                    </ul>
                </div>

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m">
                    {{--<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">--}}
                        {{--<i class="zmdi zmdi-search"></i>--}}
                    {{--</div>--}}

                    @if(Auth::check())
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                             data-notify="{{Cart::count()}}">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    @else
                        <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                             data-notify="0">
                            <i class="zmdi zmdi-shopping-cart"></i>
                        </div>
                    @endif

                </div>
            </nav>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="index.html"><img src="{{ URL::to('images/homepage/icons/logo-01.png')}}" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m m-r-15">
            {{--<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">--}}
                {{--<i class="zmdi zmdi-search"></i>--}}
            {{--</div>--}}

            @if(Auth::check())
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                     data-notify="{{Cart::count()}}">
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            @else
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart"
                     data-notify="0">
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            @endif
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>
    </div>


    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="topbar-mobile">
            <li>
                <div class="left-top-bar">
                    Website PTO xin chào các bạn.
                </div>
            </li>

            <li>
                <div class="right-top-bar flex-w h-full">

                    <a href="#" class="flex-c-m p-lr-10 trans-04">
                        Tài khoản của tôi
                    </a>
                </div>
            </li>
        </ul>

        <ul class="main-menu-m">
            <li>
                <a href="index.html">Home</a>

                <span class="arrow-main-menu-m">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </span>
            </li>

            <li>
                <a href="pto.html">PTO</a>
            </li>


            <li>
                <a href="news.html">Tin tức</a>
            </li>

            <li>
                <a href="about.html">Về chúng tôi</a>
            </li>

            <li>
                <a href="contact.html">Liên hệ</a>
            </li>
        </ul>
    </div>

    {{--<!-- Modal Search -->--}}
    {{--<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">--}}
        {{--<div class="container-search-header">--}}
            {{--<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">--}}
                {{--<img src="/images/homepage/icons/icon-close2.png" alt="CLOSE">--}}
            {{--</button>--}}

            {{--<form class="wrap-search-header flex-w p-l-15" action="{{route('sreach')}}" method="get">--}}
                {{--@csrf--}}
                {{--@method("get")--}}
                {{--<button class="flex-c-m trans-04" type="submit">--}}
                    {{--<i class="zmdi zmdi-search"></i>--}}
                {{--</button>--}}
                {{--<input class="plh3" type="text" name="search" placeholder="Tìm kiếm...">--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</div>--}}

</header>
<!--end header-->

<!-- Gio hang -->

<div class="wrap-header-cart js-panel-cart {{ app('request')->input('open')=='true'?'show-header-cart':'' }}" id="cart">
    <div class="s-full js-hide-cart"></div>

    <div class="header-cart flex-col-l p-l-65 p-r-25">
        <div class="header-cart-title flex-w flex-sb-m p-b-8">
           <span class="mtext-103 cl2">
              Giỏ hàng của bạn

           </span>

            <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                <i class="zmdi zmdi-close"></i>
            </div>
        </div>
        <div class="header-cart-content flex-w js-pscroll ps" style="position: relative; overflow: hidden;">
            @foreach(Cart::content() as $row)
                <ul class="header-cart-wrapitem w-full">
                    <li class="header-cart-item flex-w flex-t m-b-12">
                        <div class="header-cart-item-img">
                            <img src="{{$row->options->has('image')?$row->options->image:''}}" alt="IMG">
                        </div>

                        <div class="header-cart-item-txt p-t-8">
                            <a href="{{route('courses.showCourse',$row->options->has('slug')?$row->options->slug:'')}}" class="header-cart-item-name m-b-18 hov-cl1 trans-04">
                                {{$row->name}}
                            </a>

                            <span class="header-cart-item-info">
								<span><b>{{ number_format($row->price) }} ₫</b></span>
                                <span style="margin-left: 70px;"><a href="#"><label class="btn-delete"
                                                                                    data-id="{{$row->rowId}}">Xóa</label></a></span>


							</span>

                        </div>

                    </li>

                </ul>
            @endforeach
            <div class="w-full">
                <div class="header-cart-total w-full p-tb-40">
                    Tổng cộng: {{Cart::subtotal()}} ₫
                </div>

                <div class="header-cart-buttons flex-w w-full">
                    <a href="{{route('order.showcard')}}"
                       class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                        Xem giỏ hàng
                    </a>


                </div>
            </div>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
            </div>
        </div>
    </div>
</div>

<!-- Gio hang -->


<main>
    @yield('content')
</main>

<!--footer-->
<!-- Footer -->
<footer class="bg3 p-t-75 p-b-32" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Tên công ty
                </h4>

                <ul>
                    <li class="p-b-10">
                        <p class="stext-107 cl7 hov-cl1 trans-04">
                            PTO - Personal Trainer Online
                        </p>
                        <p class="stext-107 cl7 hov-cl1 trans-04">
                            KM29 Đại Lộ Thăng Long, Thạch Hòa, Thạch Thất, Khu Công Nghê cao Hòa Lạc, Hà Nội
                        </p>

                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Huấn luyện viên cá nhân
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            Huấn luyện viên nữ
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#" class="stext-107 cl7 hov-cl1 trans-04">
                            Huấn luyện viên nam
                        </a>
                    </li>


                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Liên lạc
                </h4>

                <p class="stext-107 cl7 size-201">
                    Bạn có thắc mắc?
                </p>
                <p class="stext-107 cl7 size-201">
                    Hãy đến liên lạc:
                    0978813796
                </p>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    liên kết ngoài
                </h4>

                <div>
                    <a href="https://www.facebook.com/cskh.ptovn/" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="p-t-40">
            <div class="flex-c-m flex-w p-b-18">
                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-01.png" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-02.png" alt="ICON-PAY">
                </a>

                <a href="#" class="m-all-1">
                    <img src="/images/homepage/icons/icon-pay-03.png" alt="ICON-PAY">
                </a>

            </div>

            <p class="stext-107 cl6 txt-center">
                <a
                        href="https://www.facebook.com/cskh.ptovn/" target="_blank">
                    PTO - Personal Trainer Online
                </a>
            </p>
            <p class="stext-107 cl6 txt-center" style="color: white;">
                Website được phát triển bởi ĐỖ ĐỨC & QUANG MINH - học viên DEVMASTER
            </p>
        </div>
    </div>
</footer>


<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <i class="zmdi zmdi-chevron-up"></i>
            </span>
</div>

<!--===============================================================================================-->
<script src="{{ URL::to('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{ URL::to('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('vendor/select2/select2.min.js')}}"></script>
<script>
    $(".js-select2").each(function () {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
<!--===============================================================================================-->
<script src="{{ URL::to('vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script>
    $('.js-pscroll').each(function () {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });

        $(window).on('resize', function () {
            ps.update();
        })
    });
</script>
<!--===============================================================================================-->
<script src="{{ URL::to('https://maps.googleapis.com/maps/api/js?key=AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes')}}"></script>
<script src="{{ URL::to('js/map-custom.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::to('js/main.js')}}"></script>

</body>
</html>
<!--end footer-->