@extends('master_layout.admin.index')
@section('titles')
    Thêm thể loại tin tức
@endsection
@section('content')

    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thể Loại
            <small>thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('category.create')}}</li>
        </ol>
    </section>
    <section class="content" >
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>+</strong> Thêm thể loại</h3>
                    </div>
                    <form action="{{route('category.store')}}" enctype="multipart/form-data" method="POST" role="form" >
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tên thể loại</label>
                                <input type="text" class="form-control{{ $errors->has('categoryName') ? ' is-invalid' : '' }}" id="" placeholder="Nhập tên thể loại" name="categoryName">
                                @if ($errors->has('categoryName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('categoryName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
