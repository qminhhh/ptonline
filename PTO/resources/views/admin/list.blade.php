@extends('master_layout.admin.index')
@section('titles')
    Danh sách người dùng
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Danh sách người dùng
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('userslist')}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="panel-body">
                <div>
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{session('thongbao')}}
                        </div>
                    @endif
                </div>
                <table id="usertable" class="table table-bordered table-hover dataTable" style="width:100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ảnh</th>
                        <th>Tên tài khoản</th>
                        <th>Xác minh</th>
                        <th>Nhóm</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>

        $(document).ready(function () {

            $(document).on('click', '.btn-danger', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this).parent('form');
                swal({
                    title: "Bạn muốn xóa người dùng?",
                    text: "Bạn sẽ không thể khôi phục khi đã xóa!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No! don't delete",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                }).then(function (result) {
                        if (result) {
                            form.submit();
                        } else {
                            swal("Bản ghi vẫn an toàn!");
                        }

                    },
                );
            });
            $('#usertable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('users.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: function (data) {
                            if (data.image != null) {
                                return "<img width=\"100px\" src='" + data.image + "'>";
                            } else {
                                return "";
                            }
                        }, name: 'image'
                    },
                    {data: 'userName', name: 'userName'},
                    {
                        data: function (data) {
                            if (data.verified != null) {
                                if (data.verified == 1) {
                                    return '<span class="badge bg-blue">Đã kích hoạt</span>';
                                }
                                else if (data.verified == 0) {
                                    return '<span class="badge bg-red">Chưa kích hoạt</span>';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {
                        data: function (data) {
                            if (data.idRole != null) {
                                if (data.idRole == 1) {
                                    return "Admin";
                                }
                                else if (data.idRole == 2) {
                                    return "PTO";
                                }else{
                                    return "Học viên";
                                }
                            } else {
                                return "";
                            }
                        }, name: 'idRole'
                    },
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>
@endpush


