@extends('master_layout.admin.index')
@section('titles')
   Chi tiết nhóm
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chi tiết nhóm
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('admin.edit',$role)}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary"
                <!-- /.box-header -->
                <!-- form start -->
                <div class="col-lg-12">
                    <h1 class="page-header">Nhóm {{$role->roleName}}
                        <small>Chỉnh sửa</small>
                    </h1>
                </div>
                <form action="{{route('admin.update',$role->id)}}" enctype="multipart/form-data" method="POST"
                      role="form">
                    @csrf
                    @method("put")
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Tên nhóm </label>
                            <input id="roleName" type="text"
                                   class="form-control{{ $errors->has('roleName') ? ' is-invalid' : '' }}"
                                   name="roleName" value="{{$role->	roleName}}">
                            @if ($errors->has('roleName'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('roleName') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input id="slug" type="text"
                                   class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                   name="slug" value="{{$role->	slug}}">
                            @if ($errors->has('slug'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Tên Permission </label>
                        </div>
                        <div class="select2-selection__rendered">
                            <select class="js-example-basic-multiple {{$errors->has('listPermission') ? ' is-invalid' : '' }}"
                                    name="listPermission[]" multiple="multiple" style="width: 100%">
                                @foreach($permissions as $p)
                                    @if(in_array($p->id,$role_list_id)))
                                        <option value="{{$p->id}}" selected>{{$p->name}}</option>
                                    @else
                                        <option value="{{$p->id}}">{{$p->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('listPermission'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('listPermission') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Chỉnh sửa</button>

                    </div>
                </form>
            </div>
            <!-- /.box -->

        </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection
