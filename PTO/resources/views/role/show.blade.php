@extends('master_layout.admin.index')
@section('titles')
    Thông tin nhóm
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Thông tin nhóm
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('admin.show',$role)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="form-group row">
                    <label for="roleName" class="col-md-4 col-form-label text-md-right">{{ __('Tên nhóm') }}</label>

                    <div class="col-md-6">
                        <label>{{$role->roleName}}</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="slug" class="col-md-4 col-form-label text-md-right">{{ __('Slug') }}</label>

                    <div class="col-md-6">
                        <label>{{$role->slug}}</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="permissionName" class="col-md-4 col-form-label text-md-right">{{ __('Tên Permission') }}</label>

                    <div class="col-md-6">
                        @foreach($role->RolePermissions as $r)
                            <label class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                                {{$r->Permission->name}}
                            </label><br/>
                        @endforeach

                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4 pull-right">
                        <a href="{{route('admin.edit',$role->id)}}" class="btn btn-default btn-flat">Chỉnh sửa nhóm</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
