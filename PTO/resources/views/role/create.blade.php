@extends('master_layout.admin.index')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Nhóm
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('admin.create')}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <form method="POST" enctype="multipart/form-data" action="{{route('admin.store')}}">
                        @method('POST')
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tên nhóm </label>
                                <input id="roleName" type="text"
                                       class="form-control{{ $errors->has('roleName') ? ' is-invalid' : '' }}"
                                       name="roleName" value="{{old('roleName')}}">
                                @if ($errors->has('roleName'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('roleName') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Slug</label>
                                <input id="slug" type="text"
                                       class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                       name="slug" value="{{old('slug')}}">
                                @if ($errors->has('slug'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Tên Permission </label>
                            </div>
                            <div class="select2-selection__rendered">
                                <select class="js-example-basic-multiple {{ $errors->has('listPermission') ? ' is-invalid' : '' }}"
                                        name="listPermission[]" multiple="multiple" style="width: 100%">
                                    @foreach($permissions as $p)
                                        <option value="{{ $p->id }}">{{$p->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('listPermission'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('listPermission') }}</strong>
                                </span>
                                @endif
                            </div>


                        </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Thêm nhóm</button>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>--}}
    <script>
        // $('#listPermission').select2({
        //     placeholder: "Choose permissions...",
        //     minimumInputLength: 2,
        //     ajax: {
        //         url: '/permissions/find',
        //         dataType: 'json',
        //         data: function (params) {
        //             return {
        //                 q: $.trim(params.term)
        //             };
        //         },
        //         processResults: function (data) {
        //             return {
        //                 results: data
        //             };
        //         },
        //         cache: true
        //     }
        // });
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>

@endsection