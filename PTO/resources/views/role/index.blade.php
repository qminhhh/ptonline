@extends('master_layout.admin.index')
@section('titles')
   Danh sách nhóm
@endsection
@section('content')
    <!-- Main content -->
    <section class="content-header">
        <h1>
            Phân quyền nhóm
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('admin.index')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            @if(session('thongbao'))
                <div class="alert alert-success">
                    {{session('thongbao')}}
                </div>
            @endif

            <div class="box-body no-padding">
                <a class="btn btn-xs btn-info " href="{{route('admin.create')}}" style="margin: 5px">Thêm mới</a>
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Tên nhóm</th>
                        <th>Slug</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>

                    @foreach($role as $r)
                        <tr>
                            <td>{{$r->id}}</td>
                            <td>{{$r->roleName}}</td>
                            <td>{{$r->slug}}</td>
                            <td>
                                {{--<form action="/role/{{$r->id}}" method="POST">--}}
                                <form action="{{route('admin.destroy',$r->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <a class="btn  btn-info btn-xs" href="{{ route('admin.show',$r->id) }}">Chi tiết</a>
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.edit',$r->id) }}">Sửa</a>
                                    {{--<button id class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i>Xóa</button>--}}
                                    <button class="btn btn-danger btn-delete btn-xs" type="submit">Xóa</button>
                                </form>
                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>

    </section>
    <script>
        $(document).ready(function(){
            // $(document).on('click', '.btn-danger', function (e) {
            //     e.stopPropagation();
            //     e.preventDefault();
            //     var form = $(this).parent('form');
            //     swal({
            //         title: "Chú ý: bạn muốn xóa?",
            //         text: "Bạn sẽ không thể khôi phục khi đã xóa!",
            //         type: "warning",
            //         showCancelButton: true,
            //         confirmButtonColor: '#DD6B55',
            //         confirmButtonText: "Yes, delete it!",
            //         cancelButtonText: "No! don't delete",
            //         closeOnConfirm: true,
            //         showLoaderOnConfirm: true,
            //         buttons: true,
            //         dangerMode: true,
            //     }).then(function (result) {
            //             if (result) {
            //                 form.submit();
            //             } else {
            //                 swal("Bản ghi vẫn an toàn!");
            //             }
            //
            //         },
            //     );
            // });
            $('.btn-delete').click(function () {

                var form=$(this).parent('form');
                swal({
                    title: "Bạn có chắc chắn xóa nhóm {{$r->roleName}}?",
                    text: "Một khi xóa sẽ không lấy lại được dữ liệu !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        });
    </script>
@endsection
{{--<div class="box-footer">--}}
    {{--<form action="{{route('admin.destroy',$role->id)}}" method="post">--}}
        {{--@csrf--}}
        {{--@method("DELETE")--}}
        {{--<button type="button" class="btn btn-primary btn-del" > Xóa </button>--}}
    {{--</form>--}}
{{--</div>--}}

