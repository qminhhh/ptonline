@extends('master_layout.admin.index')
@section('titles')
    @if(\Illuminate\Support\Facades\Auth::user()->idRole == 2)
        Kênh PTO
    @endif
    @if(\Illuminate\Support\Facades\Auth::user()->idRole == 1)
        Kênh Quản Trị
    @endif
@endsection
@section('content')
    <section class="content-header">

        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('home') }}</li>
        </ol>

    </section>
<section class="content">
    <!-- Date and time range -->
    <div class="form-group">
        <label>Thống kê theo thời gian:</label>

        <div class="input-group">
            <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Thời gian
                    </span>
                <i class="fa fa-caret-down"></i>
            </button>
        </div>
    </div>


    <!-- /.form group -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><span id="totalOrder"> 0</span><sup style="font-size: 20px"></sup></h3>

                    <p>Đơn đặt hàng</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                @if(Auth::user()->idRole == 2)
                    <a href="{{route('order.listCourse')}}" class="small-box-footer">Hiển thị thêm <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><span id="totalPrice">0đ</span></h3>

                    <p>Doanh thu </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                @if(Auth::user()->idRole == 2)
                <a href="{{route('order.listCourse')}}" class="small-box-footer">Hiển thị thêm <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><span id="totalUser"> 0</span></h3>

                    <p>Học viên </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                @if(Auth::user()->idRole == 2)
                <a href="{{route('users.listTrainee')}}" class="small-box-footer">Hiển thị thêm <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><span id="totalCourse"> 0</span></h3>

                    <p>Khóa học</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                @if(Auth::user()->idRole == 2)
                <a href="{{route('courses.index')}}" class="small-box-footer">Hiển thị thêm <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    @if(Auth::user()->idRole == 1)
    <div class="row">
        <div class="chart">

            <canvas id="test123" style="height:250px;width: 100%"></canvas>
        </div>
    </div>
    @endif
    <!-- /.row (main row) -->




@endsection

@push('script_footer')

        <script>
            //Date range as a button
            $(document).ready(function () {
                function cb(start, end) {
                    $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                    $.ajax({
                        url: '{!! route('dashboard.data') !!}',
                        type: 'GET',
                        data: {
                            'start':start.format('DD/MM/YYYY'),
                            'end':end.format('DD/MM/YYYY'),

                        },
                    })
                        .done(function(rs) {
                            console.log(rs);
                            console.log("success");
                            $('#totalOrder').text(rs.countOrder);
                            $('#totalUser').text(rs.countUser);
                            $('#totalPrice').text(rs.purchases);
                            $('#totalCourse').text(rs.countCourse);


                            var ctx = document.getElementById("test123");
                            var test = new Chart(ctx, {
                                type: 'line',
                                options: {
                                    elements: {
                                        line: {
                                            tension: 0, // disables bezier curves
                                        }
                                    },
                                    yAxisID:'Ngày'
                                },
                                data: {
                                    labels:rs.label ,
                                    datasets: [{
                                        label: "Người đăng ký",
                                        backgroundColor: "rgba(231, 76, 60,0.31)",
                                        borderColor: "rgba(231, 76, 60,0.7)",
                                        pointBorderColor: "rgba(231, 76, 60,0.7)",
                                        pointBackgroundColor: "#fff",
                                        pointHoverBackgroundColor: "#fff",
                                        pointHoverBorderColor: "rgba(231, 76, 60,1)",
                                        pointBorderWidth: 9,
                                        data:rs.data
                                    }]
                                },
                            });

                        })
                        .fail(function() {
                            console.log("error");
                        })
                        .always(function() {
                            console.log("complete");
                        });
                }
                $('#daterange-btn').daterangepicker(
                    {
                        ranges   : {
                            'Hôm nay'       : [moment(), moment()],
                            'Hôm qua'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            '7 ngày trước' : [moment().subtract(6, 'days'), moment()],
                            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                            'Tháng này'  : [moment().startOf('month'), moment().endOf('month')],
                            'Tháng trước'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        startDate: moment().subtract(29, 'days'),
                        endDate  : moment()
                    },
                    cb
                )
                var start=moment().startOf('month');
                var end=moment().endOf('month')

                cb(start,end);



            });
        </script>
@endpush