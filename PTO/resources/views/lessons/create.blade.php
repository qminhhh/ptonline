@extends('master_layout.admin.index')
@section('titles')
    Thêm mới khóa học
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>+</strong> Thêm bài học</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{route('lessons.store')}}" enctype="multipart/form-data" method="POST" role="form">
                        @csrf
                        <div class="box-body">

                            <input type="hidden" class="form-control" id="exampleInputEmail1" name="idCourse"
                                   value="{{$id}}">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên bài học</label>
                                <input type="text"
                                       class="form-control {{ $errors->has('lessonName') ? ' is-invalid' : '' }} "
                                       id="exampleInputEmail1" placeholder="Nhập tên bài học" name="lessonName"
                                       value="{{ old('lessonName')}}">
                                @if ($errors->has('lessonName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('lessonName') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Nội dung</label>
                                <textarea name="contentL" id="input"
                                          class="form-control ckeditor {{ $errors->has('contentL') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('contentL')}}</textarea>
                                @if ($errors->has('contentL'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('contentL') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Link video : </label>
                                <input type="text"
                                       class="form-control {{ $errors->has('video') ? ' is-invalid' : '' }} "
                                       id="exampleInputEmail1" placeholder="Nhập link video bài học" name="video"
                                       value="{{ old('video')}}">

                                @if(session('message'))
                                    <div class="text-danger">
                                        {{session('message')}}
                                    </div>
                                @endif

                            </div>


                            <div class="form-group">
                                <label for="">Tài liệu học </label>
                                <textarea name="linkVideo" id="input"
                                          class="form-control ckeditor {{ $errors->has('linkVideo') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('linkVideo')}}</textarea>
                                @if ($errors->has('linkVideo'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('linkVideo') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Thực đơn</label>
                                <textarea name="menu" id="input"
                                          class="form-control ckeditor{{ $errors->has('menu') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('menu') }}</textarea>
                                @if ($errors->has('menu'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('menu') }}</strong>
                                </span>
                                @endif
                            </div>

                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Tạo</button>
                            </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@push('script_footer')
    <script>
        var button1 = document.getElementById('add-video');

        button1.onclick = function () {
            selectFileWithCKFinder('ckfinder-input-1');
        };

        function selectFileWithCKFinder(elementId) {
            CKFinder.popup({
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        var output = document.getElementById(elementId);
                        output.value = file.getUrl();
                    });

                    finder.on('file:choose:resizedImage', function (evt) {
                        var output = document.getElementById(elementId);
                        output.value = evt.data.resizedUrl;
                    });
                }
            });
        }
    </script>
@endpush
