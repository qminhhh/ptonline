@extends('master_layout.admin.index')
@section('titles')
    Chỉnh sửa bài học
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Bài học
            <small>Chỉnh sửa</small>
        </h1>
        <ol class="breadcrumb">
            {{--<li>{{ Breadcrumbs::render('lessons.edit')}}</li>--}}
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="col-lg-12">
                        <h1 class="page-header">Khóa học {{$lesson->lessonName}}
                        </h1>
                    </div>
                    <!-- form start -->
                    <form action="{{route('lessons.update',$lesson->id)}}" enctype="multipart/form-data"
                          method="POST" role="form">
                        @csrf
                        @method('PUT')
                        <input type="hidden" class="form-control " id="exampleInputEmail1" name="idCourse"
                               value="{{$id}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên bài học</label>
                                <input type="text"
                                       class="form-control {{ $errors->has('lessonName') ? ' is-invalid' : '' }}"
                                       id="exampleInputEmail1" value="{{$lesson->lessonName}}"
                                       name="lessonName">
                                @if ($errors->has('lessonName'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('lessonName') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="">Nội dung</label>
                                <textarea name="contentL" id="input"
                                          class="form-control ckeditor {{ $errors->has('contentL') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">
                                    {{$lesson->content}}
                                </textarea>
                                @if ($errors->has('contentL'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('contentL') }}</strong>
                                </span>
                                @endif
                            </div>

                            @if($lesson->video != null)

                                <div class="box-body">
                                    <label for="exampleInputEmail1">Video</label>
                                    <div class="form-group">
                                                    <span>

                                                        <iframe width="560" height="315"
                                                                src="https://www.youtube.com/embed/{{$lesson->video}}"
                                                                frameborder="0"
                                                                allow="autoplay; encrypted-media"
                                                                allowfullscreen></iframe>
                                </span>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">

                                        <input type="text"
                                               class="form-control"
                                               id="exampleInputEmail1"
                                               value="https://www.youtube.com/watch?v={{$lesson->video}}"
                                               name="video">
                                        @if(session('message'))
                                            <div class="text-danger">
                                                {{session('message')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Link video : </label>
                                    <input type="text"
                                           class="form-control"
                                           id="exampleInputEmail1" placeholder="Nhập link video bài học"
                                           name="video"
                                           value="{{ old('video')}}">
                                </div>


                            @endif


                            <div class="form-group">
                                <label for="">Tài liệu học : </label>
                                <textarea name="linkVideo" id="input"
                                          class="form-control ckeditor {{ $errors->has('linkVideo') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">
                                    {{$lesson->linkVideo}}
                                </textarea>
                                @if ($errors->has('linkVideo'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('linkVideo') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Thực đơn</label>
                                <textarea name="menu" id="input"
                                          class="form-control ckeditor {{ $errors->has('menu') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">
                                    {{$lesson->menu}}
                                </textarea>
                                @if ($errors->has('menu'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('menu') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập Nhật</button>
                        </div>
                    </form>
                    <div class="box-footer">
                        <form action="{{route('lessons.destroy',$lesson->id).'?idCourse='.$lesson->idCourse}}}}"
                              method="post">
                            @csrf
                            @method("DELETE")
                            <button type="button" class="btn btn-primary btn-del"> Xóa</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>

    <script>
        $(document).ready(function () {
            $('.btn-del').click(function () {

                var form = $(this).parent('form');
                swal({
                    title: "Bạn có chắc chắn xóa  {{$lesson->lessonName}}?",
                    text: "Một khi xóa sẽ không lấy lại được dữ liệu !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
            $('.btn-box-tool').click(function () {
                var form = $(this).parent('form');
                swal({
                    title: "Bạn muốn xóa bình luận này?",
                    text: "Một khi xóa sẽ không lấy lại được dữ liệu !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        });
    </script>
    <!-- /.content -->
@endsection
