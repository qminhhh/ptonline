@extends('master_layout.admin.index')
@section('titles')
    Ý kiến học viên
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Ý kiến
            <small> - {{$lesson->lessonName}}</small>
        </h1>
        <ol class="breadcrumb">
            {{--<li>{{ Breadcrumbs::render('lessons.edit')}}</li>--}}
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-body">
                 @if($comment != null )
                     <!-- Post -->
                         @foreach($comment as $key=>$cmt)
                             <div class="post clearfix">
                                 <div class="user-block">
                                     <img class="img-circle img-bordered-sm" src="{{$cmt->user->image}}"
                                          alt="User Image">
                                     <span class="username">
                          <a href="#">{{$cmt->user->fullName}}</a>
                                         @if(Auth::id() == $cmt->idUser)
                                             <form class="form-group"
                                                   action="{{route('comment.destroy',$cmt->id)}}"
                                                   method="POST">
                                                                @csrf
                                                 @method("DELETE")
                                                 <button type="button" class="btn-box-tool pull-right" style="border: 0px;
    font-size: 15px;"><i class="fa fa-times"></i></button>
                                                            </form>
                                         @endif
                        </span>
                                     <span class="description">Gửi cho bạn lúc - {{date('d-m-Y H:i', strtotime($cmt->created_at))}}</span>
                                 </div>
                                 <!-- /.user-block -->
                                 <p>
                                 <h4>{!! $cmt->content !!}</h4>
                                 </p>

                                 <br>
                                 @foreach($cmt->replies as $reply)
                                     <div class="post clearfix" style="margin-left: 50px;">
                                         <div class="user-block">
                                             <img class="img-circle img-bordered-sm"
                                                  src="{{$reply->user->image}}" alt="User Image">
                                             <span class="username">
                          <a href="#">{{$reply->user->fullName}}</a>
                                                 @if(Auth::id() == $reply->idUser)
                                                     <form class="form-group"
                                                           action="{{route('comment.destroy',$reply->id)}}"
                                                           method="POST">
                                                                @csrf
                                                         @method("DELETE")
                                                         <button type="button" style="border: 0px;
    font-size: 15px;" class=" btn-box-tool pull-right"><i class="fa fa-times"></i></button>
                                                            </form>
                                                 @endif
                                                 {{--<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>--}}
                        </span>
                                             <span class="description"> @if(Auth::id() == $reply->idUser)
                                                     Bạn trả lời lúc -
                                                 @else Gửi cho bạn lúc - @endif
                                                 {{date('d-m-Y H:i', strtotime($reply->created_at))}}</span>
                                         </div>
                                         <!-- /.user-block -->
                                         <p>
                                             {!! $reply->content !!}
                                         </p>
                                     </div>
                                 @endforeach
                                 <form class="form-horizontal" action="{{route('comment.saveCmt',$lesson->id)}}"
                                       enctype="multipart/form-data" method="POST" role="form">
                                     @csrf
                                     <div class="form-group margin-bottom-none" style="margin-left: 35px;">
                                         <div class="col-sm-10">
                                             <input type="hidden" name="parent_id" value="{{$cmt->id}}">
                                             <input type="hidden" name="comment_id" value="{{$cmt->id}}">
                                             <input class="form-control input-sm" placeholder="Trả lời"
                                                    name="comment">
                                         </div>
                                         <div class="col-sm-2">
                                             <button type="submit"
                                                     class="btn btn-success pull-right btn-block btn-sm">
                                                 Gửi
                                             </button>
                                         </div>
                                     </div>
                                 </form>
                             </div>
                         @endforeach
                     @else
                         <div class="post clearfix">
                             <div class="user-block">
                                 <span class="description">Chưa có bình luận của học viên</span>
                             </div>
                         </div>
                 @endif
                 <!-- /.post -->

                    </div>

                </div>
                <!-- /.tab-pane -->


                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->

        </div>
        <!-- /.row -->
    </section>

    <script>
        $(document).ready(function () {
            $('.btn-box-tool').click(function () {
                var form = $(this).parent('form');
                swal({
                    title: "Bạn muốn xóa bình luận này?",
                    text: "Một khi xóa sẽ không lấy lại được dữ liệu !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        });
    </script>
    <!-- /.content -->
@endsection
