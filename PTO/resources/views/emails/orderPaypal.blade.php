<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<!--mail hien thi-->
<div  class="card-outside">
    <div class="cart_status">
        <div style="margin-left: 15px">
            <label class="text-uppercase "><strong>Xác nhận mua khóa học thành công</strong></label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> Kính chào quý khách <strong>{{$user->lastName}} {{$user->firstName}}</strong>,</label>
            <br>
            <label for="">
                <strong>PTO.com </strong> vừa nhận được đơn hàng mã <strong>#{{$order->id}}</strong> của quý khách đặt ngày
                <strong>{{date('d-m-Y H:i',strtotime($order->created_at))}}</strong> với hình thức thanh toán là <strong>Thanh toán trực tuyến</strong>.
            </label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> <strong>Chi tiết đơn hàng</strong>
            </label>
            <br>
            <label class=" ">{{Cart::count()}} khóa học  : </label>
            <br> <br>
            @foreach(Cart::content() as $row)
                <div style="margin-left: 15px">
                    <label class="text-uppercase "> {{$row->name}} - {{ number_format($row->price) }} ₫</label>
                    <hr>
                </div>
            @endforeach
            <label class="text-uppercase"> Tổng tiền : <strong>{{Cart::subtotal()}} ₫ </strong></label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> <strong>“Mã kích hoạt khoá học :  ”</strong> <strong>{{$order->active_code}}</strong> , Vui lòng truy cập
                vào trang web sau để nhập mã kích hoạt : {{url(route('courses.myCourse'))}} !
                <br>
                Cảm ơn quý khách đã mua hàng !
            </label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class="">Trân trọng,
            </label>
            <br>
            <label for=""><strong>PTO team</strong></label>
        </div>
        @php Cart::destroy(); @endphp
    </div>
</div>
</body>
</html>