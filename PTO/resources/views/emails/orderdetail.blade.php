<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<!--mail hien thi-->
<div  class="card-outside">
    <div class="cart_status">
        <div style="margin-left: 15px">
            <label class="text-uppercase "><strong>Xác nhận mua khóa học thành công</strong></label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> Kính chào quý khách <strong>{{$order->name}}</strong>,</label>
            <br>
            <label for="">
                <strong>PTO.com </strong> vừa nhận được đơn hàng mã <strong>#{{$order->id}}</strong> của quý khách đặt ngày
                <strong>{{date('d-m-Y H:i',strtotime($order->created_at))}}</strong> với hình thức thanh toán là <strong>Thanh toán khi nhận hàng</strong>.
                Chúng tôi sẽ gửi thông báo đến quý khách qua một email khác ngay khi sản phẩm
                được giao cho đơn vị vận chuyển.
            </label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> <strong>Đơn hàng được giao đến</strong>
            </label>
            <br>
            <label for="">
                {{$order->name}} <br>
                {{$order->address}} {{$order->district}} {{$order->province}}<br>
                Phone:  {{$order->phone}}
            </label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> <strong>Chi tiết đơn hàng</strong>
            </label>
            <br>
            <label class=" ">{{Cart::count()}} khóa học  : </label>
            <br> <br>
            @foreach(Cart::content() as $row)
                <div style="margin-left: 15px">
                    <label class="text-uppercase "> {{$row->name}} - {{ number_format($row->price) }} ₫</label>
                    <hr>
                </div>
            @endforeach
            <label class="text-uppercase"> Tổng tiền : <strong>{{Cart::subtotal()}} ₫ </strong></label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class=""> <strong>“Mã kích hoạt khoá học”</strong> sẽ được gửi tới địa chỉ của bạn qua
                đường chuyển phát nhanh(COD), thu tiền trực tiếp với nhân
                giao vận. Thời gian vận chuyển có thể lên tới 3 ngày và muộn nhất là 1
                tuần. Quý khách vui lòng chuẩn bị sẵn số tiền mặt tương ứng để thuận tiện cho việc thanh toán.
            </label>
        </div>
        <br>
        <div style="margin-left: 15px">
            <label class="">Trân trọng,
            </label>
            <br>
            <label for=""><strong>PTO team</strong></label>
        </div>
        @php Cart::destroy(); @endphp
    </div>
</div>
</body>
</html>