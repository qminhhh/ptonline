@extends('master_layout.admin.index')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách đơn hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="#">Danh sách</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách đơn hàng</div>
                    <div class="panel-body">
                        <table id="order-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID Đơn hàng</th>
                                <th>Người mua</th>
                                <th>Tổng tiền (VNĐ)</th>
                                <th>Ngày mua</th>
                                <th>Trạng thái</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-danger', function (e) {
                e.stopPropagation();
                e.preventDefault();
                var form = $(this).parent('form');
                swal({
                    title: "Bạn có muốn xóa đơn hàng",
                    text: "Dữ liệu sẽ mất!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: "Xóa!",
                    cancelButtonText: "Hủy",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                    buttons: true,
                    dangerMode: true,
                }).then(function (result) {
                        if(result){
                            form.submit();
                        }else{
                            swal("Đơn hàng không xóa!");
                        }

                    },
                );
            });
            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('order-data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'userName', name: 'userName'},
                    {data: 'cat_name', name: 'cat_name'},
                    {
                        data: function (data) {
                            if (data.status != null) {
                                if (data.status == 0) {
                                    return  '<span class="text-danger glyphicon glyphicon-remove">';
                                }
                                else {
                                    return '<span class="text-success glyphicon glyphicon-ok"></span>';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
    </script>
@endpush
