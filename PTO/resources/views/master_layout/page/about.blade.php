@extends('welcome')
@section('title')
    Về chúng tôi
@endsection
@section('content')
    <!-- Title page -->
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('/images/image1/contact/10.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
            Về chúng tôi
        </h2>
    </section>


    <!-- Content page -->
    <section class="bg0 p-t-75 p-b-120">
        <div class="container">
            <div class="row p-b-148">
                <div class="col-md-7 col-lg-8">
                    <div class="p-t-7 p-r-85 p-r-15-lg p-r-0-md">
                        <h3 class="mtext-111 cl2 p-b-16">
                            Câu chuyện của chúng tôi
                        </h3>

                        <p class="stext-113 cl6 p-b-26">
                            PTO.com là một trang web dạy và học trực tuyến về thể hình cho những người muốn có một sức khỏe và thân hình rắn rỏi theo một lộ trình bài bản,
                            chi tiết kết hợp liên tục giữa luyện tập và kiểm tra một cách chuyên nghiệp.
                        </p>

                        <p class="stext-113 cl6 p-b-26">
                            PTO.com có nền tảng cho phép PTO - tức huấn luyện viên cá nhân có thể dễ dàng tìm kiếm học viên một cách nhanh chóng, quản lý học viên các bài giảng của mình một cách đơn giản ngay trên PTO.com.

                        </p>

                        <p class="stext-113 cl6 p-b-26">
                            Và học viên cũng dễ dàng tìm kiếm được PTO  nhiệt tình, uy tín và phù hợp với bản thân, giúp học viên không chỉ hoàn thành mục tiêu cá nhân học viên còn được cung cấp những kiến thức cơ bản trong tập luyện, cũng như chế độ dinh dưỡng, giúp các học viên tự trang bị kiến thức cho mình để có thể tự tập ở mọi thời điểm và bất cứ nơi đâu.

                        </p>
                    </div>
                </div>

                <div class="col-11 col-md-5 col-lg-4 m-lr-auto">
                    <div class="how-bor1 ">
                        <div class="hov-img0">
                            <img src="/images/image1/about/2.jpg" alt="IMG">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="order-md-2 col-md-7 col-lg-8 p-b-30">
                    <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                        <h3 class="mtext-111 cl2 p-b-16">
                            Nhiệm vụ của chúng tôi
                        </h3>

                        <p class="stext-113 cl6 p-b-26">
                            * Hỗ trợ việc quản lý học viên, theo dõi khóa học, quản lý tài chính chưa dễ dàng và đơn giản.

                        </p>
                        <p class="stext-113 cl6 p-b-26">
                            * Xây dựng môi trường chuyên nghiệp, thân thiện, sáng tạo, bình đẳng lẫn nhau giữa các PTO.

                        </p>
                        <p class="stext-113 cl6 p-b-26">
                            * Giúp PTO tiếp cận, truyền đạt kiến thức dễ dàng và nhanh chóng đến các học viên.


                        </p>
                        <p class="stext-113 cl6 p-b-26">
                            * PTO.com giúp học viên tìm kiếm PTO giàu kinh nghiệm, uy tín nhiệt huyết một cách dễ dàng, nhanh chóng và đảm bảo PTO luôn theo sát, hỗ trợ kịp thời trong suốt quá trình luyện tập giúp học viên được hướng dẫn  tập luyện đúng kĩ thuật, an toàn, hiệu quả.

                        </p>

                        <div class="bor16 p-l-29 p-b-9 m-t-22">
                            <p class="stext-114 cl6 p-r-40 p-b-11">
                                "Không thành công nào đến 1 cách dễ dàng, quan trọng là các bạn có đủ nghị lực để thay đổi bản thân mình hay không? Không có một vẻ đẹp và sức khỏe tự nhiên có được cả. Thành quả hôm nay chính là công sức các bạn đã bỏ ra những năm trước đó trong GYM"
                            </p>

                            <span class="stext-111 cl8">
								- PTO Team -

							</span>
                        </div>
                    </div>
                </div>

                <div class="order-md-1 col-11 col-md-5 col-lg-4 m-lr-auto p-b-30">
                    <div class="how-bor2">
                        <div class="hov-img0">
                            <img src="/images/image1/about/1.jpg" alt="IMG">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection