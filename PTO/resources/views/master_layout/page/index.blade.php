@extends('welcome')
@section('title')
    Trang chủ
@endsection
@section('content')
    <section class="section-slide">
        <div class="wrap-slick1">
            <div class="slick1">
                {{--<div class="item-slick1" style="background-image: url('/images/homepage/slide-01.jpg');">--}}
                {{--<div class="container h-full">--}}
                {{--<div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">--}}
                {{--<div class="layer-slick1 animated visible-false" data-appear="fadeInDown"--}}
                {{--data-delay="0">--}}
                {{--<span class="ltext-101 cl2 respon2">--}}
                {{--Personal Trainer Online--}}
                {{--</span>--}}
                {{--</div>--}}
                {{--<div class="layer-slick1 animated visible-false" data-appear="fadeInUp"--}}
                {{--data-delay="800">--}}
                {{--<h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1" style="font-size: 300%;">--}}
                {{--HUẤN LUYỆN VIÊN CÁ NHÂN--}}
                {{--</h2>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="item-slick1" style="background-image: url('/images/update/banner/e.jpg');">
                    <div class="container h-full">
                        <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="rollIn"
                                 data-delay="0">
            <span class="ltext-101 cl2 respon2">
             Thân hình đẹp như mơ
         </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn"
                                 data-delay="800">
     <span class="ltext-201 cl2 p-t-19 p-b-43 respon1" style="font-size: 300%;">
       Hãy đến với chúng tôi
   </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item-slick1" style="background-image: url('/images/update/banner/c.jpg');">
                    <div class="container h-full">
                        <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown"
                                 data-delay="0">
            <span class="ltext-101 cl2 respon2">
                {{--Thân hình đẹp như mơ, dáng chuẩn đến từng cm--}}
                Thân hình săn chắc
            </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp"
                                 data-delay="800">
                                <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1" style="font-size: 300%;">
                                    Dáng Chuẩn Từng Cm
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="item-slick1" style="background-image: url('/images/update/banner/b.jpg');">
                    <div class="container h-full">
                        <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="rotateInDownLeft"
                                 data-delay="0">
            <span class="ltext-101 cl2 respon2">
                Để một sức khỏe tốt
            </span>
                            </div>

                            <div class="layer-slick1 animated visible-false" data-appear="rotateInUpRight"
                                 data-delay="800">
        <span class="ltext-201 cl2 p-t-19 p-b-43 respon1" style="font-size: 300%;">
            Hãy tập nào
        </span>
                            </div>
                        </div>
                    </div>
                </div>


                {{--    <div class="item-slick1" style="background-image: url('/images/update/banner/e.png');">
                       <div class="container h-full">
                           <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                               <div class="layer-slick1 animated visible-false" data-appear="fadeInDown"
                                    data-delay="0">
                                   <span class="ltext-101 cl2 respon2">
                                       Personal Trainer Online
                                   </span>
                               </div>
                               <div class="layer-slick1 animated visible-false" data-appear="fadeInUp"
                                    data-delay="800">
                                   <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1" style="font-size: 300%;">
                                       HUẤN LUYỆN VIÊN CÁ NHÂN
                                   </h2>
                               </div>
                           </div>
                       </div>
                   </div> --}}


            </div>
        </div>
        </div>
    </section>

    <!-- Product -->
    <section class="bg0 p-t-23 p-b-140">
        <div class="container">
            <div class="p-b-10">
                <h3 class="ltext-103 cl5">
                    Huấn luyện viên cá nhân
                </h3>
            </div>

            <div class="flex-w flex-sb-m p-b-52">
                <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                    <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
                        Tất cả PTO
                    </button>

                    <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".women">
                        HLV Nữ
                    </button>

                    <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".men">
                        HLV Nam
                    </button>

                </div>

                {{--<div class="flex-w flex-c-m m-tb-10">--}}
                {{--<div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">--}}
                {{--<i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>--}}
                {{--<i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>--}}
                {{--Tìm kiếm--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<!-- Search pto -->--}}
                {{--<div class="dis-none panel-search w-full p-t-10 p-b-15">--}}
                {{--<div class="bor8 dis-flex p-l-15">--}}
                {{--<button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">--}}
                {{--<i class="zmdi zmdi-search"></i>--}}
                {{--</button>--}}

                {{--<input class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" name="search-product"--}}
                {{--placeholder="Tìm kiếm">--}}
                {{--</div>--}}
                {{--</div>--}}

            </div>

            <div class="row isotope-grid">
                @foreach($user as $u)
                    @if($u->gender == 0)
                        <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-pic hov-img0">
                                    <img width="270px" height="360px" src="{{$u->image}}" alt="IMG-PRODUCT">

                                    <a href="{{route('ptodetail',$u->slug)}}"
                                       class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                        Chi tiết
                                    </a>
                                </div>

                                <div class="block2-txt flex-w flex-t p-t-14">
                                    <div class="block2-txt-child1 flex-col-l ">
                                        <a href="{{route('ptodetail',$u->slug)}}" class="stext-104 cl4 hov-cl1 trans-04">
                                            PT - <strong> {{$u->fullName}}</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item men">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-pic hov-img0">
                                    <img width="270px" height="360px" src="{{$u->image}}" alt="IMG-PRODUCT">

                                    <a href="{{route('ptodetail',$u->slug)}}"
                                       class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 ">
                                        Chi tiết
                                    </a>
                                </div>

                                <div class="block2-txt flex-w flex-t p-t-14">
                                    <div class="block2-txt-child1 flex-col-l ">
                                        <a href="{{route('ptodetail',$u->slug)}}" class="stext-104 cl4 hov-cl1 trans-04">
                                            PT - <strong> {{$u->fullName}}</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                @endforeach
            </div>

            <!-- Load more -->
            <div class="flex-c-m flex-w w-full p-t-45">
                <a href="{{route('showall')}}"
                   class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
                    Xem thêm
                </a>
            </div>
        </div>
    </section>
@endsection