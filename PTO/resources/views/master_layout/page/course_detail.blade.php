@extends('welcome')
@section('title')
    Chi tiết khóa học
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="/uikit/css/uikit.css">
    <script src="/uikit/js/uikit.js"></script>
@endsection
@section('content')
    <div id="container" class="detail-outside">
        <div class="detail_day">
            <div class="m-l-36" style="text-transform: none;">
                <a href="{{route('courses.showAll')}}">Khóa học</a> &#62; {{$course->courseName}}
            </div>
            <br>
            <div class="main-detail-day">
                <div class="row" style="border: 1px solid lightgrey;">
                    <div class="text-capitalize">
                        <h2>{{$course->courseName}}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">

                        <div class="row content-detail">
                            <div class="col-sm-11 left-detail left-detail-plus"
                                 style="background: #f8f9fa;;border: 1px solid #f8f9fa;">
                                <div>
                                    <img src="{{$course->image}}" style="width: 100%">
                                </div>
                            </div>
                            <div class="col-sm-11 left-detail left-detail-plus">
                                <div class="text-center">
                                    <br>
                                    <h2>Giới thiệu giáo viên</h2>
                                </div>
                                <br>
                                <div class="row content-left-detail text-left">
                                    <div class="img-pto" class="col-sm-3">
                                        <div>
                                            <img src="{{$course->user->image}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="font-weight-bold">
                                            <p> PTO:</p>
                                        </div>
                                        <div>
                                            <label>{{$course->user->fullName}}</label>
                                        </div>
                                        <div class="font-weight-bold">
                                            <p> Bằng cấp/Kinh nghiệm:</p>
                                        </div>
                                        <div>
                                            <label>{!!$course->user->certificate  !!}</label>
                                        </div>
                                        <div style="margin-top: 10px;">
                                            <a href="{{route('ptodetail',$course->user->slug)}}">Thông tin chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-sm-11 right-detail right-detail-plus">

                                <div class="text-center">
                                    <br>
                                    <h2>Thông tin khóa học</h2>
                                </div>
                                <br>
                                <div class="row"> <!--div nay chứa mô tả và thanh toán-->
                                    <div class="col-sm-12">
                                        <div class="font-weight-bold text-capitalize">
                                            <p class="glyphicon glyphicon-hand-right">Tổng quát</p>
                                        </div>
                                        <div>
                                            <p>{!! $course->introduce !!}</p>
                                        </div>
                                        <div class="font-weight-bold text-capitalize">
                                            <p>Yêu cầu của khóa học</p>
                                        </div>
                                        <div>
                                            <p>{!! $course->require !!}</p>
                                        </div>
                                        <div class="font-weight-bold text-capitalize">
                                            <p>Lợi ích từ khóa học</p>
                                        </div>
                                        <div>
                                            <p>{!! $course->benefit !!}</p>
                                        </div>
                                        <div class="font-weight-bold text-capitalize">
                                            <p>Đối tượng mục tiêu</p>
                                        </div>
                                        <div>
                                            <p>{!! $course->target !!}</p>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <br>
                        <div class="row lesson-detail">
                            @if($rate == 0)
                                <div class="tab-rate">
                                    <div class="text-center">
                                        <br>
                                        <h2>Đánh giá từ học viên</h2>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-center font-weight-bold"><strong><h1>0</h1></strong></div>
                                            <div class="text-center font-weight-bold">Chưa có đánh giá</div>
                                            <div class="text-center">
                                                <div class="stars-bottom">
                                                    <span class="fa fa-star "></span>
                                                    <span class="fa fa-star "></span>
                                                    <span class="fa fa-star "></span>
                                                    <span class="fa fa-star "></span>
                                                    <span class="fa fa-star "></span>
                                                </div>
                                            </div>
                                            <div class="text-center">( 0 người đã đánh giá )</div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @else
                                <div class="tab-rate">
                                    <div class="text-center">
                                        <br>
                                        <h3>Đánh giá từ học viên</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-center font-weight-bold"><strong>
                                                    <h1>{{$course->rating}}</h1></strong></div>
                                            <div class="text-center">
                                                <div class="stars-bottom">
                                                    <span class="fa fa-star @if(1 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                    <span class="fa fa-star @if(2 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                    <span class="fa fa-star @if(3 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                    <span class="fa fa-star @if(4 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                    <span class="fa fa-star @if($course->rating ==5) checked @endif"></span>
                                                </div>
                                            </div>
                                            <div class="text-center">( {{$rating->count()}} người đã đánh giá )</div>
                                        </div>
                                    </div>
                                    <div> <!-- cmt cua nguoi dung -->
                                        @foreach($rating as $r)
                                            <div class="row user-comment">
                                                <div class="col-sm-2">
                                                    <div>
                                                        <img src="{{$r->user->image}}"/>
                                                    </div>
                                                </div>
                                                <div class="text-capitalize right-user-comment col-sm-8 tip left">
                                                    <div class="font-weight-bold col-sm-9">
                                                        <span>{{$r->user->fullName}}</span>
                                                    </div>
                                                    <div class="col-sm-9" style="margin-top: 5px">
                                                        <div class="stars-bottom-comment">
                                                            <span class="fa fa-star @if($r->star >=1) checked @endif"></span>
                                                            <span class="fa fa-star @if($r->star >=2) checked @endif"></span>
                                                            <span class="fa fa-star @if($r->star >=3) checked @endif"></span>
                                                            <span class="fa fa-star @if($r->star >=4) checked @endif"></span>
                                                            <span class="fa fa-star @if($r->star ==5) checked @endif"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12"
                                                         style="margin-top: 5px;text-transform: none;">
                                                        {!! $r->comment !!}
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                        {{--<div class="load-more" style="margin-top: 10px"><a href="">Xem thêm</a></div>--}}
                                        <br>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row lesson-detail">
                            <div class="tab-rate">
                                <div class="fb-comments" data-href="{{url($course->slug).'.html'}}"
                                     data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="uk-card uk-card-default uk-card-body"
                             style="z-index: 980; border: 1px solid lightgrey; margin-top: 40px;"
                             uk-sticky="offset: 100; bottom: #endfloat">
                            <div class="col-sm-12 right-text" id="div-fixed">
                                <div>
                                    <div class="text-center text-capitalize font-weight-bold ">
                                        <span> {{ number_format($course->price) }}</span>
                                        <span>₫</span>
                                        <hr>
                                        <span>
                                            <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text-center">
                                                <div class="stars-bottom">
                                                    @if($rate == 0)
                                                        <span class="fa fa-star "></span>
                                                        <span class="fa fa-star "></span>
                                                        <span class="fa fa-star "></span>
                                                        <span class="fa fa-star "></span>
                                                        <span class="fa fa-star "></span>
                                                    @else
                                                        <span class="fa fa-star @if(1 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                        <span class="fa fa-star @if(2 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                        <span class="fa fa-star @if(3 <= $course->rating && $course->rating <= 5  ) checked @endif"></span>
                                                        <span class="fa fa-star @if(4 <= $course->rating && $course->rating <= 5 ) checked @endif"></span>
                                                        <span class="fa fa-star @if($course->rating ==5) checked @endif"></span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        </span>
                                        <hr>
                                        <span class="change"> Thể loại: {{$course->type->typeName}}</span>

                                        <hr>
                                        <span class="change"> Số bài học: {{$course->lessons->count()}}</span>

                                        <hr>
                                        <span class="change"> Thời gian: {{$course->lessons->count()}} ngày</span>

                                        <hr>
                                    </div>
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <div class="btn-add-now font-weight-bold text-center text-capitalize"
                                             data-id="{{$course->id}}" data-name="{{$course->courseName}}"
                                             data-price="{{$course->price}}" data-image="{{$course->image}}"
                                             data-namept="{{$course->user->fullName}}" data-slug="{{$course->slug}}">
                                            <div> mua ngay</div>
                                        </div>
                                        <div class="btn-add font-weight-bold text-center text-capitalize "
                                             data-id="{{$course->id}}" data-name="{{$course->courseName}}"
                                             data-price="{{$course->price}}" data-image="{{$course->image}}"
                                             data-namept="{{$course->user->fullName}} " data-slug="{{$course->slug}}">
                                            <div>thêm vào giỏ hàng</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
    <br>
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-add-now').click(function (e) {
                e.preventDefault();
                // $('.js-panel-cart').addClass('show-header-cart');
                var data = {
                    id: $(this).data('id'),
                    courseName: $(this).data('name'),
                    slug: $(this).data('slug'),
                    price: $(this).data('price'),
                    image: $(this).data('image'),
                    namept: $(this).data('namept'),
                };
                $.ajax({
                    url: '{{route('order.addcart')}}',
                    type: 'post',
                    data: data,
                })
                    .done(function (result) {
                        if (result.status == 3) {
                            swal({
                                title: result.message,
                                text: '',
                                icon: 'error',
                                button: "OK",
                            });
                        }
                        if (result.status == 2) {
                            swal({
                                title: result.message,
                                text: '',
                                icon: 'error',
                                button: "OK",
                            });
                        }
                        if (result.status == 0) {
                            window.location.assign('{{route('order.showcard')}}');
                        }
                        if (result.status == 1) {
                            window.location.assign('{{route('order.showcard')}}');
                        }
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });
            });
        });
    </script>
@endpush