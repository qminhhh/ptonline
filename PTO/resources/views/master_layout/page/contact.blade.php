@extends('welcome')
@section('title')
    Liên hệ
@endsection
@section('content')
    <!-- Title page -->
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('/images/image1/contact/8.jpg');">
        <h2 class="ltext-105 cl0 txt-center">
            Liên hệ
        </h2>
    </section>


    <!-- Content page -->
    <section class="bg0 p-t-104 p-b-116">
        <div class="container">
            <div class="flex-w flex-tr">
                <div class="size-210 bor10 p-lr-70 p-t-55 p-b-70 p-lr-15-lg w-full-md">
                    <form id="frm">
                        <h4 class="mtext-105 cl2 txt-center p-b-30">
                            Gửi tin từ
                        </h4>

                        <div class="bor8 m-b-20 how-pos4-parent">
                            <input class="stext-111 cl2 plh3 size-116 p-l-62 p-r-30" type="email" name="email" placeholder="Địa chỉ e-mail" required>
                            <img class="how-pos4 pointer-none" src="images/homepage/icons/icon-email.png" alt="ICON">
                        </div>

                        <div class="bor8 m-b-20 how-pos4-parent">
                            <input class="stext-111 cl2 plh3 size-116 p-l-62 p-r-30" type="text" name="phone" placeholder="Số điện thoại" minlength="9" required>
                            <img class="how-pos4 pointer-none" src="images/homepage/icons/icon-email.png" alt="ICON">
                        </div>

                        <div class="bor8 m-b-30">
                            <textarea class="stext-111 cl2 plh3 size-120 p-lr-28 p-tb-25" name="contents" placeholder="Chúng tôi có thể giúp gì cho bạn?" minlength="10" required></textarea>
                        </div>

                        <div class="g-recaptcha" data-sitekey="6LdiZWIUAAAAAEOWtPTwWwYWEsS9AbnkMoqD0ppc"></div>

                        <br>

                        <button class="flex-c-m stext-101 cl0 size-121 bg3 bor1 hov-btn3 p-lr-15 trans-04 pointer" type="submit">
                            Gửi
                        </button>
                    </form>

                </div>

                <div class="size-210 bor10 flex-w flex-col-m p-lr-93 p-tb-30 p-lr-15-lg w-full-md">
                    <div class="flex-w w-full p-b-42">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-map-marker"></span>
						</span>

                        <div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Địa chỉ
							</span>

                            <p class="stext-115 cl6 size-213 p-t-18">
                                Trung tâm PTO, Đại học FPT
                            </p>
                        </div>
                    </div>

                    <div class="flex-w w-full p-b-42">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-phone-handset"></span>
						</span>

                        <div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Hãy gọi điện đến
							</span>

                            <p class="stext-115 cl1 size-213 p-t-18">
                                (+84)97 7619 240
                            </p>
                        </div>
                    </div>

                    <div class="flex-w w-full">
						<span class="fs-18 cl5 txt-center size-211">
							<span class="lnr lnr-envelope"></span>
						</span>

                        <div class="size-212 p-t-2">
							<span class="mtext-110 cl2">
								Hỗ trợ bạn
							</span>

                            <p class="stext-115 cl1 size-213 p-t-18">
                                cskh.ptovn@gmail.com
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#frm').submit(function(e) {
                e.preventDefault();
                var data = $(this).serialize();
                $.ajax({
                    url: '{{route('contact.store')}}',
                    type: 'post',
                    data: data,
                })
                    .done(function(result) {
                        swal({
                            title: result.message,
                            text: "",
                            icon: result.code,
                            button: "OK",
                        });
                        $('#frm')[0].reset();
                        grecaptcha.reset();

                    })
                    .fail(function() {
                        swal({
                            title: result.message,
                            text: "",
                            icon: result.code,
                            button: "OK",
                        });
                       // $('#frm')[0].reset();
                        grecaptcha.reset();
                    })
                    .always(function() {
                        console.log("complete");
                    });
            });


        });
    </script>
@endpush