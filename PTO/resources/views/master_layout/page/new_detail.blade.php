@extends('welcome')
@section('title')
    Tin tức chi tiết
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/uikit/css/uikit.css">
    <script src="/uikit/js/uikit.js"></script>
@endsection

@section('content')
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{route('homepage')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Trang chủ
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="{{route('new.index')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Tin tức
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
				{{$new->title}}
			</span>
        </div>
    </div>


    <!-- Content page -->
    <section class="bg0 p-t-52 p-b-20">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3 p-b-80">
                    <div class="uk-card uk-card-default uk-card-body"
                         style="z-index: 980;box-shadow: 0 5px 15px rgba(0, 0, 0, 0);"
                         uk-sticky="offset: 100; bottom: #endfloat">
                        <div class="side-menu" id="bar-fixed">
                            <div class="">
                                <h4 class="mtext-112 cl2 p-b-27">
                                    Thể loại
                                </h4>
                                @foreach($category as $c)
                                    <div class="flex-w m-r--5">
                                        <a href="{{route('category.show',$c->id)}}"
                                           class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                                            {{$c->categoryName}}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 p-b-80">
                    <div class="p-r-45 p-r-0-lg">
                        <!--  -->
                        <div class="wrap-pic-w how-pos5-parent">
                            <img src="{{$new->image}}" alt="IMG-BLOG">

                            <div class="flex-col-c-m size-123 bg9 how-pos5">
								<span class="ltext-107 cl2 txt-center">
									15
								</span>

                                <span class="stext-109 cl3 txt-center">
									06 2018
								</span>
                            </div>
                        </div>

                        <div class="p-t-32">
							<span class="flex-w flex-m stext-111 cl2 p-b-19">
								<span>
									<span class="cl4">Bởi</span> Admin
									<span class="cl12 m-l-4 m-r-6">|</span>
								</span>

								<span>
									{{date('d-m-Y H:i',strtotime($new->created_at))}}
                                    <span class="cl12 m-l-4 m-r-6">|</span>
								</span>

							</span>

                            <h4 class="ltext-109 cl2 p-b-28">
                                {!!$new->title!!}
                            </h4>

                            <p class="stext-117 cl6 p-b-26">
                                {!!$new->content!!}
                            </p>

                        </div>

                        <div class="flex-w flex-t p-t-16">
							<span class="size-216 stext-116 cl8 p-t-4">
								Thể loại
							</span>

                            <div class="flex-w size-217">
                                <a href="#"
                                   class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                                    {{$new->category->categoryName}}
                                </a>

                            </div>
                        </div>

                        <!--  -->
                        <div class="p-t-40">
                            <h5 class="mtext-113 cl2 p-b-12">
                                Bình luận
                            </h5>

                            {{--<p class="stext-107 cl6 p-b-40">--}}
                                {{--Địa chỉ email của bạn sẽ không được công bố. Các trường bắt buộc được đánh dấu *--}}
                            {{--</p>--}}

                            {{--<form>--}}
                                {{--<div class="bor19 m-b-20">--}}
                                    {{--<textarea class="stext-111 cl2 plh3 size-124 p-lr-18 p-tb-15" name="cmt"--}}
                                              {{--placeholder="Bình luận..."></textarea>--}}
                                {{--</div>--}}

                                {{--<div class="bor19 size-218 m-b-20">--}}
                                    {{--<input class="stext-111 cl2 plh3 size-116 p-lr-18" type="text" name="name"--}}
                                           {{--placeholder="Tên *">--}}
                                {{--</div>--}}

                                {{--<div class="bor19 size-218 m-b-20">--}}
                                    {{--<input class="stext-111 cl2 plh3 size-116 p-lr-18" type="text" name="email"--}}
                                           {{--placeholder="E-mail *">--}}
                                {{--</div>--}}

                                {{--<button class="flex-c-m stext-101 cl0 size-125 bg3 bor2 hov-btn3 p-lr-15 trans-04">--}}
                                    {{--Đăng bình luận--}}
                                {{--</button>--}}
                            {{--</form>--}}
                            <div class="row lesson-detail">
                                <div class="tab-rate">
                                    <div class="fb-comments" data-href="{{url($new->slug).'.html'}}" data-numposts="5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection