@extends('welcome')
@section('title')
    Khóa học
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="/uikit/css/uikit.css">
    <script src="/uikit/js/uikit.js"></script>
@endsection
@section('content')
    <div class="p-b-5 m-l-250 m-r-30 text-center ">
        <h3 class="ltext-103 cl5 uppercase" style="padding-top: 30px;">
            * DANH SÁCH KHÓA HỌC - {{$type->typeName}} *
        </h3>
    </div>
    <!-- Content page -->
    <section class="bg0 p-t-30 p-b-60">
        <div class="container">
            <div class="row m-l-30 m-r-30">
                <div class="col-md-3 col-lg-3 p-b-80">
                    <div class="uk-card uk-card-default uk-card-body"
                         style="z-index: 980;box-shadow: 0 5px 15px rgba(0, 0, 0, 0);"
                         uk-sticky="offset: 100; bottom: #endfloat">
                        <div class="side-menu" id="bar-fixed">
                            <div class="">
                                <h4 class="mtext-112 cl2 p-b-27">
                                    Thể loại
                                </h4>
                                @foreach($types as $t)
                                    <div class="flex-w m-r--5">
                                        <a href="{{route('type.show',$t->id)}}"
                                           class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-13">
                                            {{$t->typeName}}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 p-b-80 row" id="content">
                    @if($type->courses != null )
                        @foreach($type->courses as $course)
                            <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15 col-md-4">
                                <!-- Block2 -->
                                <div class="block2" id="course2">
                                    <div class="block2-pic " id="course1">
                                        <img width="263px" height="200px" src="{{$course->image}}" alt="IMG-PTO">
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12" style="margin-top: 5px">
                                            <div class="stars-bottom-comment">
                                                <span class="fa fa-star @if($course->rating >=1) checked @endif"></span>
                                                <span class="fa fa-star @if($course->rating >=2) checked @endif"></span>
                                                <span class="fa fa-star @if($course->rating >=3) checked @endif"></span>
                                                <span class="fa fa-star @if($course->rating >=4) checked @endif"></span>
                                                <span class="fa fa-star @if($course->rating ==5) checked @endif"></span>
                                                <span class=""> ( @if($course->rate != null)  {{$course->rate->count()}}
                                                    đánh
                                                    giá
                                                    )  @else 0 đánh giá ) @endif </span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="block2-txt flex-w flex-t p-t-14">
                                        <div class="block2-txt-child1 flex-col-l ">
                                <span class="stext-105 cl3">
                                  <strong><a href="{{route('courses.showCourse',$course->slug)}}">{{$course->courseName}}</a></strong>
                                    {{--<strong>  Tên khóa học: </strong>--}}

                              </span>

                                            <span class="stext-105 cl3"
                                                  style="margin-left: 30px; margin-top: 10px;">
                                 PT: <a href="{{route('ptodetail',$course->user->id)}}">{{$course->user->fullName}}</a>
                                                {{--<strong>  Tên khóa học: </strong>--}}

                              </span>
                                            <br>
                                            <span class="stext-105 cl3">
                                <h4><span><b style="margin-left: 50px;"> {{ number_format($course->price) }}</b></span><span><b> ₫</b></span></h4><br/>
                            </span>

                                        </div>

                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            <div class="m-l-20 m-b-20">
                                                <a href="{{route('courses.showCourse',$course->slug)}}"
                                                   style="border: 1px solid" class="btn btn-default">
                                                    <i class="fa fa-paper-plane" style="font-size:16px;"></i> Chi tiết
                                                </a>

                                                <a class="btn-add btn btn-default "
                                                   style="border: 1px solid blue"
                                                   data-id={{$course->id}} data-name="{{$course->courseName}}"
                                                   data-price="{{$course->price}}" data-image="{{$course->image}}"
                                                   data-namept="{{$course->user->fullName}}"
                                                   data-slug="{{$course->slug}}">
                                                    <i class="fa fa-shopping-cart" style="font-size:16px;"></i> Giỏ hàng
                                                </a>
                                            </div>
                                        @else
                                            <div class="m-l-75 m-b-20">
                                                <a href="{{route('courses.showCourse',$course->slug)}}"
                                                   style="border: 1px solid" class="btn btn-default">
                                                    <i class="fa fa-paper-plane" style="font-size:16px;"></i> Chi tiết
                                                </a>

                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15 col-md-4">
                            Không có khóa học !
                        </div>
                    @endif
                </div>
            </div>
        </div>


    </section>
@endsection

@push('scripts')
    <script>
        var vm = new Vue({
            el: '#cart',
            data: {carts: []},
            methods: {
                renderCart: function (data) {
                    this.carts = data;
                }
            },

        })
    </script>
@endpush
