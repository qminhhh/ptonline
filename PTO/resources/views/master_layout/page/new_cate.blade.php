@extends('welcome')
@section('title')
    Tin tức
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="/uikit/css/uikit.css">
    <script src="/uikit/js/uikit.js"></script>
@endsection
@section('content')
    <!-- Title page -->
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('/images/image1/contact/2.jpg');">
        <h2 class="ltext-105 cl0 txt-center uppercase" style="color: #ece3e3;">
            TIN TỨC - {{$category->categoryName}}
        </h2>
        <br>
        <p class="txt-center" style="color: #fff;font-size: 20px;">Cập nhật liên tục các tin tức về thể hình nhanh và
            sớm nhất để các bạn Gymer luôn cập nhật được tin tức nhanh và hay nhất về thế giới Gymer.</p>
    </section>


    <!-- Content page -->
    <section class="bg0 p-t-62 p-b-60">
        <div class="container">
            <div class="row m-l-30 m-r-30">

                <div class="col-md-3 col-lg-3 p-b-80">
                    <div class="uk-card uk-card-default uk-card-body"
                         style="z-index: 980;box-shadow: 0 5px 15px rgba(0, 0, 0, 0);"
                         uk-sticky="offset: 100; bottom: #endfloat">
                        <div class="side-menu" id="bar-fixed">
                            <div class="">
                                <h4 class="mtext-112 cl2 p-b-27">
                                    Thể loại
                                </h4>
                                @foreach($categories as $c)
                                    <div class="flex-w m-r--5">
                                        <a href="{{route('category.show',$c->id)}}"
                                           class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
                                            {{$c->categoryName}}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 p-b-80" id="content">
                    <div class="p-r-45 p-r-0-lg">
                    @foreach($category->news  as $n)
                        <!-- item new -->
                            <div class="p-b-63 row">
                                <div class="col-md-4">
                                    <a href="{{route('new.newdetail',$n->slug)}}" class="hov-img0 how-pos5-parent">
                                        <img src="{{$n->image}}" alt="IMG-BLOG">

                                        <div class="flex-col-c-m size-123 bg9 how-pos5">
									<span class="ltext-107 cl2 txt-center">
										15
									</span>

                                            <span class="stext-109 cl3 txt-center">
										06 2018
									</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-8">

                                    <h4 class="p-b-15">
                                        <a href="{{route('new.newdetail',$n->slug)}}"
                                           class="ltext-108 cl2 hov-cl1 trans-04">
                                            {!! $n->title !!}
                                        </a>
                                    </h4>

                                    <p class="stext-117 cl6">
                                        {!! $n->abbreviate !!}
                                    </p>

                                    <div class="flex-w flex-sb-m p-t-18">
									<span class="flex-w flex-m stext-111 cl2 p-r-30 m-tb-10">
										<span>
											<span class="cl4">Bởi</span> Admin
											<span class="cl12 m-l-4 m-r-6">|</span>
										</span>

										<span>
											{{$n->category->categoryName}}
                                            <span class="cl12 m-l-4 m-r-6">|</span>
										</span>

									</span>
                                        <a href="{{route('new.newdetail',$n->slug)}}"
                                           class="stext-101 cl2 hov-cl1 trans-04 m-tb-10">
                                            Đọc tiếp


                                            <i class="fa fa-long-arrow-right m-l-9"></i>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>

    </section>
@endsection

@push('scripts')
@endpush
