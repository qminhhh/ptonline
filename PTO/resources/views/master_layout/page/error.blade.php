@extends('master_layout.admin.index')
@section('content')
    <!-- Main content -->
    <section class="content">
            <h1>
                Lỗi 401
            </h1>
            <div class="error-page">
                <h2 class="headline text-yellow"> 401</h2>
                <div class="error-content">
                    <h3><i class="fa fa-warning text-yellow"></i> Oops! Không có quyền truy cập</h3>

                    <p>
                        <br/>
                        Bạn không được phép truy cập vào tài nguyên này.
                        <br/>
                    </p>

                    <form class="search-form">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i
                                            class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.input-group -->
                    </form>
                </div>
            </div>


    </section>

@endsection


