@extends('welcome')
@section('title')
    Chi tiết PTO
@endsection
@section('content')
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="{{route('homepage')}}" class="stext-109 cl8 hov-cl1 trans-04">
                Trang chủ
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="{{route('showall')}}" class="stext-109 cl8 hov-cl1 trans-04">
                PTO
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
            Chi tiết
        </span>
        </div>
    </div>


    <!-- Product Detail -->
    <section class="sec-product-detail bg0 p-t-65 p-b-60">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-7 p-b-30">
                    <div class="p-l-25 p-r-30 p-lr-0-lg">
                        <div class="wrap-slick3 flex-sb flex-w">
                            <div class="wrap-slick3-dots" style="display: none;"></div>
                            <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                            <div class="slick3 gallery-lb">
                                <div class="item-slick3" data-thumb="images/product-detail-01.jpg">
                                    <div class="wrap-pic-w pos-relative">
                                        <img src="{{$user->image}}" alt="IMG-PRODUCT">

                                        <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04"
                                           href="{{$user->image}}">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-5 p-b-30">
                    <div class="p-r-50 p-t-5 p-lr-0-lg">
                        <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                            PT: {{$user->fullName}}
                        </h4>

                        <div class="stext-102 cl3 p-t-23">
                            <strong>Chiều cao : </strong> {{$user->height}} cm
                        </div>
                        <div class="stext-102 cl3 p-t-23">
                            <strong>Cân nặng :</strong> {{$user->weight}} kg
                        </div>

                        <div class="stext-102 cl3 p-t-23">
                            <strong>Số đo ba vòng : </strong> {{$user->bust}} - {{$user->wais}} - {{$user->hips}} cm
                        </div>

                        <p class="stext-102 cl3 p-t-23" type="file">
                            <strong>Giới thiệu bản thân :</strong>
                            <br>
                            {!! $user->introduce !!}
                        </p>
                        <p class="stext-102 cl3 p-t-23" type="file">
                            <strong> Tại sao bạn nên chọn mình làm PT</strong>
                            <br>
                            {!! $user->reason !!}
                        </p>
                        <p class="stext-102 cl3 p-t-23" type="file">
                            <strong>Chứng chỉ/ Bằng cấp :</strong>
                            <br>
                            {!! $user->certificate !!}
                        </p>
                        <div class="stext-102 cl3 p-t-23">
                            <strong>Số điện thoại : </strong> {{$user->phone}}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Related Products -->
            <section class="sec-relate-product bg0 p-t-45 p-b-105">
                <div class="container">
                    <div class="p-b-45">
                        <h1 class="ltext-80 cl5 txt-center">
                            ----- Khóa học của PT {{$user->fullName}} -----
                        </h1>
                    </div>

                    <!-- Slide2 -->
                    <div class="wrap-slick2">
                        <div class="slick2">
                            @foreach($user->courses as $c )
                                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15 ">
                                    <!-- Block2 -->
                                    <div class="block2" id="course2">
                                        <div class="block2-pic hov-img0" id="course1">
                                            <img width="263px" height="200px" src="{{$c->image}}" alt="IMG-PTO">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12" style="margin-top: 5px">
                                                <div class="stars-bottom-comment">
                                                    <span class="fa fa-star @if($c->rating >=1) checked @endif"></span>
                                                    <span class="fa fa-star @if($c->rating >=2) checked @endif"></span>
                                                    <span class="fa fa-star @if($c->rating >=3) checked @endif"></span>
                                                    <span class="fa fa-star @if($c->rating >=4) checked @endif"></span>
                                                    <span class="fa fa-star @if($c->rating ==5) checked @endif"></span>
                                                    <span class=""> ( @if($c->rate != null)  {{$c->rate->count()}} đánh
                                                        giá )  @else 0 đánh giá ) @endif </span>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="block2-txt flex-w flex-t p-t-14">
                                            <div class="block2-txt-child1 flex-col-l ">
                                <span class="stext-105 cl3">
                                  <strong><a href="{{route('courses.showCourse',$c->slug)}}">{{$c->courseName}}</a></strong>
                                    {{--<strong>  Tên khóa học: </strong>--}}

                              </span>
                                                <span class="stext-105 cl3">

                                <br>
                                <h4><span><b style="margin-left: 50px;"> {{ number_format($c->price) }}</b></span><span><b> ₫</b></span></h4><br/>
                            </span>

                                            </div>


                                            <br>

                                            @if(\Illuminate\Support\Facades\Auth::check())
                                                <div class="m-l-20 m-b-20">
                                                    <a href="{{route('courses.showCourse',$c->slug)}}"
                                                       style="border: 1px solid" class="btn btn-default">
                                                        <i class="fa fa-paper-plane" style="font-size:16px;"></i> Chi
                                                        tiết
                                                    </a>

                                                    <a class="btn-add btn btn-default "
                                                       style="border: 1px solid blue"
                                                       data-id={{$c->id}} data-name="{{$c->courseName}}"
                                                       data-price="{{$c->price}}" data-image="{{$c->image}}"
                                                       data-namept="{{$c->user->fullName}}" data-slug="{{$c->slug}} ">

                                                        <i class="fa fa-shopping-cart" style="font-size:16px;"></i> Giỏ
                                                        hàng
                                                    </a>
                                                </div>
                                            @else
                                                <div class="m-l-75 m-b-20">
                                                    <a href="{{route('courses.showCourse',$c->slug)}}"
                                                       style="border: 1px solid" class="btn btn-default">
                                                        <i class="fa fa-paper-plane" style="font-size:16px;"></i> Chi
                                                        tiết
                                                    </a>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection

@push('scripts')
    <script>

    </script>
@endpush