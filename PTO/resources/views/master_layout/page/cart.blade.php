@extends('welcome')
@section('title')
    Giỏ Hàng
@endsection
@section('content')

    <!--giỏ hàng-->
    <div class="card-outside">
        <div class="cart_status">
            <div style="margin-left: 15px">
                <label class="text-uppercase ">Giỏ hàng:</label>
                <label> {{Cart::count()}} khoá học</label>
            </div>
            <div class="de-cuong">
                <div class="col-sm-8">
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="cart-col row">
                <div class="cart-col-1 col-sm-8">
                    @foreach(Cart::content() as $row)
                        <div class="row shopping-cart-item">
                            <div class="col-sm-3">

                                <p>
                                    <img src="{{$row->options->has('image')?$row->options->image:''}}" alt="IMG">
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p>
                                    <label><b>Khoá học:</b> <a href="{{route('courses.showCourse',$row->options->has('slug')?$row->options->slug:'')}}">{{$row->name}}</a></label>
                                </p>
                                <p>

                                </p>
                                <p>
                                    <label><b>PTO :</b> {{$row->options->has('namept')?$row->options->namept:''}}
                                    </label>
                                </p>
                                <p>
                                    <a href="#"><label class="btn-delete-cart" data-id="{{$row->rowId}}">Xóa</label></a>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <span><b>  {{ number_format($row->price) }} </b></span><span><b> ₫</b></span><br/>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="cart-col-2 col-sm-3">

                    <div>
                        <div><label>Thành tiền:</label></div>
                        <div><label> {{Cart::subtotal()}} ₫
                                <hr>
                                * giá trên đã bao gồm VAT
                            </label></div>
                    </div>

                    <div>
                        <button type="button" data-toggle="modal" data-target="#myModal"
                                class="btn btn-large btn-block btn-primary btn-checkout">
                            Thanh toán khi nhận hàng
                        </button>

                        <button type="button" data-toggle="modal" data-target="#modalOnline"
                                class="btn btn-large btn-block btn-success btn-checkout">
                            Thanh toán trực tuyến
                        </button>

                    </div>


                </div>


            </div>
        </div>

    </div>

    @if(Cart::count() > 0)
        <!-- Modal -->
        <div id="myModal" style="z-index: 999999 "
             class="modal fade" role="dialog">
            <div class="modal-dialog" style="max-width:80%">

                <!-- Modal content-->
                <div class="modal-content " style="opacity: 0.95">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Thông tin thanh toán</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="">
                        <form action="{{route('order.saveCOD')}}" method="post" role="form">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""><strong>Họ tên</strong></label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               id="name"
                                               placeholder="Nhập họ tên" name="name"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" required
                                               minlength=3>
                                        @if ($errors->has('name'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""><strong>Tỉnh / thành</strong></label>
                                        <select class="form-control" name="province" id="provence">
                                            @foreach($province as $pro)
                                                <option
                                                        @if(\Illuminate\Support\Facades\Auth::user()->province == $pro->provinceName)
                                                        {{"selected"}}
                                                        @endif
                                                        value="{{$pro->provinceName}}">
                                                    {{$pro->provinceName}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""><strong>Quận/Huyện</strong></label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}"
                                               id="district"
                                               placeholder="Nhập quận/huyện" name="district"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->district}}"
                                               minlength="5" required>
                                        @if ($errors->has('district'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('district') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>


                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for=""><strong>Địa chỉ</strong></label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                               id="address"
                                               placeholder="Nhập địa chỉ" name="address"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->address }}"
                                               minlength="10" required>
                                        @if ($errors->has('address'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for=""><strong>Email</strong></label>
                                        <input type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               id="email"
                                               placeholder="Nhập email" name="email"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" required>
                                        @if ($errors->has('email'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for=""><strong>Số điện thoại</strong></label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                               id="phone"
                                               placeholder="Nhập số điện thoại" name="phone"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->phoneOrder }}"
                                               minlength="9" required>
                                        @if ($errors->has('phone'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for=""><strong>Ghi chú</strong></label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}"
                                               id="note"
                                               placeholder="Nhập ghi chú" name="note"
                                               value="{{ old('note') }}">
                                        @if ($errors->has('note'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('note') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                            </div>


                            <div id="recaptcha" class="g-recaptcha"
                                 data-sitekey="6LdiZWIUAAAAAEOWtPTwWwYWEsS9AbnkMoqD0ppc"></div>
                            <span class="msg-error error"></span>
                            <br>
                            <button type="submit" class="btn btn-primary">Thanh Toán</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!--end giỏ hàng-->
        <!-- Modal -->
        <div id="modalOnline" style="z-index: 999999 "
             class="modal fade" role="dialog">
            <div class="modal-dialog" style="max-width:80%">

                <!-- Modal content-->
                <div class="modal-content " style="opacity: 0.95;margin-top: 150px;">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Thanh toán online </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><strong>Hệ thống đang trong quá trình bảo trì nên đơn hàng của bạn sẽ
                                            được
                                            thanh toán bằng đơn vị USD với tỷ giá 1$ = 23,254.0 ₫</strong></label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><strong>Tổng đơn hàng : </strong></label>
                                    <span><b>  {{ number_format(($row->price / 23254),2) }} </b></span><span><b> $</b></span><br/>
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for=""><strong>Xin lỗi vì sự bất tiện này !</strong></label>
                                </div>
                            </div>

                        </div>
                        <a href="{{route('paypal.index')}}" class="btn btn-primary">Tôi đồng ý</a>

                    </div>
                </div>

            </div>
        </div>
        <!--end giỏ hàng-->
    @endif



@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-delete-cart').click(function (e) {
                e.preventDefault();
                var data = {
                    id: $(this).data('id'),
                };
                $.ajax({
                    url: '{{route('order.deleteCart')}}',
                    type: 'post',
                    data: data,
                })
                    .done(function () {
                        window.location.reload();
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });
            });

            $('form').submit(function (e) {
                var $captcha = $('#recaptcha'),
                    response = grecaptcha.getResponse();

                if (response.length === 0) {
                    $('.msg-error').text("reCAPTCHA không được để trống");
                    if (!$captcha.hasClass("error")) {
                        $captcha.addClass("error");
                    }
                    e.preventDefault();
                } else {
                    $('.msg-error').text('');
                    $captcha.removeClass("error");
                    return true;
                    //    alert('reCAPTCHA marked');
                }

            });

        });
    </script>
@endpush