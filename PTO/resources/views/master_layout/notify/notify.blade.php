@extends('master_layout.admin.index')
@section('titles')
    Danh sách thông báo
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thông báo
            <small>danh sách</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('order.listCourse')}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- social buttons -->
                    <div class="box">
                            <div class="box-header">
                                <li class="header">Bạn có {{$count}} thông báo mới / {{$notifies->count()}} thông báo .</li>
                            </div>
                            <div class="box-body chat" id="chat-box">
                                @foreach($notifies as $notify)
                                        <div class="item" @if($notify->status == 0) style="background-color: #f5f0ef;" @endif>
                                            <img src="{{$notify->user->image}}" alt="user image" class="offline"
                                                 style="border: 2px solid #d4c4c2;">

                                            <p class="message">
                                                <a href="{{route('notify.seen',$notify->id).'?link='.$notify->link}}"
                                                   class="name" style=" margin-top: 10px;">
                                                    <small class="text-muted pull-right"><i
                                                                class="fa fa-clock-o"></i> {{date('d-m-Y H:i',strtotime($notify->created_at))}}
                                                    </small>
                                                    {!! $notify->message !!}
                                                </a>
                                            </p>
                                        </div>
                                @endforeach
                            </div>
                            <!-- /.item -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

@endpush
