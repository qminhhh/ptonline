<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">

        @if(Auth::user()->idRole ==2)
            <span class="logo-mini"><b>P</b>TO</span>
        @elseif(Auth::user()->idRole ==1)
            <span class="logo-mini"><b>A</b>dmin</span>
        @endif

    <!-- logo for regular state and mobile devices -->
        @if(Auth::user()->idRole ==2)
            <span class="logo-lg"><b>Kênh </b>PTO</span>
        @elseif(Auth::user()->idRole ==1)
            <span class="logo-lg"><b>Kênh </b>Quản trị</span>
        @endif

    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{Auth::user()->image}}" class="user-image" alt="User Image">
                        <span class="hidden-xs"> {{Auth::user()->fullName}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{Auth::user()->image}}" class="img-circle" alt="User Image">

                            <p>

                                {{Auth::user()->userName}}
                                <small>Member since {{Auth::user()->created_at}}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            {{--<div class="row">--}}
                            @if(in_array('read-admin',$menu))
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat" href="{{route('users.show',Auth::id())}}">
                                        Thông tin của tôi
                                    </a>
                                </div>
                            @endif
                            @if(in_array('read-pto',$menu))
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat" href="{{route('pto.show',Auth::id())}}">
                                        Thông tin của tôi
                                    </a>
                                </div>
                            @endif
                            <div class="pull-right">
                                <form method="POST" action="{{route('logout')}}">
                                    @csrf
                                    <button class="btn btn-default btn-flat" type="submit">Đăng xuất</button>
                                </form>
                            </div>
                        {{--</div>--}}
                        <!-- /.row -->
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
    {{--<link rel="stylesheet" type="text/css" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"/>--}}
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{Auth::user()->image}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->fullName}}</p>
                <i class="fa fa-circle text-success"></i> Online
            </div>
        </div>
        <!-- search form -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            @if(in_array('read-dashboard',$menu))
                <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard"></i>
                        <span>Thống kê</span></a></li>
            @endif


            @if(in_array('read-pto',$menu))
                <li class="active treeview">
                    <a href="#">
                        <i class="glyphicon glyphicon-list-alt"></i> <span>Quản lý Khóa học</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('courses.index')}}"><i
                                        class="glyphicon glyphicon-saved"></i>Khóa
                                học đã kích hoạt</a></li>
                        <li class="active"><a href="{{route('courses.deactive')}}"><i
                                        class="glyphicon glyphicon-import"></i>Khóa học chưa kích hoạt</a></li>
                        <li class="active"><a href="{{route('courses.lock')}}"><i
                                        class="glyphicon glyphicon-lock"></i>Khóa học đã bán</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array('read-pto',$menu))

                <li><a href="{{route('users.listTrainee')}}"><i class="glyphicon glyphicon-user"></i>
                        <span>Quản lý Học viên</span></a></li>

            @endif

            @if(in_array('read-pto',$menu))

                <li><a href="{{route('order.listCourse')}}"><i class="glyphicon glyphicon-usd"></i>
                        <span>Quản lý Tài chính</span></a></li>
            @endif

            @if(in_array('read-pto',$menu))
                <li><a href="{{route('notify.index')}}"><i class="glyphicon glyphicon-bell"></i>
                        <span>Thông báo  </span>
                        <span class="label label-warning">{{$g_notifies}}</span></a>
                </li>
            @endif


            @if(in_array('read-admin',$menu))
                <li class="active treeview">
                    <a href="#">
                        <i class="glyphicon glyphicon-user"></i> <span>Quản lý PTO</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('pto-list')}}"><i class="glyphicon glyphicon-th-list"></i>Danh
                                sách
                                PTO</a></li>
                        <li><a href="{{route('pto.list')}}"><i class="glyphicon glyphicon-refresh"></i>Danh sách PTO
                                chờ phê duyệt</a></li>
                    </ul>
                </li>
            @endif
            @if(in_array('read-admin',$menu))
                <li class="active treeview">
                    <a href="#">
                        <i class="glyphicon glyphicon-bookmark"></i> <span>Quản lý Khóa học</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{route('courses.list')}}"><i class="glyphicon glyphicon-list-alt"></i>
                                <span>Danh sách Khóa học</span></a></li>
                        <li>
                            <a href="{{route('type.list')}}"><i class="glyphicon glyphicon-paperclip"></i>Thể loại Khóa
                                học</a>
                        </li>
                    </ul>

            @endif

            @if(in_array('read-admin',$menu))
                <li><a href="{{route('order.list')}}"><i class="glyphicon glyphicon-usd"></i>
                        <span>Quản lý Đơn hàng</span></a>
                </li>
            @endif
            {{--mới sửa--}}
            {{--@if(in_array('read-contact',$menu))--}}
            @if(in_array('read-admin',$menu))
                <li><a href="{{route('contact.list')}}"><i class="glyphicon glyphicon-phone-alt"></i>
                        <span>Quản lý Liên hệ</span></a></li>
            @endif
            {{--@endif--}}
            @if(in_array('read-admin',$menu))
                <li><a href="{{route('userslist')}}"><i class="glyphicon glyphicon-education"></i>

                        <span>Quản lý Người dùng</span></a></li>
            @endif
            @if(in_array('read-admin',$menu))
                <li><a href="{{route('admin.index')}}"><i class="glyphicon glyphicon-eye-open"></i>
                        <span>Quản lý Nhóm</span></a></li>
            @endif

            @if(in_array('read-admin',$menu))
                <li class="active treeview">
                    <a href="#">
                        <i class="glyphicon glyphicon-camera"></i> <span>Quản lý Tin tức</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('category.list')}}"><i class="glyphicon glyphicon-fire"></i>Danh
                                sách Thể loại</a></li>
                        <li><a href="{{route('new.list')}}"><i class="glyphicon glyphicon-folder-open"></i>Danh sách Bài
                                viết</a>
                        </li>
                    </ul>
                </li>
            @endif
            {{--@if(in_array('read chat',$menu))--}}
            {{--<li><a href="{{route('chat.index')}}"><i class="fa fa-envelope"></i>--}}
            {{--<span>Tin nhắn</span></a>--}}
            {{--</li>--}}
            {{--@endif--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<script>

    $(document).ready(function () {
        $('#dob').datepicker({});
        $('.btn-danger').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            alert();
        });

    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

