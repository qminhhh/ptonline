@extends('master_layout.admin.index')
@section('titles')
    Danh sách học viên
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Học viên
            <small>danh sách</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('users.listTrainee')}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách học viên</div>
                    <div class="panel-body">
                        <table id="trainee-table" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Tên khóa học</th>
                                <th>Tiến trình</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#trainee-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('users.dataTrainee') !!}',
                columns: [
                    {data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {data: 'nameTrainee', name: 'nameTrainee'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'nameCourse', name: 'nameCourse'},
                    {
                        data: function (data) {
                            if (data.process == null) {
                                return '<div class="clearfix">\n' +
                                    '<span class="pull-left">Chưa học</span>\n' +
                                    '<small class="pull-right">0%</small>\n' +
                                    '</div>\n' +
                                    '<div class="progress xs">\n' +
                                    '<div class="progress-bar progress-bar-green" style="width: 0%;"></div>\n' +
                                    '</div>';
                            } else {
                                if (data.process == 100) {
                                    return '<div class="clearfix">\n' +
                                        '<span class="pull-left">Hoàn thành</span>\n' +
                                        '<small class="pull-right">100%</small>\n' +
                                        '</div>\n' +
                                        '<div class="progress xs">\n' +
                                        '<div class="progress-bar progress-bar-green" style="width: 100%;"></div>\n' +
                                        '</div>';
                                } else {
                                    return `<div class="clearfix">
                                    <span class="pull-left">Đang học</span>
                                <small class="pull-right">` + data.process + `%</small>
                                    </div>
                                    <div class="progress xs">
                                    <div class="progress-bar progress-bar-red" style="width: ` + data.process + `%;"></div>
                                    </div>`;
                                }
                            }
                        }, name: 'process'
                    },
                ]
            });
        });
    </script>
@endpush
