@extends('master_layout.admin.index')
@section('titles')
    Thêm mới tin tức
@endsection
@section('content')

    <!-- Main content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tin tức
            <small>thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('new.create')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>+</strong> Thêm mới Tin tức</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form action="{{route('new.store')}}" enctype="multipart/form-data" method="POST" role="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Thể loại</label>
                                <select class="form-control" name="category" id="categoryName">
                                    @foreach($category as $cate)
                                        <option value="{{$cate->id}}">{{$cate->categoryName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                       id="" placeholder="Nhập tiêu đề" name="title" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Hình ảnh</label>
                                <input type="file" name="image"
                                       class="form-control {{ $errors->has('image') ? ' is-invalid' : '' }}">
                                @if ($errors->has('image'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Nội dung tóm tắt</label>
                                <textarea name="abbreviate" id="input"
                                          class="form-control ckeditor {{ $errors->has('abbreviate') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('abbreviate') }}</textarea>
                                @if ($errors->has('abbreviate'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('abbreviate') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Nội dung chi tiết</label>
                                <textarea name="contents" id="input"
                                          class="form-control ckeditor {{ $errors->has('contents') ? ' is-invalid' : '' }}"
                                          rows="3" required="required">{{ old('contents') }}</textarea>
                                @if ($errors->has('contents'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('contents')}}</strong>
                                </span>
                                @endif
                            </div>

                            <input type="checkbox" name="public" value="1">
                            <label for="option"><span></span>
                                <p>Hiển thị lên trang chủ</p></label>
                            <br>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection


