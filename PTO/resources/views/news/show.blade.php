@extends('master_layout.admin.index')
@section('titles')
    Chi tiết tin tức
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tin tức
            <small>chi tiết</small>
        </h1>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('new.show',$new)}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Chi tiết tin tức</h3>
                    </div>
                    <!-- /.box-header -->
                    <form method="POST" enctype="multipart/form-data" action="">
                        @csrf
                    <div class="box-body">

                        <p class="text-muted">
                            <img src="{{$new->image}}" alt="" width="250px">
                        </p>

                        <hr>

                        <strong><i class="fa fa-diamond margin-r-5"></i> Thể loại : </strong>

                        <p class="text-muted">
                           {{$new->category->categoryName}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-pencil  margin-r-5"></i> Tiêu đề : </strong>

                        <p class="text-muted">
                            {{$new->title}}
                        </p>

                        <hr>

                        <strong><i class="fa fa-send margin-r-5"></i> Nội dung tóm tắt : </strong>

                        <p class="text-muted">
                           {!! $new->abbreviate !!}
                        </p>

                        <hr>

                        <strong><i class="fa fa-send margin-r-5"></i> Nội dung : </strong>

                        <p class="text-muted">
                            {!! $new->content !!}
                        </p>

                        <hr>

                        <strong><i class="fa fa-send margin-r-5"></i> Trạng thái : </strong>

                        <p class="text-muted">

                            @if($new->status == 1)
                                Hiển thị trên trang chủ
                                @else
                                Riêng tư
                                @endif
                        </p>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4 pull-right">
                                <a href="{{route('new.edit',$new->id)}}" class="btn btn-default btn-flat">Chỉnh sửa tin tức</a>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection