@extends('master_layout.admin.index')
@section('titles')
    Danh sách tin tức
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tin Tức
            <small>Danh sách</small>
        </h1>
        <br>
        <a title="Show" href="{{route('new.create')}}" class="btn btn-xs btn-info"><i
                    class="glyphicon glyphicon-plus"></i>Thêm tin tức
        </a>
        <ol class="breadcrumb">
            <li>{{ Breadcrumbs::render('new.list')}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row ">
            {{--col-md-10 col-md-offset-1--}}
            <div class="col-md-12">
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Danh sách tin tức</div>
                    <div class="panel-body">
                        <table id="new-table" class="table table-bordered table-hover dataTable" style="width:100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ảnh</th>
                                <th style="width: 300px">Tiêu đề</th>
                                <th>Thể loại</th>
                                <th>Đăng tin</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script_footer')

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#new-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('new.data') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: function (data) {
                            if (data.image != null) {
                                return "<img width=\"100px\" src='" + data.image + "'>";
                            } else {
                                return "";
                            }
                        }, name: 'image'
                    },
                    {data: 'title', name: 'title'},
                    {data: 'cat_name', name: 'cat_name'},
                    {
                        data: function (data) {
                            if (data.status != null) {
                                if (data.status == 0) {
                                    return '<span class="text-danger glyphicon glyphicon-remove">';
                                }
                                else {
                                    return '<span class="text-success glyphicon glyphicon-ok"></span>';
                                }
                            } else {
                                return "";
                            }
                        }, name: 'status'
                    },
                    {data: 'actions', name: 'actions'},
                ]
            });
        });
        $(document).on('click', '.btn-danger', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var form = $(this).parent('form');
            swal({
                title: "Bạn có muốn xóa tin tức",
                text: "Dữ liệu sẽ mất!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: "Xóa!",
                cancelButtonText: "Hủy",
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
                buttons: true,
                dangerMode: true,
            }).then(function (result) {
                    if(result){
                        form.submit();
                    }else{
                        swal("Liên hệ không xóa!");
                    }

                },
            );
        });
    </script>
@endpush
