<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class RPTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i < 53; $i++) {
            DB::table('role_permissions')->insert([
//                'idRole' => $faker->numberBetween(1,2),
//                'idPermission' => $faker->numberBetween(1,44),
                'idRole' => 1,
                'idPermission' => $i,

            ]);

        }
    }
}
