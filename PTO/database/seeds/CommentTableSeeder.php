<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<10;$i++) {
            DB::table('comment')->insert([
                'idUser' => $faker->numberBetween(1,10),
                'idLesson' => $faker->numberBetween(23,26),
                'content' => $faker->paragraph,
                'parent_id' => 0,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
