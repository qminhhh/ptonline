<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['admin', 'pto', 'user'];
        foreach ($roles as $role) {
                DB::table('roles')->insert([
                    'roleName' => $role,
                    'slug' => str_slug($role),
                ]);
        }


    }
}
