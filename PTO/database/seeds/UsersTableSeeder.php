<?php

use Illuminate\Database\Seeder;
//use Carbon\Carbon;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function imagesList($path = 'upload/fakers')
    {
        $images = [];
        $path = public_path($path);
            $allowedMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];
            $files = File::allFiles($path);
            foreach ($files as $file) {
                $contentType = mime_content_type((string)$file);
                if (in_array($contentType, $allowedMimeTypes)) {
                    $images[] = stristr((string)$file, DIRECTORY_SEPARATOR . "upload");
                }
            }

            if (empty($images) or count($images) < 5) {
                uploadImages();

                return imagesList();
            }

        return $images;
    }

    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 30; $i++) {
                DB::table('users')->insert([
//                'image'=>$faker->imageUrl(),
                    'image' =>$faker->randomElement($this->imagesList()),
                    'fullName' => $faker->name,
                    'userName' => $faker->name,
                    'password' => bcrypt('secret'),
                    'gender' => $faker->numberBetween(0, 1),
                    'phone' => $faker->numberBetween(10000, 9000000),
                    'email' => $faker->email,
                    'DOB' => $faker->dateTime(),
                    'status' => $faker->numberBetween(0, 1),
                    'introduce' => $faker->paragraph,
                    'height' => $faker->numberBetween(150, 190),
                    'weight' => $faker->numberBetween(40, 120),
                    'bust' => $faker->numberBetween(60, 120),
                    'wais' => $faker->numberBetween(60, 120),
                    'hips' => $faker->numberBetween(60, 120),
                    'certificate' => $faker->paragraph,
                    'reason' => $faker->paragraph,
                    'rate' => $faker->numberBetween(0, 5),
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime(),
                    'idRole' => $faker->numberBetween(1, 2),

                ]);
        }
    }
}
