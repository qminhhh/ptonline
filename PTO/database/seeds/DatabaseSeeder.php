<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderDetailTableSeeder::class);
        $this->call(LessonsTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RPTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(BodyProcessTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(NotifyTableSeeder::class);
    }
}
