<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function imagesList($path = 'upload/courses')
    {
        $images = [];
        $path = public_path($path);
        $allowedMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];
        $files = File::allFiles($path);
        foreach ($files as $file) {
            $contentType = mime_content_type((string)$file);
            if (in_array($contentType, $allowedMimeTypes)) {
                $images[] = stristr((string)$file, DIRECTORY_SEPARATOR . "upload");
            }
        }

        if (empty($images) or count($images) < 5) {
            uploadImages();

            return imagesList();
        }

        return $images;
    }
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<10;$i++) {
            DB::table('courses')->insert([
                'courseName' =>$faker->name,
                'idUser' => $faker->numberBetween(1,10),
                'type_id' => $faker->numberBetween(1,5),
                'price' => $faker->numberBetween(10000,9000000),
                'introduce' => $faker->paragraph,
//                'image' => $faker->imageUrl(),
                'image' =>$faker->randomElement($this->imagesList()),
                'target' => $faker->paragraph,
                'status' =>rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
                'slug' => str_slug($faker->name),
            ]);
        }
    }
}
