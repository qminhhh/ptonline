<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<20;$i++) {
            DB::table('lessons')->insert([
                'lessonName' =>$faker->name,
                'idCourse' => $faker->numberBetween(1,10),
                'content' => $faker->paragraph,
                'linkVideo'=> $faker->email,
                'menu' => $faker->paragraph,
                'status' =>rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
