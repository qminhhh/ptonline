<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provience')->insert([
            ['id' => 1, 'provinceName' => 'Hà Nội'],
            ['id' => 2, 'provinceName' => 'TP Hồ Chí Minh'],
        ]);
    }
}
