<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class OrderDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<10;$i++) {
            DB::table('order_detail')->insert([
                'course_id' => $faker->numberBetween(1,10),
                'order_id' => $faker->numberBetween(1,10),
                'price' => $faker->numberBetween(10000,9000000),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
