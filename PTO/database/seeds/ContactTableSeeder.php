<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<15;$i++) {
            DB::table('contact')->insert([
                'email' => $faker->email,
                'phone' => $faker->numberBetween(10000,9000000),
                'status' => $faker->numberBetween(0,1),
                'content' => $faker->paragraph,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
