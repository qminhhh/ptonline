<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<5;$i++) {
            DB::table('types')->insert([
                'typeName' => $faker->name,
                'status' =>rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
