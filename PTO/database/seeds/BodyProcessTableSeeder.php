<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BodyProcessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<30;$i++) {
            DB::table('body_process')->insert([
                'date_update' => $faker->dateTime(), 
                'height' => $faker->numberBetween(150,190),
                'weight' => $faker->numberBetween(40,120),
                'bust' => $faker->numberBetween(60,120),
                'wais' => $faker->numberBetween(60,120),
                'hips' => $faker->numberBetween(60,120),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
                'idUser' => $faker->numberBetween(1,10),
            ]);
        }
    }
}
