<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function imagesList($path = 'upload/news')
    {
        $images = [];
        $path = public_path($path);
        $allowedMimeTypes = ['image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/svg+xml'];
        $files = File::allFiles($path);
        foreach ($files as $file) {
            $contentType = mime_content_type((string)$file);
            if (in_array($contentType, $allowedMimeTypes)) {
                $images[] = stristr((string)$file, DIRECTORY_SEPARATOR . "upload");
            }
        }

        if (empty($images) or count($images) < 5) {
            uploadImages();

            return imagesList();
        }

        return $images;
    }
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<15;$i++) {
            DB::table('news')->insert([
                'title' =>$faker->name,
                'category_id' => $faker->numberBetween(1,5),
//                'image' => $faker->imageUrl(),
                'image' =>$faker->randomElement($this->imagesList()),
                'abbreviate' => $faker->paragraph,
                'content'=> $faker->paragraph,
                'status' =>rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
