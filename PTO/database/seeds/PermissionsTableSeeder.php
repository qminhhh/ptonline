<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = ['user','course','pt','contact','category','lesson','new','order','orderdetail','role','type','admin','pto','chat','dashboard'];
        foreach ($permissions as $permission) {
            DB::table('permissions')->insert([
                'name' => 'read '.$permission,
                'description' => 'read',
                'slug' => str_slug('read '.$permission),
            ]);
            DB::table('permissions')->insert([
                'name' => 'create '.$permission,
                'description' => 'create',
                'slug' => str_slug('create '.$permission),
            ]);
            DB::table('permissions')->insert([
                'name' => 'update '.$permission,
                'description' => 'update',
                'slug' => str_slug('update '.$permission),
            ]);
            DB::table('permissions')->insert([
                'name' => 'delete '.$permission,
                'description' => 'delete',
                'slug' => str_slug('delete '.$permission),
            ]);
        }

    }

}
