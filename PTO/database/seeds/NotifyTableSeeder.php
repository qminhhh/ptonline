<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NotifyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<10;$i++) {
            DB::table('notify')->insert([
                'user_id' => $faker->numberBetween(1,10),
                'message' => $faker->paragraph,
                'noti' => $faker->paragraph,
                'link' =>  $faker->email,
                'status' => 0 ,
                'seen' => 0,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
