<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<10;$i++) {
            DB::table('orders')->insert([
                'user_id' => $faker->numberBetween(1,10),
                'total' => $faker->numberBetween(10000,9000000),
                'paymentdue'=>$faker->dateTime(),
                'status' =>rand(0,1),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}
