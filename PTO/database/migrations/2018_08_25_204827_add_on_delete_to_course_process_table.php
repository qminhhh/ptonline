<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnDeleteToCourseProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_process', function (Blueprint $table) {
            $table->dropForeign('course_process_user_id_foreign');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
//            $table->dropForeign('course_process_lesson_id_foreign');
//            $table->foreign('lesson_id')
//                ->references('id')->on('lessons')
//                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_process', function (Blueprint $table) {
            //
        });
    }
}
