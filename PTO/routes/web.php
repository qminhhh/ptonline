<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/xacthuc/{token}', 'Auth\RegisterController@verify');


// ad - pto - user - guest
Route::get('/home', 'UsersController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('login/google', 'Auth\LoginController@redirectToProvider')->name('login.google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('login/facebook', 'Auth\LoginController@redirect')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@callback');
Route::get('/homepage', 'UsersController@index')->name('homepage');
Route::get('/show-all-pto', 'UsersController@showAll')->name('showall');
Route::get('courses/showAll', ['as' => 'courses.showAll', 'uses' => 'CourseController@showAll']);

Route::get('/{slug}.html', 'CourseController@showCourse')->name('courses.showCourse');

Route::get('new', ['as' => 'new.index', 'uses' => 'NewController@index']);
Route::get('/tin-tuc/{slug}.html', 'NewController@new_detail')->name('new.newdetail');
Route::get('contact', ['as' => 'contact.index', 'uses' => 'ContactController@index']);
Route::post('contact/store', ['as' => 'contact.store', 'uses' => 'ContactController@store']);
Route::get('/', 'UsersController@index');
Route::get('/search', 'SearchController@search')->name('sreach');

Route::get('/index', function () {
    return view('master_layout.admin.index');
});

Route::get('/all-course', 'CourseController@showAll')->name('all-course');
Route::get('/pto-all/{slug}.html', 'ptoController@ptoshowdetail')->name('ptodetail');
Route::get('paypal', ['as' => 'paypal.index', 'uses' => 'PayPalTestController@index']);
Route::get('paypal/status', ['as' => 'paypal.status', 'uses' => 'PayPalTestController@status']);
Route::post('comment/{id}/store', ['as' => 'comment.store', 'uses' => 'CommentController@store']);
Route::post('comment/{id}/saveCmt', ['as' => 'comment.saveCmt', 'uses' => 'CommentController@saveCmt']);
Route::post('courses/{id}/storeRate', ['as' => 'courses.storeRate', 'uses' => 'CourseController@storeRate', 'roles' => ['create-account']]);
Route::delete('comment/{id}', ['as' => 'comment.destroy', 'uses' => 'CommentController@destroy', 'permission' => "delete-type"]);
Route::get('notifies', ['as' => 'notify.index', 'uses' => 'NotifyController@index']);
Route::get('notifies/{id}/seen', ['as' => 'notify.seen', 'uses' => 'NotifyController@seen']);
Route::get('notifies/show', ['as' => 'notify.showNotify', 'uses' => 'NotifyController@showNotify']);
Route::get('notifies/{id}/status', ['as' => 'notify.status', 'uses' => 'NotifyController@status']);
Route::get('pt/create', ['as' => 'pto.create', 'uses' => 'ptoController@create', 'permission' => "create-account"])->middleware('auth');
Route::put('pt/{id}/update', ['as' => 'pto.update', 'uses' => 'ptoController@update', 'permission' => "update-account"])->middleware('auth');
Route::get('pt/{id}/show', ['as' => 'pto.show', 'uses' => 'ptoController@ptoshow', 'permission' => "read-pt"])->middleware('auth');
Route::get('pt/error', ['as' => 'pto.error', 'uses' => 'ptoController@error'])->middleware('auth');
Route::get('pt/rule', ['as' => 'pto.rule', 'uses' => 'ptoController@rule'])->middleware('auth');
Route::get('type/{id}/show', ['as' => 'type.show', 'uses' => 'TypeController@show']);
Route::get('category/{id}/show', ['as' => 'category.show', 'uses' => 'CategoryController@show', 'permission' => "read-category"]);


Route::middleware(['CheckRole', 'CheckVerified'])->group(function () {

    Route::get('/userslist', ['as' => 'userslist', 'uses' => 'UsersController@list', 'permission' => "read-admin"]);
    Route::get('users/data', ['as' => 'users.data', 'uses' => 'UsersController@data', 'permission' => "read-admin"]);
    Route::get('users', ['as' => 'users.index', 'uses' => 'UsersController@index', 'permission' => "read-user"]);
    Route::get('users/create', ['as' => 'users.create', 'uses' => 'UsersController@create', 'permission' => "create-user"]);
    Route::post('users/store', ['as' => 'users.store', 'uses' => 'UsersController@store', 'permission' => "create-user"]);
    Route::get('users/{id}/show', ['as' => 'users.show', 'uses' => 'UsersController@show', 'permission' => "read-user"]);
    Route::get('users/{id}/chart', ['as' => 'users.chart', 'uses' => 'UsersController@chart', 'permission' => "read-user"]);
    Route::post('users/updateChart', ['as' => 'users.updateChart', 'uses' => 'UsersController@updateChart', 'permission' => "create-user"]);
    Route::get('users/{id}/edit', ['as' => 'users.edit', 'uses' => 'UsersController@edit', 'permission' => "update-user"]);
    Route::post('users/{id}', ['as' => 'users.update', 'uses' => 'UsersController@update', 'permission' => "update-user"]);
    Route::post('users1/{id}', ['as' => 'users.update1', 'uses' => 'UsersController@update1', 'permission' => "update-user"]);
    Route::post('updatePass/{id}', ['as' => 'users.updatePass', 'uses' => 'UsersController@updatePass', 'permission' => "update-user"]);
    Route::get('users/{id}/myAccount', ['as' => 'users.myAccount', 'uses' => 'UsersController@myAccount', 'permission' => "read-user"]);
    Route::get('users/{id}/changePass', ['as' => 'users.changePass', 'uses' => 'UsersController@changePass', 'permission' => "read-user"]);
    Route::delete('users/{id}', ['as' => 'users.destroy', 'uses' => 'UsersController@destroy', 'permission' => "delete-user"]);
    Route::get('users/listTrainee', ['as' => 'users.listTrainee', 'uses' => 'UsersController@listTrainee', 'permission' => 'read-pto']);
    Route::get('users/dataTrainee', ['as' => 'users.dataTrainee', 'uses' => 'UsersController@dataTrainee', 'permission' => 'read-pto']);


    Route::get('pt/data', ['as' => 'pto.data', 'uses' => 'ptoController@data', 'permission' => "read-admin"]);
    Route::get('pt/data1', ['as' => 'pto.data1', 'uses' => 'ptoController@data1', 'permission' => "read-admin"]);
    Route::get('pt/{id}/accept', ['as' => 'pto.accept', 'uses' => 'ptoController@accept', 'permission' => "read-admin"]);
    Route::post('pt/store', ['as' => 'pto.store', 'uses' => 'ptoController@store', 'permission' => "create-pt"]);
    Route::get('pt/{id}/edit', ['as' => 'pto.edit', 'uses' => 'ptoController@edit', 'permission' => "update-pt"]);
    Route::get('pt/{id}/edited', ['as' => 'pto.edited', 'uses' => 'ptoController@edited', 'permission' => "update-pt"]);
    Route::put('pt/{id}/update1', ['as' => 'pto.update1', 'uses' => 'ptoController@update1', 'permission' => "update-pt"]);
    Route::put('pt/{id}/delete', ['as' => 'pto.delete', 'uses' => 'ptoController@delete', 'permission' => "update-pt"]);
    Route::delete('pt/{id}', ['as' => 'pto.destroy', 'uses' => 'ptoController@destroy', 'permission' => "delete-pt"]);
    Route::get('pt/listwaiting', ['as' => 'pto.list', 'uses' => 'ptoController@list', 'permission' => "read-admin"]);
    Route::get('pt/list', ['as' => 'pto-list', 'uses' => 'ptoController@ptoList', 'permission' => "read-admin"]);
//    Route::get('/pto-detail/{id}', ['as' => 'pto-detail', 'uses' => 'ptoController@ptoDetail', 'permission' => "read-admin"]);
    Route::put('/pt/{id}/updated', ['as' => 'ptoupdate', 'uses' => 'ptoController@updated', 'permission' => "update-pt"]);
    Route::get('pt/dataPT', ['as' => 'pto.dataPT', 'uses' => 'ptoController@dataPT', 'permission' => "read-pt"]);
    Route::get('pt/{id}/show', ['as' => 'pto.show', 'uses' => 'ptoController@show', 'permission' => "read-pt"]);


//Category of Course
    Route::get('type/data', ['as' => 'type.data', 'uses' => 'TypeController@data', 'permission' => "read-type"]);
    Route::get('type/list', ['as' => 'type.list', 'uses' => 'TypeController@list', 'permission' => "read-type"]);
    Route::get('type/create', ['as' => 'type.create', 'uses' => 'TypeController@create', 'permission' => "create-type"]);
    Route::post('type/store', ['as' => 'type.store', 'uses' => 'TypeController@store', 'permission' => "create-type"]);
    Route::get('type/{id}/edit', ['as' => 'type.edit', 'uses' => 'TypeController@edit', 'permission' => "update-type"]);
    Route::put('type/{id}', ['as' => 'type.update', 'uses' => 'TypeController@update', 'permission' => "update-type"]);
    Route::delete('type/{id}', ['as' => 'type.destroy', 'uses' => 'TypeController@destroy', 'permission' => "delete-type"]);

//Course
    Route::get('courses/data', ['as' => 'courses.data', 'uses' => 'CourseController@data', 'permission' => "read-admin"]);
    Route::get('courses/list', ['as' => 'courses.list', 'uses' => 'CourseController@list', 'permission' => "read-admin"]);
    Route::get('courses', ['as' => 'courses.index', 'uses' => 'CourseController@index', 'permission' => "create-course"]);
    Route::get('courses/deactive', ['as' => 'courses.deactive', 'uses' => 'CourseController@deactive', 'permission' => 'create-course']);
    Route::get('courses/lock', ['as' => 'courses.lock', 'uses' => 'CourseController@lock', 'permission' => 'create-course']);
    Route::get('courses/create', ['as' => 'courses.create', 'uses' => 'CourseController@create', 'permission' => "create-course"]);
    Route::post('courses/store', ['as' => 'courses.store', 'uses' => 'CourseController@store', 'permission' => "create-course"]);
    Route::get('courses/{id}/show', ['as' => 'courses.show', 'uses' => 'CourseController@show', 'permission' => "read-admin"]);
    Route::get('courses/{id}/edit', ['as' => 'courses.edit', 'uses' => 'CourseController@edit', 'permission' => "update-course"]);
    Route::put('courses/{id}', ['as' => 'courses.update', 'uses' => 'CourseController@update', 'permission' => "update-course"]);
    Route::delete('courses/{id}', ['as' => 'courses.destroy', 'uses' => 'CourseController@destroy', 'permission' => "delete-course"]);
    Route::get('courses/myCourse', ['as' => 'courses.myCourse', 'uses' => 'CourseController@myCourse', 'permission' => "read-course"]);
    Route::get('course/{id}/detail', ['as' => 'courses.detail', 'uses' => 'CourseController@detail', 'permission' => "read-course"]);
    Route::get('course-detail/{id}', ['as' => 'course.detail', 'uses' => 'CourseController@courseDetail', 'permission' => "read-pto"]);
    Route::get('courses/{id}/courseDetail', ['as' => 'courses.courseDetail', 'uses' => 'CourseController@courseDetail', 'permission' => "read-pto"]);
    Route::get('courses/{id}/courseLockDt', ['as' => 'courses.courseLockDt', 'uses' => 'CourseController@courseLockDt', 'permission' => "read-pto"]);
    Route::get('courses/{id}/dataTrainee', ['as' => 'courses.dataTrainee', 'uses' => 'CourseController@dataTrainee', 'permission' => "read-course"]);
    Route::put('course_manage/{id}/lessonDone', ['as' => 'course_manage.lessonDone', 'uses' => 'CourseManageController@lessonDone', 'permission' => "read-course"]);
    Route::get('course/{id}/rating', ['as' => 'courses.rating', 'uses' => 'CourseController@rating', 'permission' => "read-course"]);
    Route::put('course/{id}/openLock', ['as' => 'courses.openLock', 'uses' => 'CourseController@openLock', 'permission' => "update-admin"]);


//Lesson
    Route::get('lessons/create', ['as' => 'lessons.create', 'uses' => 'LessonController@create', 'permission' => "create-lesson"]);
    Route::post('lessons/store', ['as' => 'lessons.store', 'uses' => 'LessonController@store', 'permission' => "create-lesson"]);
    Route::get('lessons/{id}/edit', ['as' => 'lessons.edit', 'uses' => 'LessonController@edit', 'permission' => "update-lesson"]);
    Route::put('lessons/{id}', ['as' => 'lessons.update', 'uses' => 'LessonController@update', 'permission' => "update-lesson"]);
    Route::delete('lessons/{id}', ['as' => 'lessons.destroy', 'uses' => 'LessonController@destroy', 'permission' => "delete-lesson"]);
    Route::get('lessons/{id}/show', ['as' => 'lessons.show', 'uses' => 'LessonController@show', 'permission' => "read-lesson"]);
    Route::get('lessons/{id}/notifice', ['as' => 'lessons.showNotifice', 'uses' => 'LessonController@showNotifice', 'permission' => "read-lesson"]);
//Category
    Route::get('category/data', ['as' => 'category.data', 'uses' => 'CategoryController@data', 'permission' => "read-admin"]);
    Route::get('category/list', ['as' => 'category.list', 'uses' => 'CategoryController@list', 'permission' => "read-admin"]);
    Route::get('category/create', ['as' => 'category.create', 'uses' => 'CategoryController@create', 'permission' => "create-category"]);
    Route::post('category/store', ['as' => 'category.store', 'uses' => 'CategoryController@store', 'permission' => "create-category"]);
    Route::get('category/{id}/edit', ['as' => 'category.edit', 'uses' => 'CategoryController@edit', 'permission' => "update-category"]);
    Route::put('category/{id}', ['as' => 'category.update', 'uses' => 'CategoryController@update', 'permission' => "update-category"]);
    Route::delete('category/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy', 'permission' => "delete-category"]);

//News
    Route::get('new/data', ['as' => 'new.data', 'uses' => 'NewController@data', 'permission' => "read-admin"]);
    Route::get('new/list', ['as' => 'new.list', 'uses' => 'NewController@list', 'permission' => "read-admin"]);
    Route::get('new/create', ['as' => 'new.create', 'uses' => 'NewController@create', 'permission' => "create-new"]);
    Route::post('new/store', ['as' => 'new.store', 'uses' => 'NewController@store', 'permission' => "create-new"]);
    Route::get('new/{id}/show', ['as' => 'new.show', 'uses' => 'NewController@show', 'permission' => "read-new"]);
    Route::get('new/{id}/edit', ['as' => 'new.edit', 'uses' => 'NewController@edit', 'permission' => "update-new"]);
    Route::put('new/{id}', ['as' => 'new.update', 'uses' => 'NewController@update', 'permission' => "update-new"]);
    Route::delete('new/{id}', ['as' => 'new.destroy', 'uses' => 'NewController@destroy', 'permission' => "delete-new"]);


//Order

    Route::get('order/dataCourse', ['as' => 'order.dataCourse', 'uses' => 'OrderController@dataCourse', 'permission' => "read-admin"]);
    Route::get('order/data', ['as' => 'order.data', 'uses' => 'OrderController@data', 'permission' => "read-admin"]);
    Route::get('order/list', ['as' => 'order.list', 'uses' => 'OrderController@list', 'permission' => "read-admin"]);
    Route::get('order/showcart', ['as' => 'order.showcard', 'uses' => 'OrderController@showCart', 'permission' => "read-order"]);
    Route::get('order/listCourse', ['as' => 'order.listCourse', 'uses' => 'OrderController@listCourse', 'permission' => "read-admin"]);
    Route::get('order/manage', ['as' => 'order.manage', 'uses' => 'OrderController@manage', 'permission' => "read-order"]);
    Route::post('order/saveCOD', ['as' => 'order.saveCOD', 'uses' => 'OrderController@saveCOD', 'permission' => "create-order"]);
    Route::post('order/addcart', ['as' => 'order.addcart', 'uses' => 'OrderController@add_cart', 'permission' => "create-order"]);
    Route::post('order/cartList', ['as' => 'order.cartList', 'uses' => 'OrderController@cartList', 'permission' => "create-order"]);
    Route::get('order/{id}/show', ['as' => 'order.show', 'uses' => 'OrderController@show', 'permission' => "read-admin"]);
    Route::put('order/{id}/order-save', ['as' => 'order.orderSave', 'uses' => 'OrderController@orderSave', 'permission' => "update-admin"]);
    Route::post('order/deleteCart', ['as' => 'order.deleteCart', 'uses' => 'OrderController@deleteCart', 'permission' => "read-order"]);
    Route::get('order/{id}/detailOrder', ['as' => 'order.detailOrder', 'uses' => 'OrderController@detailOrder', 'permission' => "read-order"]);
    Route::post('order/{id}/activeOrder', ['as' => 'order.activeOrder', 'uses' => 'OrderController@activeOrder', 'permission' => "create-order"]);
    Route::get('order/listCourse', ['as' => 'order.listCourse', 'uses' => 'OrderController@listCourse', 'permission' => "read-order"]);
    Route::get('order/dataCourse', ['as' => 'order.dataCourse', 'uses' => 'OrderController@dataCourse', 'permission' => "read-order"]);


//contact
    Route::get('contact/data', ['as' => 'contact.data', 'uses' => 'ContactController@data', 'permission' => "read-admin"]);
    Route::get('contact/list', ['as' => 'contact.list', 'uses' => 'ContactController@list', 'permission' => "read-admin"]);
    Route::get('contact/{id}/show', ['as' => 'contact.show', 'uses' => 'ContactController@show', 'permission' => "read-contact"]);
    Route::delete('contact/{id}', ['as' => 'contact.destroy', 'uses' => 'ContactController@destroy', 'permission' => "delete-contact"]);

//admin-role
    Route::get('admin/data', ['as' => 'admin.data', 'uses' => 'AdminController@data', 'permission' => "read-role"]);
    Route::get('admin/list', ['as' => 'admin.list', 'uses' => 'AdminController@list', 'permission' => "read-role"]);
    Route::get('admin', ['as' => 'admin.index', 'uses' => 'AdminController@index', 'permission' => "read-admin"]);
    Route::get('admin/create', ['as' => 'admin.create', 'uses' => 'AdminController@create', 'permission' => "create-role"]);
    Route::post('admin/store', ['as' => 'admin.store', 'uses' => 'AdminController@store', 'permission' => "create-role"]);
    Route::get('admin/{id}/show', ['as' => 'admin.show', 'uses' => 'AdminController@show', 'permission' => "read-role"]);
    Route::get('admin/{id}/edit', ['as' => 'admin.edit', 'uses' => 'AdminController@edit', 'permission' => "update-role"]);
    Route::put('admin/{id}', ['as' => 'admin.update', 'uses' => 'AdminController@update', 'permission' => "update-role"]);
    Route::delete('admin/{id}', ['as' => 'admin.destroy', 'uses' => 'AdminController@destroy', 'permission' => "delete-role"]);

    //chat
    Route::get('chat', ['as' => 'chat.index', 'uses' => 'ChatController@index', 'permission' => "read-chat"]);
    Route::post('chat/store', ['as' => 'chat.store', 'uses' => 'ChatController@store', 'permission' => "create-chat"]);
//dashboard
    Route::get('dashboard/data', ['as' => 'dashboard.data', 'uses' => 'DashboardController@data', 'permission' => "read-dashboard"]);
    Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index', 'permission' => "read-dashboard"]);


});




