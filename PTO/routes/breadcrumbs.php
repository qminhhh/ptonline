<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('dashboard.index'));
});


// Home > pto-list
Breadcrumbs::for('pto-list', function ($trail) {
    $trail->parent('home');
    $trail->push('PTO', route('pto-list'));
});

// Home > Danh sách PTO > pto-detail
Breadcrumbs::for('pto-detail', function ($trail, $user) {
    $trail->parent('pto-list');
    $trail->push($user->userName, route('pto-detail', $user->id));
});

// Home > pto.list
Breadcrumbs::for('pto.list', function ($trail) {
    $trail->parent('home');
    $trail->push('PTO chờ duyệt', route('pto.list'));
});

// Home > Danh sách PTO chờ duyệt > Chi tiết
Breadcrumbs::for('pto.accept', function ($trail, $user) {
    $trail->parent('pto.list');
    $trail->push($user->userName, route('pto.accept', $user->id));
});

// Home > Quản lý Khóa học
Breadcrumbs::for('courses.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Khóa học', route('courses.list'));
});

// Home > Quản lý Khóa học > Chi tiết Khóa học
Breadcrumbs::for('courses.show', function ($trail, $course) {
    $trail->parent('courses.list');
    $trail->push($course->courseName, route('courses.show', $course->id));
});

// Home > Quản lý Đơn hàng
Breadcrumbs::for('order.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Đơn hàng', route('order.list'));
});

// Home > Quản lý Đơn hàng >Chi tiết đơn hàng
Breadcrumbs::for('order.show', function ($trail, $order) {
    $trail->parent('order.list');
    $trail->push($order->id, route('order.show',$order->id));
});

// Home > Quản lý Liên hệ
Breadcrumbs::for('contact.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Liên hệ', route('contact.list'));
});

// Home > Quản lý Đơn hàng >Chi tiết liên hệ
Breadcrumbs::for('contact.show', function ($trail, $contact) {
    $trail->parent('contact.list');
    $trail->push($contact->email, route('contact.show',$contact->id));
});

// Home > Quản lý Người dùng
Breadcrumbs::for('userslist', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Người dùng', route('userslist'));
});

// Home > Quản lý Người dùng > Thông tin người dùng
Breadcrumbs::for('users.show', function ($trail,$user) {
    $trail->parent('userslist');
    $trail->push($user->userName, route('users.show',$user->id));
});

// Home > Thông tin PTO
Breadcrumbs::for('pto.show', function ($trail,$user) {
    $trail->parent('home');
    $trail->push($user->userName, route('pto.show',$user->id));
});

// Home > Thông tin PTO >Chỉnh sửa
Breadcrumbs::for('pto.edited', function ($trail,$user) {
    $trail->parent('home');
    $trail->push($user->userName, route('pto.edited',$user->id));
});

// Home > Quản lý Người dùng > Chỉnh sửa
Breadcrumbs::for('users.edit', function ($trail,$user) {
    $trail->parent('userslist');
    $trail->push($user->userName, route('users.edit',$user->id));
});

// Home > Quản lý Nhóm
Breadcrumbs::for('admin.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Nhóm', route('admin.index'));
});

// Home > Quản lý Nhóm > Thêm mới Nhóm
Breadcrumbs::for('admin.create', function ($trail) {
    $trail->parent('admin.index');
    $trail->push('Thêm mới Nhóm', route('admin.create'));
});

// Home > Quản lý Nhóm > Chi tiết
Breadcrumbs::for('admin.show', function ($trail,$role) {
    $trail->parent('admin.index');
    $trail->push($role->roleName, route('admin.show',$role->id));
});

// Home > Quản lý Nhóm > Chỉnh sửa
Breadcrumbs::for('admin.edit', function ($trail,$role) {
    $trail->parent('admin.index');
    $trail->push($role->roleName, route('admin.edit',$role->id));
});

// Home > Danh sách Thể loại
Breadcrumbs::for('category.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Danh sách Thể loại', route('category.list'));
});

// Home > Danh sách Thể loại > Thêm mới
Breadcrumbs::for('category.create', function ($trail) {
    $trail->parent('category.list');
    $trail->push('Thêm mới Thể loại', route('category.create'));
});

// Home > Danh sách Thể loại > Chỉnh sửa
Breadcrumbs::for('category.edit', function ($trail,$category) {
    $trail->parent('category.list');
    $trail->push($category->categoryName, route('category.edit',$category->id));
});

// Home > Danh sách Tin tức
Breadcrumbs::for('new.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Danh sách Tin tức', route('new.list'));
});

// Home > Danh sách Tin tức > Thêm mới
Breadcrumbs::for('new.create', function ($trail) {
    $trail->parent('new.list');
    $trail->push('Thêm mới Tin tức', route('new.create'));
});

// Home > Danh sách Tin tức > Chi tiết
Breadcrumbs::for('new.show', function ($trail, $new) {
    $trail->parent('new.list');
    $trail->push($new->title, route('new.show', $new->id));
});

// Home > Danh sách Tin tức > Chi tiết
Breadcrumbs::for('new.edit', function ($trail, $new) {
    $trail->parent('new.list');
    $trail->push($new->title, route('new.edit', $new->id));
});

// Home > Tin nhắn
Breadcrumbs::for('chat.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Tin nhắn', route('chat.index'));
});

// Home > Danh sách Thể loại
Breadcrumbs::for('type.list', function ($trail) {
    $trail->parent('home');
    $trail->push('Thể loại khóa học', route('type.list'));
});

// Home > Danh sách Thể loại > Thêm mới
Breadcrumbs::for('type.create', function ($trail) {
    $trail->parent('type.list');
    $trail->push('Thêm mới Thể loại', route('type.create'));
});

// Home > Danh sách Thể loại > Chỉnh sửa
Breadcrumbs::for('type.edit', function ($trail,$type) {
    $trail->parent('type.list');
    $trail->push($type->typeName, route('type.edit',$type->id));
});

// Home > Khóa học đã kích hoạt
Breadcrumbs::for('courses.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Khóa học đã kích hoạt', route('courses.index'));
});

// Home > Khóa học đã kích hoạt > Chi tiết
Breadcrumbs::for('courses.courseDetail', function ($trail,$course) {
    $trail->parent('home');
    $trail->push($course->courseName, route('courses.courseDetail',$course->id));
});

//// Home > Chi tiết khóa học > Chỉnh sửa
//Breadcrumbs::for('courses.edit', function ($trail,$course,$type) {
//    $trail->parent('courses.courseDetail');
//    $trail->push('Cập nhật', route('courses.edit',$course->id,$type->id));
//});

// Home > Khóa học đã kích hoạt > Chi tiết > Chỉnh sửa
Breadcrumbs::for('lessons.edit', function ($trail,$course,$lesson) {
    $trail->parent('courses.courseDetail',$course);
    $trail->push($lesson->lessonName, route('lessons.edit',$lesson->id));
});

//// Home > Khóa học đã kích hoạt > Chi tiết > Thêm bài học
//Breadcrumbs::for('lessons.create', function ($trail,$course,$lesson) {
//    $trail->parent('lessons.edit',$course);
//    $trail->push('Thêm bài học', route('lessons.create',$lesson->id));
//});

// Home > Thêm mới khóa học
Breadcrumbs::for('courses.create', function ($trail) {
    $trail->parent('home');
    $trail->push('Thêm mới Khóa học', route('courses.create'));
});

// Home > Khóa học đã khóa
Breadcrumbs::for('courses.lock', function ($trail) {
    $trail->parent('home');
    $trail->push('Khóa học đã khóa', route('courses.lock'));
});

// Home > Khóa học chưa kích hoạt
Breadcrumbs::for('courses.deactive', function ($trail) {
    $trail->parent('home');
    $trail->push('Khóa học chưa kích hoạt', route('courses.deactive'));
});

// Home > Quản lý Học viên
Breadcrumbs::for('users.listTrainee', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Học viên', route('users.listTrainee'));
});

// Home > Khóa học đã kích hoạt
Breadcrumbs::for('order.listCourse', function ($trail) {
    $trail->parent('home');
    $trail->push('Quản lý Tài chính', route('order.listCourse'));
});

