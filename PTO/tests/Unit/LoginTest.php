<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function test_body_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('course');

        $this->call('GET', '/logout');
    }






    public function test_body13_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => '123@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('Giỏ hàng');

        $this->call('GET', '/logout');
    }


    public function test_body14_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('fa fa-paper-plane');

        $this->call('GET', '/logout');
    }


    public function test_body15_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('Chi tiết');

        $this->call('GET', '/logout');
    }

    public function test_body16_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('cousePrice');

        $this->call('GET', '/logout');
    }

    public function test_body17_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('PT:');

        $this->call('GET', '/logout');
    }


    public function test_body18_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('courseName');

        $this->call('GET', '/logout');
    }

    public function test_body19_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('stars-bottom-comment');

        $this->call('GET', '/logout');
    }

    public function test_body20_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('fa fa-star');

        $this->call('GET', '/logout');
    }


    public function test_body21_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('IMG-PTO');

        $this->call('GET', '/logout');
    }


    public function test_body22_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('Thể loại');

        $this->call('GET', '/logout');
    }


    public function test_body23_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('* DANH SÁCH TẤT CẢ KHÓA HỌC *');

        $this->call('GET', '/logout');
    }


    public function test_body24_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }



    public function test_body243_div5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/myCourse');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }

    public function test_body246_div4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/users/3/myAccount');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body243_div3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/users/3/changePass');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body245_div2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/users/3/chart');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body249_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/notifies/show');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body248_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/6/show');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body247_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body246_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body245_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body244_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body243_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body242_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }

    public function test_body24_div6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body24_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body724_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body624_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }

    public function test_body524_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body424_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body324_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body224_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body124_div()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


}
