<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_news_add()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tập gym tăng cường sức mạnh cơ bắp',
            'category_id' => '1',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/20/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Chia sẻ thực đơn giảm cân Lowcarb giúp giảm 5kg trong 1 tháng',
            'category_id' => '2',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/18/edit', [
            'title' => 'Tập gym tăng cường sức mạnh giúp cơ thể cường tráng',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Không chỉ nóng bỏng trên thảm đỏ, Kỳ Duyên trong phòng tập cũng sexy quá trời',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/19/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Ai cũng nên tập gym nhưng đừng dại tập những bài này mà không có người hướng dẫn',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/21/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/new/create', [
            'title' => 'Bí quyết luyện tập cho vóc dáng hoàn hảo',
            'category_id' => '4',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_edit4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/21/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'category_id' => '5',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/22/edit', [
            'status' => '0',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Chia sẻ thực đơn giảm cân Lowcarb giúp giảm 5kg trong 1 tháng',
            'category_id' => '9',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/23/edit', [
            'category_id' => '4',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Giải đáp thắc mắc về tập gym cho nữ',
            'category_id' => '1',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/18/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Những phép lịch sự tối thiểu ở phòng gym mà anh chàng nào cũng nên biết',
            'category_id' => '3',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/15/edit', [
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Thay đổi vóc dáng với thực đơn tăng cơ giảm mỡ',
            'category_id' => '4',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/21/edit', [
            'status' => '0',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tìm Hiểu Và Phòng Tránh Đau Lưng',
            'category_id' => '2',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/14/edit', [
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',

            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tập gym tăng cường sức mạnh cơ hông và cơ ngực',
            'category_id' => '4',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/14/edit', [
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Sức mạnh cơ bắp và cơ đùi',
            'category_id' => '9',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }
    public function test_news_edit12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/12/edit', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '3',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tập gym tăng cường sức khỏe dẻo dai',
            'category_id' => '1',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_edit13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/13/edit', [
           'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_news_add14()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tập gym tăng cường sức mạnh cơ bắp và sức khỏe hàng ngày',
            'category_id' => '5',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_news_add15()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/new/create', [
            'title' => 'Tập gym tăng cường sức mạnh cơ thể',
            'category_id' => '2',
            'abbreviate' => 'Bạn có biết Gym đã và đang thay đổi cuộc sống của rất nhiều người?',
            'content' => 'Gym không còn là khái niệm xa lạ với phần lớn người Việt Nam. Hiện nay, các phòng Gym đang mọc lên như nấm để đáp ứng nhu cầu tập thể dục và sống khoẻ của con người.',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'status' => '1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


}
