<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LessonTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */

    public function test_lesson_add()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 1',
            'idCourse' => '40',
            'content' => 'Khởi động, eo nhanh, lắc hông, giật, cơ đùi,thư giãn...',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'bữa sáng : sữa + trứng ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/24/edit?idCourse=13', [
            'lessonName' => 'Bài 1',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 2',
            'idCourse' => '40',
            'content' => 'Những bài tập Bodyweight training giúp cải thiện vóc dáng cùng các chia sẻ về dinh dưỡng và làm đẹp. Chẳng bao lâu nữa chiếc eo bánh mì sẽ thành chiếc eo săn chắc quyến rũ với bài tập eo thon bụng phẳng này!',
            'video' => 'https://www.youtube.com/watch?v=XTQ3n9uYD_4&t=27s',
            'linkVideo' => 'Sách Eo thon bụng phẳng bài tập bụng dưới và eo',
            'menu' => ' Bữa sáng: 130 kcal - bánh mỳ kẹp trứng: 1 chiếc ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/25/edit?idCourse=14', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 3',
            'idCourse' => '40',
            'content' => 'Chúng mình lại cùng tập bài tập tiếp theo trong chuỗi video 5 minutes series, với 5 phút mỗi ngày, mỗi ngày một vùng cơ khác nhau',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Giảm mỡ bụng với bài tập 5 phút mỗi ngày',
            'menu' => 'bữa trưa: 357 kcal - Gà nướng mật mong ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/26/edit?idCourse=13', [
            'linkVideo' => 'Sách tăng cân , tăng cơ cho nam',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 4',
            'idCourse' => '40',
            'content' => 'Một bài tập HIIT thường chỉ kéo dài 20 phút nhưng đốt một lượng calo tương đương chạy bộ 40-60 phút.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'HITT 1: Jump Jack',
            'menu' => 'Bữa tối: 240.5 kcal- taco cải thảo: 150g ức gà luộc ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/27/edit?idCourse=14', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 5',
            'idCourse' => '40',
            'content' => 'Một bài tập HIIT thường chỉ kéo dài 20 phút nhưng đốt một lượng calo tương đương chạy bộ 40-60 phút',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách HITT 2: Squat  ',
            'menu' => 'bữa sáng : sữa + trứng ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/29/edit?idCourse=13', [
            'lessonName' => '',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 6',
            'idCourse' => '40',
            'content' => 'Ai cũng biết luyện tập để giữ gìn vóc dáng là điều vô cùng quan trọng nhất là đối với phái đẹp. ',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách 5 phút trên giường ',
            'menu' => 'Bữa sáng: 330 kcal - 1 cốc nước chanh, 1 bắp luộc ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/30/edit?idCourse=13', [
            'lessonName' => 'Bài 3',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/create?id=40', [
            'lessonName' => 'Bài 7',
            'idCourse' => '40',
            'content' => 'Tập từ 1- 4',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách Các động tác đơn giản ',
            'menu' => 'Bữa sáng : sữa + trứng ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/31/edit?idCourse=14', [
            'lessonName' => 'Bài 7',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 8',
            'idCourse' => '40',
            'content' => 'Tập từ 5-9',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'bữa sáng : sữa + trứng ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/34/edit?idCourse=14', [
            'lessonName' => 'Bài 8',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/create?id=40', [
            'lessonName' => 'Bài 9',
            'idCourse' => '40',
            'content' => 'Các động tác đơn giản',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'bữa sáng : sữa + trứng ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/25/edit?idCourse=14', [
            'lessonName' => 'Bài 9',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 10',
            'idCourse' => '40',
            'content' => 'Đây là 1 động tác khá đơn giản, tuy nhiên bạn cần lựa sức mình mà tập khi mới thử lúc ban đầu. ',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách Bridge Lifts',
            'menu' => 'Bữa đêm: 176 kcal -  Sữa chocolate: 200ml sữa tươi không đường, 50g chocolate ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/25/edit?idCourse=14', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 11',
            'idCourse' => '40',
            'content' => 'Alternating crunch là một bài tập hoàn hảo để điêu khắc hiệu quả bụng và có được một gói sáu tuyệt vời. ',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '. Bữa sáng: 116.5 kcal- sữa chua hoa quả : 1 quả xoài chín, 1 hộp sữa chua. ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }


    public function test_lesson_edit11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/25/edit?idCourse=14', [
            'lessonName' => 'Bài 11',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 12',
            'idCourse' => '40',
            'content' => 'Các cuộc khủng hoảng xen kẽ giúp tăng cường và giai điệu bụng và bụng xiên nhưng không chỉ: tập thể dục này cũng mang lại sự ổn định lớn hơn cho các cơ bắp của lưng dưới.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => ' Bữa trưa: 194 kcal - salad rau nầm: 5g đường, 5g giấm,  20g cà rốt,  30g rau nầm, 2g dầu mè, tỏi phi.',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/31/edit?idCourse=14', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 13',
            'idCourse' => '40',
            'content' => 'Các cuộc khủng hoảng với bàn tay đẩy là một bài tập lý tưởng để đào tạo bụng tập trung đặc biệt vào cuối cao của các cơ bụng. B',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '. Bữa chiều: 222,7 kcal - bơ chanh: 1 quả bơ chín, 1 quả chanh ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/33/edit?idCourse=13', [
            'lessonName' => 'Bài 13',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add14()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 14',
            'idCourse' => '40',
            'content' => 'Bài tập này là một biến thể của khủng hoảng kinh điển: với lực đẩy về phía trước của bàn tay, nỗ lực nhiều hơn được thực hiện, đặc biệt là ở cấp cao, được huấn luyện một cách mạnh mẽ hơn và được nhắm mục tiêu.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => ' Bữa tối: 396 kcal- beefsteak: 30g súp lơ, 30g cà rốt, dầu olive, muối, tiêu, tỏi, 80g thịt bò ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit14()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/34/edit?idCourse=14', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add15()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 15',
            'idCourse' => '40',
            'content' => 'Khởi động, eo nhanh, lắc hông',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sáchăng cơ ',
            'menu' => ' Bữa đêm: 176 kcal - sữa chocolate: 20g chocolate, 200ml sữa không đường, nấu lên',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit15()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/36/edit?idCourse=13', [
            'lessonName' => 'Bài 15',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add16()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 16',
            'idCourse' => '40',
            'content' => 'Bài tập nhỏ: Crunch with pushing hands',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '1. Bữa sáng: 174.4 kcal- cháo yến mạch mận : 30g yến mạch, 10ml mật ong, nước nóng, 1 quả mận chín ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit16()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/38/edit?idCourse=13', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add17()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 17',
            'idCourse' => '40',
            'content' => 'Bài tập nhỏ: Legs elevation in 4 strokes',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa trưa: 253.5 kcal - salad gà ngô: 150g ức gà luộc, 50g ngô mỹ luộc,  50g cà chua bi,  50g xà lách, 10g mật ong, 10g giấm táo, 5g muối,  tiêu, 10ml dầu olive',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit17()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/39/edit?idCourse=13', [
            'lessonName' => 'Bài 17',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add18()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 18',
            'idCourse' => '40',
            'content' => 'Bài tập nhỏ: Crunch with vertical legs and touch of the ankles',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa chiều: 19.6 kcal - salab cà chua giấm: 100g cà chua, 20g húng quế, 10ml giấm táo, tiêu.',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit18()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/40/edit?idCourse=13', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add19()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 19',
            'idCourse' => '40',
            'content' => 'Bài tập nhỏ: Crunch with knees to the chest',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa tối: 240.5 kcal- taco cải thảo: 150g ức gà luộc, muối, tiêu, 50g bơ, 50g xoài chín, 20g cà chua, 10ml giấm táo, 10ml dầu olive, 30g cải thảo luộc,  ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit19()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/42/edit?idCourse=13', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add20()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=40', [
            'lessonName' => 'Bài 20',
            'idCourse' => '40',
            'content' => 'Tổng hợp tất cả các động tác trên',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa đêm: 205 kcal - sữa chua hoa quả: 30g xoài, 30g mận, 30g bơ, 30g dưa hấu, 1 hộp sữa chua.',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit20()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/43/edit?idCourse=13', [
            'lessonName' => 'Bài 20',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add21()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 1',
            'idCourse' => '41',
            'content' => 'Bài tập nhỏ: Crunch with vertical legs and touch of the ankles',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '. Bữa đêm: 70 kcal - quả táo : 1 quả ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_edit21()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/44/edit?idCourse=13', [
            'lessonName' => 'Ngày 1',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add22()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 2',
            'idCourse' => '41',
            'content' => 'Bài tập nhỏ: Legs elevation in 4 strokes',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa tối: 315 kcal - 150g ức gà luộc,',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit22()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/46/edit?idCourse=15', [
            'lessonName' => 'Ngày 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add23()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 3',
            'idCourse' => '41',
            'content' => 'Bài tập nhỏ: Alternating Crunch',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa chiều: 75 kcal - sinh tố xoài 1 cốc',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit23()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/47/edit?idCourse=15', [
            'lessonName' => 'Ngày 3',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

    public function test_lesson_add24()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 4',
            'idCourse' => '41',
            'content' => 'Bài tập nhỏ: Foot 2 Foot Crunch',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => ' Bữa trưa: 337 kcal - 1/2 bát cơm,',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit24()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/48/edit?idCourse=15', [
            'lessonName' => 'Ngày 4',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add25()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 5',
            'idCourse' => '41',
            'content' => 'Bài tập nhỏ: Crunch with crossed arms',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa sáng: 205 kcal - bún cá 1 bát ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit25()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/149/edit?idCourse=22', [
            'lessonName' => 'Ngày 5',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add26()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 6',
            'idCourse' => '41',
            'content' => 'Bài tập lớn',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa đêm: 109 kcal -  sữa chocola',
            'menu' => 'Bữa đêm: 109 kcal -  sữa chocola',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit26()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/150/edit?idCourse=22', [
            'lessonName' => 'Ngày 6',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add27()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 7',
            'idCourse' => '41',
            'content' => 'Khởi động',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa tối: 352 kcal - salad rau: 150g xà lách, ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit27()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/152/edit?idCourse=23', [
            'lessonName' => 'Ngày 7',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add28()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 8',
            'idCourse' => '41',
            'content' => 'Co duỗi từng chân trên không trung',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => ' Bữa chiều: 58.8 kcal - 1 hộp sữa chua',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit28()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/152/edit?idCourse=23', [
            'lessonName' => 'Ngày 8',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add29()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 9',
            'idCourse' => '41',
            'content' => 'Squat ở tư thế nằm',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa trưa: 390 kcal - 1/2 bát cơm, 100g thịt sốt chua ngọt, 150g rau củ luộc',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit29()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/153/edit?idCourse=23', [
            'lessonName' => 'Ngày 9',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add30()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 10',
            'idCourse' => '41',
            'content' => 'Bạn nằm nghiêng về bên phải, tay phải để dọc bên thân, khuỷu tay trái co lại góc 90 độ, ',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa sáng: 198 kcal - mỳ xào bò 1 bát',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit30()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/153/edit?idCourse=23', [
            'lessonName' => 'Ngày 10',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add31()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 11',
            'idCourse' => '41',
            'content' => 'bàn tay trái nắm lấy khuỷu tay phải, 2 chân sát nhau.Đá chân trái thẳng lên trần, sau đó hạ bàn chân trái vào đầu gối chân phải rồi duỗi ra về tư thế ban đầu',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa đêm: 70 kcal - quả táo : 1 quả',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit31()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/124/edit?idCourse=31', [
            'lessonName' => 'Ngày 11',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add32()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 12',
            'idCourse' => '41',
            'content' => 'Nằm nghiêng bên trái, tay trái để dọc thân, tay phải co vuông góc, bàn tay phải nắm vào khuỷu tay trái, 2 chân sát nhau, co đầu gối lại',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa tối: 315 kcal - 150g ức gà luộc, 100g xúp lơ luộc',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit32()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/125/edit?idCourse=31', [
            'lessonName' => 'Ngày 12',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add33()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 13',
            'idCourse' => '41',
            'content' => 'Chân phải tách ra thẳng lên phía trần trong khi cố gắng giữ chân trái ở đúng vị trí. Trở lại tư thế ban đầu và lặp lại.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa chiều: 75 kcal - sinh tố xoài 1 cốc',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit33()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/127/edit?idCourse=32', [
            'lessonName' => 'Ngày 13',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add34()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 14',
            'idCourse' => '41',
            'content' => 'Tập 10 - 15 lần mỗi bên.(Tập động tác thứ 4 x20)',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa trưa: 337 kcal - 1/2 bát cơm, 150g sườn xào chua ngọt, 100g khoai lang luộc',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit34()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/128/edit?idCourse=32', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add35()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 15',
            'idCourse' => '41',
            'content' => 'Nằm ngửa trên sàn, 2 tay để cạnh người, 2 chân sát nhau giờ thẳng lên trần.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa sáng: 250 kcal - bún riêu cua 1 bát ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit35()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/129/edit?idCourse=32', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add36()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 16',
            'idCourse' => '41',
            'content' => 'Dần dần tách 2 chân sang 2 bên hết khả năng. Co chân lại và lặp lại động tác.',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '5. Bữa đêm:76 kcal - sữa chua 1 hộp',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit36()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/130/edit?idCourse=33', [
            'lessonName' => 'Bài 2',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add37()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 17',
            'idCourse' => '41',
            'content' => 'Hai chân dạng bằng vai, hai tay chạm vào nhau, thực hiện tư thế đứng lên ngồi xuống, sao cho góc chân chùng khoảng 45 độ Liên tục thực hiện động tác',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa tối: 350 kcal - 100g thịt bò luộc, 200g rau cải luộc, xì dầu, 2 cốc nước ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit37()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/135/edit?idCourse=34', [
            'lessonName' => 'Ngày 17',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add38()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 18',
            'idCourse' => '41',
            'content' => 'Nằm ngửa xuống đất, hai tay duỗi thẳng, chân co lên và nâng cao phần lưng, eo, chân co giữ vị trí 90 độ',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa chiều: 90 kcal - bánh mỳ không',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit38()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/139/edit?idCourse=36', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add39()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 19',
            'idCourse' => '41',
            'content' => 'Một chân chống, một chân giơ thẳng nâng vùng mình và eo lên, nâng càng cao càng tốt',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => '. Bữa trưa: 320 kcal - 1/2 bát cơm, 1 bìa to đậu sốt cà chua, 100g cải thảo luộc. ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit39()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/140/edit?idCourse=36', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_add40()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/lessons/create?id=41', [
            'lessonName' => 'Ngày 40',
            'idCourse' => '41',
            'content' => 'Đổi chân tiếp tục thực hiện động tác',
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            'linkVideo' => 'Sách tăng cân , tăng cơ ',
            'menu' => 'Bữa sáng: 120 kcal - bánh cuốn: 1 đĩa, sữa đậu nành 100g',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã tạo bài học thành công');

        $this->call('GET', '/logout');
    }
    public function test_lesson_edit40()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/lessons/140/edit?idCourse=36', [
            'video' => 'https://www.youtube.com/watch?v=C24z5OU83Vw',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật bài học thành công');

        $this->call('GET', '/logout');
    }

}
