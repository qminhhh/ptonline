<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */

    public function test_course_add()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('success');
        $this->call('GET', '/logout');
    }

    public function test_course_edit()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/40/edit', [
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => '20 bài tập giảm cân, giảm mỡ bụng nhanh chóng',
            'price' => '70000',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân , giảm mỡ  bụng nhanh chóng',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Cần các dụng cụ tập cơ bản',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_course_edit1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/35/edit', [
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Để nữ giới có một vóc dáng đẹp',
            'price' => '40000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ tăng cân tăng cơ',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Dành cho nam',
            'require' => 'Có máy tập',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_course_edit2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/38/edit', [
            'price' => '70000',
            'type_id' => '3',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học tập cơ lưng ',
            'price' => '30000',
            'idUser' => '1',
            'type_id' => '4',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }


    public function test_course_edit3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/37/edit', [
            'type_id' => '3',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học tập cơ bụng',
            'price' => '50000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nữ',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Có máy tập',
            'benefit' => 'Vòng eo cải thiện đáng kể',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/35/edit', [
            'target' => 'Tất cả mọi người và mọi giới tính',
            'require' => 'Trang phục phải gọn gàng , dụng cụ tập đầy đủ',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }


    public function test_course_add5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam và nữ',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/34/edit', [
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học nâng tạ tại nhà',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/40/edit', [
            'price' => '80000',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }


    public function test_course_add7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => '21 bài tập gập bụng tại nhà dễ tập đánh tan mỡ bụng cho nam lẫn nữ',
            'price' => '30000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/27/edit', [
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }


    public function test_course_add8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Tập gym tăng cường sức mạnh cơ bắp',
            'price' => '20000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm tin tức thành công');

        $this->call('GET', '/logout');
    }

    public function test_course_edit8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/25/edit', [
            'price' => '60000',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Giảm mỡ bụng ở các bạn nam',
            'price' => '70000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/20/edit', [
            'type_id' => '3',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'tập toàn thân cho người béo',
            'price' => '80000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/30/edit', [
            'price' => '160000',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Rocky Pull-Ups/Pulldowns',
            'price' => '90000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/32/edit', [
            'type_id' => '3',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/23/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Thể dục thẩm mỹ: các bài tập cho eo thon',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit13()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/29/edit', [
            'price' => '60000',
            'type_id' => '3',
            'status' => '0',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add14()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit14()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/36/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }


    public function test_course_add15()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit15()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/29/edit', [
            'type_id' => '2',
            'status' => '1',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add16()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit16()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/32/edit', [
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }


    public function test_course_add17()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit17()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/30/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add18()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit18()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/27/edit', [
            'price' => '60000',
            'type_id' => '2',
            'status' => '0',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add19()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit19()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/36/edit', [
            'price' => '60000',
            'type_id' => '2',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add20()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit20()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/36/edit', [
            'price' => '60000',
            'type_id' => '2',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add21()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit21()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/28/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add22()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit22()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/40/edit', [
            'type_id' => '3',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add23()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit23()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/23/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add24()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit24()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/40/edit', [
            'price' => '80000',
            'status' => '0',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add25()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit25()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/39/edit', [
            'price' => '60000',
            'type_id' => '2',
            'status' => '1',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add26()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit26()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/40/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add27()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit27()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/34/edit', [
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add28()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit28()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/28/edit', [
            'type_id' => '2',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add29()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam và nữ',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit29()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/28/edit', [
            'type_id' => '2',
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

    public function test_course_add30()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/courses/create', [
            'courseName' => 'Khóa học giảm cân cho nam',
            'price' => '60000',
            'idUser' => '1',
            'type_id' => '2',
            'status' => '0',
            'introduce' => 'Khóa học này hỗ trợ giảm cân cho các bạn nam',
            'image' => '/upload/courses/giam-can-eo-thon-co-san-chac1535299207.jpg',
            'target' => 'Tất cả mọi người',
            'require' => 'Trang phục phải gọn gàng',
            'benefit' => 'Tăng sức khỏe rõ rệt',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('success');

        $this->call('GET', '/logout');
    }

    public function test_course_edit30()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/courses/30/edit', [
            'status' => '1',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã cập nhật khóa học thành công');
        $this->call('GET', '/logout');
    }

}
