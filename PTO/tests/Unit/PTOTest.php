<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PTOTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */


    public function test_body12_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('PTO');

        $this->call('GET', '/logout');
    }


    public function test_body13_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body14_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Tin tức');

        $this->call('GET', '/logout');
    }


    public function test_body15_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Về chúng tôi');

        $this->call('GET', '/logout');
    }



    public function test_body16_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Liên hệ');

        $this->call('GET', '/logout');
    }



    public function test_body17_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('HLV Nữ');

        $this->call('GET', '/logout');
    }


    public function test_body18_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/dashboard');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }

    public function test_newTitle_h1()
    {
        $response = $this->get('/new');

        $response->assertSee('TIN TỨC');
    }

    public function test_slogan_p1()
    {
        $response = $this->get('/new');

        $response->assertSee('slogan');
    }

    public function test_TypeOfNew_1()
    {
        $response = $this->get('/new');

        $response->assertSee('Thể loại');
    }







}
