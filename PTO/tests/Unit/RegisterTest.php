<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */


    public function test_body124_div12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }

}
