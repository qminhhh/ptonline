<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MiddwareTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_middleware1()
    {
        $response = $this->get('/order/showcart');
        $response->assertSee('homepage');
    }

    public function test_middleware2()
    {
        $response = $this->get('/homepage');
        $response->assertSee('Trang chủ');
    }

    public function test_middleware3()
    {
        $response = $this->get('/show-all-pto');
        $response->assertSee('Tất cả PTO');
    }

    public function test_middleware4()
    {
        $response = $this->get('/courses/showAll');
        $response->assertSee('Khóa học');
    }

    public function test_middleware5()
    {
        $response = $this->get('/new');
        $response->assertSee('Tin tức');
    }

    public function test_middleware6()
    {
        $response = $this->get('/about');
        $response->assertSee('Về chúng tôi');
    }

    public function test_middleware7()
    {
        $response = $this->get('/contact');
        $response->assertSee('Liên hệ');
    }


    public function test_middleware8()
    {
        $response = $this->get('/users/1/myAccount');
        $response->assertSee('homepage');
    }

    public function test_middleware9()
    {
        $response = $this->get('/userslist');
        $response->assertSee('homepage');
    }


    public function test_middleware10()
    {
        $response = $this->get('/users/data');
        $response->assertSee('homepage');
    }


    public function test_middleware11()
    {
        $response = $this->get('/users');
        $response->assertSee('homepage');
    }

    public function test_middleware12()
    {
        $response = $this->get('/users/1/show');
        $response->assertSee('homepage');
    }

    public function test_middleware13()
    {
        $response = $this->get('/users/1/chart');
        $response->assertSee('homepage');
    }

    public function test_middleware14()
    {
        $response = $this->get('/users/1/myAccount');
        $response->assertSee('homepage');
    }

    public function test_middleware15()
    {
        $response = $this->get('/users/1/changePass');
        $response->assertSee('homepage');
    }

    public function test_middleware16()
    {
        $response = $this->get('/users/listTrainee');
        $response->assertSee('homepage');
    }

    public function test_middleware17()
    {
        $response = $this->get('/users/dataTrainee');
        $response->assertSee('homepage');
    }


    public function test_middleware18()
    {
        $response = $this->get('/pt/data');
        $response->assertSee('homepage');
    }

    public function test_middleware19()
    {
        $response = $this->get('pt/data1');
        $response->assertSee('homepage');
    }


    public function test_middleware20()
    {
        $response = $this->get('pt/listwaiting');
        $response->assertSee('homepage');
    }

    public function test_middleware21()
    {
        $response = $this->get('/pt/list');
        $response->assertSee('homepage');
    }


    public function test_middleware23()
    {
        $response = $this->get('/type/data');
        $response->assertSee('homepage');
    }

    public function test_middleware24()
    {
        $response = $this->get('/type/list');
        $response->assertSee('homepage');
    }

    public function test_middleware25()
    {
        $response = $this->get('/courses/data');
        $response->assertSee('homepage');
    }

    public function test_middleware26()
    {
        $response = $this->get('/courses/list');
        $response->assertSee('homepage');
    }

    public function test_middleware27()
    {
        $response = $this->get('/courses/1/show');
        $response->assertSee('homepage');
    }


    public function test_middleware28()
    {
        $response = $this->get('course/1/detail');
        $response->assertSee('homepage');
    }

    public function test_middleware29()
    {
        $response = $this->get('course-detail/1');
        $response->assertSee('homepage');
    }


    public function test_middleware30()
    {
        $response = $this->get('courses/1/courseDetail');
        $response->assertSee('homepage');
    }


    public function test_middleware31()
    {
        $response = $this->get('/courses/1/courseLockDt');
        $response->assertSee('homepage');
    }

    public function test_middleware32()
    {
        $response = $this->get('/courses/1/dataTrainee');
        $response->assertSee('homepage');
    }

    public function test_middleware33()
    {
        $response = $this->get('/course_manage/1/lessonDone');
        $response->assertSee('homepage');
    }

    public function test_middleware34()
    {
        $response = $this->get('/course/1/rating');
        $response->assertSee('homepage');
    }

    public function test_middleware35()
    {
        $response = $this->get('/lessons/1/show');
        $response->assertSee('homepage');
    }

    public function test_middleware36()
    {
        $response = $this->get('/category/data');
        $response->assertSee('homepage');
    }

    public function test_middleware37()
    {
        $response = $this->get('/category/list');
        $response->assertSee('homepage');
    }


    public function test_middleware38()
    {
        $response = $this->get('/category/1/show');
        $response->assertSee('homepage');
    }

    public function test_middleware39()
    {
        $response = $this->get('/new/data');
        $response->assertSee('homepage');
    }

    public function test_middleware40()
    {
        $response = $this->get('new/list');
        $response->assertSee('homepage');
    }


}
