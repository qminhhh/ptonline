<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return void
     */

    public function test_category_add1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Kiến thức thể hình',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');
        $this->call('GET', '/logout');
    }

    public function test_category_edit1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/1/edit', [
            'categoryName' => 'Kiến thức thể hình',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Thể hình nam',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/2/edit', [
            'categoryName' => 'Kiến thức dinh dưỡng',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }


    public function test_category_add3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Thể hình nữ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/3/edit', [
            'categoryName' => 'Thể loại tin tức',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Dinh dưỡng',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/4/edit', [
            'categoryName' => 'Kiến thức thể hình 1',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Tin tức và sự kiện',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/5/edit', [
            'categoryName' => 'Kiến thức thể hình 2',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Dinh dưỡng thể hình',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/9/edit', [
            'categoryName' => 'Kiến thức thể hình 3',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Tăng cân , tăng cơ',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_edit7()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/1/edit', [
            'categoryName' => 'Kiến thức thể hình và thể thao',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Chỉnh sửa thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'StreetWorkout',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Phòng tập thể hình',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Chế độ ăn hợp lý',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'Thể loại khác',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }

    public function test_category_add12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('POST', '/category/create', [
            'categoryName' => 'PTO nổi tiếng',
            '_token' => csrf_token()
        ]);

        $this->followRedirects($response);
        $response->assertSee('Đã thêm thể loại thành công');

        $this->call('GET', '/logout');
    }


    public function test_body()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'abc@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body1()
    {
        $response = $this->call('POST', '/login', [
            'email' => '123@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/new');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body2()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/contact');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }


    public function test_body3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/about');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }



}
