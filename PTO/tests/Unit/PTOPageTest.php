<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PTOPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_body3()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/courses/showAll');

        $this->followRedirects($response);
        $response->assertSee('Chi tiết');

        $this->call('GET', '/logout');
    }

    public function test_body4()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body5()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Giới thiệu giáo viên');

        $this->call('GET', '/logout');
    }


    public function test_body6()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Thông tin khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body7()
    {
        $response = $this->call('POST', '/login', [
            'email' => '123@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Đánh giá từ học viên');

        $this->call('GET', '/logout');
    }


    public function test_body8()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body9()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body10()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body11()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body12()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/giam-can-eo-thon-co-san-chac-13.html');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_pto_button2()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee('Tất cả PTO');
    }

    public function test_male_button2()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee(' HLV Nữ');
    }

    public function test_female_button2()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee(' HLV Nam');
    }

    public function test_title_text()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee('Huấn luyện viên cá nhân');
    }

    public function test_pt_image()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee('IMG-PRODUCT');
    }

    public function test_ptDetail_a()
    {
        $response = $this->get('/show-all-pto');

        $response->assertSee('Chi tiết');
    }

    public function test_body12_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('PTO');

        $this->call('GET', '/logout');
    }


    public function test_body13_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Khóa học');

        $this->call('GET', '/logout');
    }


    public function test_body14_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Tin tức');

        $this->call('GET', '/logout');
    }


    public function test_body15_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Về chúng tôi');

        $this->call('GET', '/logout');
    }


    public function test_body16_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('Liên hệ');

        $this->call('GET', '/logout');
    }


    public function test_body17_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/');

        $this->followRedirects($response);
        $response->assertSee('HLV Nữ');

        $this->call('GET', '/logout');
    }


    public function test_body18_div1()
    {
        $response = $this->call('POST', '/login', [
            'email' => 'pto@gmail.com',
            'password' => 'secret',
            '_token' => csrf_token()
        ]);
        $this->followRedirects($response);

        $response = $this->call('GET', '/dashboard');

        $this->followRedirects($response);
        $response->assertSee('homepage');

        $this->call('GET', '/logout');
    }



}
